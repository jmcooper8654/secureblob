// ------------------------------------------------------------------------------
// <copyright file="ByteArrayToolTests.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-9</date>
// <summary>
//     Description of File Name "ByteArrayToolTests.cs" in Project "UnitTestByteArrayTool":
// </summary>
// ------------------------------------------------------------------------------

namespace UnitTestByteArrayTool
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;

    using Gunfighter.Gplv3.Testing;
    using Gunfighter.Gplv3.Testing.Data;
    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the <see cref="ByteArrayTool"/> class.
    /// </summary>
    [TestClass]
    public class ByteArrayToolTests : UnitTestBase
    {
        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestInitialize()"/></remarks>
        [TestInitialize]
        public override void EachTestInitialize()
        {
            base.EachTestInitialize();
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestCleanup()"/></remarks>
        [TestCleanup]
        public override void EachTestCleanup()
        {
            base.EachTestCleanup();
        }

        [TestMethod]
        public void GetBytes_ExpectedBool_AreEqual()
        {
            // Arrange
            bool expected = true;
            bool actual = false;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToBool(actualBytes);
            }

            // Assert for GetBytes()
            Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes));

            // Assert for ToBool()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedChar_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            char expected = (char)rnd.Next(char.MaxValue);
            char actual = char.MinValue;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToChar(actualBytes);
            }

            // Assert for GetBytes()
            Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes));

            // Assert for ToChar()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedDateTime_AreEqual()
        {
            // Arrange
            DateTime expected = DateTime.UtcNow;
            DateTime actual = DateTime.MinValue;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToDateTime(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToDateTime()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedDecimal_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            Decimal expected = new Decimal(rnd.Next() * rnd.NextDouble());
            Decimal actual = 0.00M;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToDecimal(actualBytes);
            }

            // Assert for GetBytes()
            Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes));

            // Assert for ToDecimal()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedDouble_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            double expected = rnd.Next() * rnd.NextDouble();
            double actual = 0.0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToDouble(actualBytes);
            }

            // Assert for GetBytes()
            Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes));

            // Assert for ToDouble()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedFloat_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            float expected = (float)(rnd.Next() * rnd.NextDouble());
            float actual = 0.0F;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToFloat(actualBytes);
            }

            // Assert for GetBytes()
            Assert.IsTrue(expectedBytes.SequenceEqual(actualBytes));

            // Assert for ToDouble()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedInt_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            int expected = rnd.Next(int.MinValue, int.MaxValue);
            int actual = 0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToInt(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToInt()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedLong_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            long expected = rnd.Next(int.MinValue, int.MaxValue) * rnd.Next(int.MinValue, int.MaxValue);
            long actual = 0L;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToLong(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToLong()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedShort_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            short expected = (short)rnd.Next(short.MinValue, short.MaxValue);
            short actual = 0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToShort(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToShort()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedUInt_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            uint expected = (uint)rnd.Next(int.MaxValue) * 2;
            uint actual = 0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToUInt(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToUInt()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedULong_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            ulong expected = (ulong)(rnd.Next() * rnd.Next() * 2);
            ulong actual = 0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToULong(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToULong()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetBytes_ExpectedUShort_AreEqual()
        {
            // Arrange
            Random rnd = new Random();
            ushort expected = (ushort)rnd.Next(ushort.MaxValue);
            ushort actual = 0;

            // Act
            var expectedBytes = ByteArrayTool.GetBytes(expected);
            var actualBytes = new byte[expectedBytes.Length];

            using (MemoryStream ms = new MemoryStream(expectedBytes))
            {
                ms.Read(actualBytes, 0, expectedBytes.Length);

                actual = ByteArrayTool.ToUShort(actualBytes);
            }

            // Assert for GetBytes
            var firstDiff = expectedBytes.Except(actualBytes);
            var secondDiff = actualBytes.Except(expectedBytes);
            Assert.IsTrue(firstDiff.SequenceEqual(secondDiff));

            // Assert for ToUShort()
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToHexString_MaxLengthFirstStartIndexPopulatedByteArray_AreEqual()
        {
            // Arrange
            byte[] vector = (new VectorData<byte>(8, () => RandomTool.GetByte())).ToArray();
            int startIndex = 0;
            int length = vector.Length;
            var expected = BitConverter.ToString(vector, startIndex, length);

            // Act
            var actual = ByteArrayTool.ToHexString(
                vector,
                startIndex,
                length);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToBase64String_MaxLengthFirstOffsetPopulatedByteArray_AreEqual()
        {
            // Arrange
            byte[] vector = (new VectorData<byte>(8, () => RandomTool.GetByte())).ToArray();
            int offset = 0;
            int length = vector.Length;
            var expected = Convert.ToBase64String(vector, offset, length, Base64FormattingOptions.None);

            // Act
            var actual = ByteArrayTool.ToBase64String(
                vector,
                offset,
                length);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToString_MaxLengthFirstIndexPopulatedByteArray_AreEqual()
        {
            // Arrange
            byte[] vector = (new VectorData<byte>(8, () => RandomTool.GetByte())).ToArray();
            int index = 0;
            int length = vector.Length;
            Encoding byteEncoding = Encoding.UTF8;
            var expected = byteEncoding.GetString(vector, index, length);

            // Act
            var actual = ByteArrayTool.ToString(
                vector,
                index,
                length,
                byteEncoding);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
