// ------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-9</date>
// <summary>
//     Description of File Name "AssemblyInfo.cs" in Project "UnitTestByteArrayTool":
// </summary>
// ------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("UnitTestByteArrayTool")]
[assembly: AssemblyDescription("Unit tests for the ByteArrayTool static container class.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("John Merryweather Cooper")]
[assembly: AssemblyProduct("SecureBlob")]
[assembly: AssemblyCopyright("Copyright © 2020 John Merryweather Cooper")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("6127b004-5116-4519-bb39-27b1638aff2b")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
