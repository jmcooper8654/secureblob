﻿// ------------------------------------------------------------------------------
// <copyright file="SymmetricCipher.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-15</date>
// <summary>
//     Description of File Name "SymmetricCipher.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    ///
    /// </summary>
    public class SymmetricCipher
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="secret"></param>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt<TCipher>(byte[] secret, byte[] key, out byte[] data)
            where TCipher : SymmetricAlgorithm, new()
        {
            data = Array.Empty<byte>();
            byte[] lengthBytes = new byte[sizeof(long)];
            Array.Copy(secret, 0, lengthBytes, 0, sizeof(long));
            long length = ByteArrayTool.ToLong(lengthBytes);

            SymmetricHeader header = new SymmetricHeader();
            header = header.Deserialize<SymmetricHeader>(secret);

            using (SymmetricAlgorithm cipher = Activator.CreateInstance<TCipher>())
            {
                cipher.IV = header.IV.ToArray();
                cipher.Key = key;
                cipher.Mode = header.Mode;
                cipher.Padding = header.Padding;

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, cipher.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(secret, (int)length, secret.Length - (int)length);
                        cs.FlushFinalBlock();
                    }

                    data = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="secret"></param>
        /// <param name="password"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt<TCipher>(byte[] secret, string password, out byte[] data)
            where TCipher : SymmetricAlgorithm, new()
        {
            data = Array.Empty<byte>();
            byte[] lengthBytes = new byte[sizeof(long)];
            Array.Copy(secret, 0, lengthBytes, 0, sizeof(long));
            long length = ByteArrayTool.ToLong(lengthBytes);

            SymmetricHeader header = new SymmetricHeader();
            header = header.Deserialize<SymmetricHeader>(secret);

            using (SymmetricAlgorithm cipher = Activator.CreateInstance<TCipher>())
            {
                cipher.IV = header.IV.ToArray();
                cipher.Key = CipherHash.GetPseudoRandomKey(password, header.Salt.ToArray(), header.Rfc2898Iterations, header.Hash);
                cipher.Mode = header.Mode;
                cipher.Padding = header.Padding;

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, cipher.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(secret, (int)length, secret.Length - (int)length);
                        cs.FlushFinalBlock();
                    }

                    data = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="secret"></param>
        /// <param name="key"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt<TCipher>(
            byte[] secret,
            byte[] key,
            CipherMode mode,
            PaddingMode padding,
            out byte[] data)
            where TCipher : SymmetricAlgorithm, new()
        {
            data = Array.Empty<byte>();
            byte[] lengthBytes = new byte[sizeof(long)];
            Array.Copy(secret, 0, lengthBytes, 0, sizeof(long));
            long length = ByteArrayTool.ToLong(lengthBytes);

            SymmetricHeader header = new SymmetricHeader();
            header = header.Deserialize<SymmetricHeader>(secret);

            using (SymmetricAlgorithm cipher = Activator.CreateInstance<TCipher>())
            {
                cipher.IV = header.IV.ToArray();
                cipher.Key = CipherHash.GetPseudoRandomKey(key, header.Salt.ToArray(), header.Rfc2898Iterations, header.Hash);
                cipher.Mode = header.Mode;
                cipher.Padding = header.Padding;

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, cipher.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(secret, (int)length, secret.Length - (int)length);
                        cs.FlushFinalBlock();
                    }

                    data = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="secret"></param>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt(byte[] secret, byte[] key, out byte[] data)
        {
            data = Array.Empty<byte>();
            byte[] lengthBytes = new byte[sizeof(long)];
            Array.Copy(secret, 0, lengthBytes, 0, sizeof(long));
            long length = ByteArrayTool.ToLong(lengthBytes);

            SymmetricHeader header = new SymmetricHeader();
            header = header.Deserialize<SymmetricHeader>(secret);

            using (AesCng aes = new AesCng()
            {
                IV = header.IV.ToArray(),
                Key = CipherHash.GetPseudoRandomKey(key, header.Salt.ToArray(), header.Rfc2898Iterations, header.Hash),
                Mode = header.Mode,
                Padding = header.Padding
            })
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(secret, (int)length, secret.Length - (int)length);
                        cs.FlushFinalBlock();
                    }

                    data = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt(
            byte[] data,
            byte[] key,
            byte[] salt,
            out byte[] secret)
        {
            secret = Array.Empty<byte>();
            salt = GenericTool.IsNullOrDefault(salt) || salt.Length < 16 ? CipherHash.GetSalt(key, 16, 1000, HashAlgorithmName.SHA256) : salt;

            using (AesCng aes = new AesCng()
            {
                Key = CipherHash.GetPseudoRandomKey(key, salt, 1000, HashAlgorithmName.SHA256)
            })
            {
                // prepare header for binary serialization
                aes.GenerateIV();
                SymmetricHeader header = new SymmetricHeader(aes.GetType().Name, aes.IV, salt);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // must be written plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt(
            byte[] data,
            byte[] key,
            byte[] salt,
            int iterations,
            HashAlgorithmName hashAlgorithm,
            CipherMode mode,
            PaddingMode padding,
            out byte[] secret)
        {
            secret = Array.Empty<byte>();
            salt = GenericTool.IsNullOrDefault(salt) || salt.Length < 16 ? CipherHash.GetSalt(key, 16, 1000, HashAlgorithmName.SHA256) : salt;

            using (AesCng aes = new AesCng()
            {
                Key = CipherHash.GetPseudoRandomKey(key, salt, iterations, hashAlgorithm),
                Mode = mode != CipherMode.CBC ? mode : CipherMode.CBC,
                Padding = padding != PaddingMode.PKCS7 ? padding : PaddingMode.PKCS7
            })
            {
                // prepare header for binary serialization
                aes.GenerateIV();
                SymmetricHeader header = new SymmetricHeader(
                    aes.GetType().Name,
                    aes.IV,
                    salt,
                    iterations,
                    hashAlgorithm,
                    aes.Mode,
                    aes.Padding);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // header must be written in plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="data"></param>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt(byte[] data, string password, byte[] salt, out byte[] secret)
        {
            secret = Array.Empty<byte>();
            salt = GenericTool.IsNullOrDefault(salt) || salt.Length < 16 ? CipherHash.GetSalt(password, 16, 1000, HashAlgorithmName.SHA256) : salt;

            using (AesCng aes = new AesCng()
            {
                Key = CipherHash.GetPseudoRandomKey(password, salt, 1000, HashAlgorithmName.SHA256)
            })
            {
                // prepare header for binary serialization
                aes.GenerateIV();
                SymmetricHeader header = new SymmetricHeader(aes.GetType().Name, aes.IV, salt);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // must be written plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="data"></param>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt(
            byte[] data,
            string password,
            byte[] salt,
            int iterations,
            HashAlgorithmName hashAlgorithm,
            CipherMode mode,
            PaddingMode padding,
            out byte[] secret)
        {
            secret = Array.Empty<byte>();
            salt = GenericTool.IsNullOrDefault(salt) || salt.Length < 16 ? CipherHash.GetSalt(password, 16, 1000, HashAlgorithmName.SHA256) : salt;

            using (AesCng aes = new AesCng()
            {
                Key = CipherHash.GetPseudoRandomKey(password, salt, iterations, hashAlgorithm),
                Mode = mode != CipherMode.CBC ? mode : CipherMode.CBC,
                Padding = padding != PaddingMode.PKCS7 ? padding : PaddingMode.PKCS7
            })
            {
                // prepare header for binary serialization
                aes.GenerateIV();
                SymmetricHeader header = new SymmetricHeader(
                    aes.GetType().Name,
                    aes.IV,
                    salt,
                    iterations,
                    hashAlgorithm,
                    aes.Mode,
                    aes.Padding);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // header must be written in plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt<TCipher>(byte[] data, byte[] key, out byte[] secret)
            where TCipher : SymmetricAlgorithm, new()
        {
            secret = Array.Empty<byte>();
            byte[] salt = CipherHash.GetSalt(key, 16, 1000, HashAlgorithmName.SHA256);

            using (dynamic cipher = Activator.CreateInstance<TCipher>())
            {
                cipher.GenerateIV();
                cipher.Key = CipherHash.GetPseudoRandomKey(key, salt, 1000, HashAlgorithmName.SHA256);

                SymmetricHeader header = new SymmetricHeader(
                    cipher.GetType().Name,
                    cipher.IV,
                    salt,
                    1000,
                    HashAlgorithmName.SHA256,
                    CipherMode.CBC,
                    PaddingMode.PKCS7);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // header must be written in plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, cipher.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt<TCipher>(byte[] data, byte[] key, CipherMode mode, PaddingMode padding, out byte[] secret)
            where TCipher : SymmetricAlgorithm, new()
        {
            secret = Array.Empty<byte>();
            byte[] salt = CipherHash.GetSalt(key, 16, 1000, HashAlgorithmName.SHA256);

            using (SymmetricAlgorithm cipher = Activator.CreateInstance<TCipher>())
            {
                cipher.GenerateIV();
                cipher.Key = key;
                cipher.Mode = mode != CipherMode.CBC ? mode : CipherMode.CBC;
                cipher.Padding = padding != PaddingMode.PKCS7 ? padding : PaddingMode.PKCS7;

                SymmetricHeader header = new SymmetricHeader(
                    cipher.GetType().Name,
                    cipher.IV,
                    salt,
                    1000,
                    HashAlgorithmName.SHA256,
                    cipher.Mode,
                    cipher.Padding);
                byte[] headerBytes = header.Serialize<SymmetricHeader>().ToArray();

                using (MemoryStream ms = new MemoryStream())
                {
                    // header must be written in plain text
                    ms.Write(headerBytes, 0, headerBytes.Length);

                    using (CryptoStream cs = new CryptoStream(ms, cipher.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.FlushFinalBlock();
                    }

                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }
    }
}
