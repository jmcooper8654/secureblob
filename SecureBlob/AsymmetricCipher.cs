﻿// ------------------------------------------------------------------------------
// <copyright file="AsymmetricCipher.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-1</date>
// <summary>
//     Description of File Name "AsymmetricCipher.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public class AsymmetricCipher
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="secret"></param>
        /// <param name="privateKey"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt<TCipher>(byte[] secret, CngKey privateKey, out byte[] data)
            where TCipher : AsymmetricAlgorithm, new()
        {
            data = Array.Empty<byte>();

            using (dynamic cipher = (TCipher)Activator.CreateInstance(typeof(TCipher), privateKey))
            {
                byte[] buffer = cipher.DecryptValue(secret);

                using (MemoryStream ms = new MemoryStream(buffer.Length))
                {
                    ms.Write(buffer, 0, buffer.Length);
                    data = ms.ToArray();
                }
            }

            return !secret.SequenceEqual(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secret"></param>
        /// <param name="privateKey"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool Decrypt(byte[] secret, CngKey privateKey, out byte[] data)
        {
            data = Array.Empty<byte>();

            using (RSACng rsa = new RSACng(privateKey))
            {
                byte[] buffer = rsa.DecryptValue(secret);

                using (MemoryStream ms = new MemoryStream(buffer.Length))
                {
                    ms.Write(buffer, 0, buffer.Length);
                    data = ms.ToArray();
                }
            }

            return !secret.SequenceEqual(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="publicKey"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt(byte[] data, CngKey publicKey, out byte[] secret)
        {
            secret = Array.Empty<byte>();

            using (RSACng rsa = new RSACng(publicKey))
            {
                byte[] buffer = rsa.EncryptValue(data);

                using (MemoryStream ms = new MemoryStream(buffer.Length))
                {
                    ms.Write(buffer, 0, buffer.Length);
                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCipher"></typeparam>
        /// <param name="data"></param>
        /// <param name="publicKey"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static bool Encrypt<TCipher>(byte[] data, CngKey publicKey, out byte[] secret)
            where TCipher : AsymmetricCipher, new()
        {
            secret = Array.Empty<byte>();

            using (dynamic cipher = (TCipher)Activator.CreateInstance(typeof(TCipher), publicKey))
            {
                byte[] buffer = cipher.EncryptValue(data);

                using (MemoryStream ms = new MemoryStream(buffer.Length))
                {
                    ms.Write(buffer, 0, buffer.Length);
                    secret = ms.ToArray();
                }
            }

            return !data.SequenceEqual(secret);
        }
    }
}
