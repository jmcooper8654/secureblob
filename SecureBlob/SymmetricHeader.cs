﻿// ------------------------------------------------------------------------------
// <copyright file="SymmetricHeader.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-3</date>
// <summary>
//     Description of File Name "SymmetricHeader.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security;
    using System.Security.Cryptography;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// Class supporting the serialization/deserialization of the Symmetric Cipher Header.
    /// </summary>
    public class SymmetricHeader : BaseHeader
    {
        /// <summary>
        ///
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        ///
        /// </summary>
        private StringBuilder buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmetricHeader"/> class.
        /// </summary>
        public SymmetricHeader()
            : this(string.Empty, Enumerable.Empty<byte>(), Enumerable.Empty<byte>(), 1000, HashAlgorithmName.SHA256, CipherMode.CBC, PaddingMode.PKCS7)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmetricHeader"/> class.
        /// </summary>
        /// <param name="algorithm">The symmetric cipher used for encryption/decryption.</param>
        /// <param name="initialVector">The initial vector salt needed by all <see cref="SymmetricAlgorithm"/>.</param>
        /// <param name="salt">The salt needed by the <see cref="Rfc2898DeriveBytes"/> constructor.</param>
        /// <remarks>The <paramref name="initialVector"/> should always be 16 bytes.  The <paramref name="salt"/> should also be at least 16 bytes.</remarks>
        /// <exception cref="SecurityException">Throws if patently insecure <c>DES</c> algorithms are passed.</exception>
        public SymmetricHeader(
            string algorithm,
            IEnumerable<byte> initialVector,
            IEnumerable<byte> salt)
            : this(algorithm, initialVector, salt, 1000, HashAlgorithmName.SHA256, CipherMode.CBC, PaddingMode.PKCS7)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="algorithm"></param>
        /// <param name="initialVector"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hash"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <remarks>
        /// The <paramref name="initialVector"/> should always be 16 bytes.
        /// The <paramref name="salt"/> should also always be at least 16 bytes.
        /// The <paramref name="hash"/> should not use <see cref="HashAlgorithmName.MD5"/> or <see cref="HashAlgorithmName.SHA1"/>
        /// </remarks>
        /// <exception cref="SecurityException">
        /// Throws if patently insecure <c>DES</c> algorithms are passed.
        /// </exception>
        public SymmetricHeader(
            string algorithm,
            IEnumerable<byte> initialVector,
            IEnumerable<byte> salt,
            int iterations,
            HashAlgorithmName hash,
            CipherMode mode,
            PaddingMode padding)
            : base(salt, iterations, hash)
        {
            this.buffer = new StringBuilder(InitialCapacity);
            this.Algorithm = string.Format(CultureInfo.InvariantCulture, algorithm);
            this.IV = initialVector.Count() == 16 ? initialVector : Enumerable.Empty<byte>();
            this.Mode = this.Mode != CipherMode.CBC ? mode : CipherMode.CBC;
            this.Padding = this.Padding != PaddingMode.PKCS7 ? padding : PaddingMode.PKCS7;
        }

        /// <summary>
        ///
        /// </summary>
        public string Algorithm { get; set; }

        /// <summary>
        ///
        /// </summary>
        public IEnumerable<byte> IV { get; set; }

        /// <summary>
        ///
        /// </summary>
        public CipherMode Mode { get; set; }

        /// <summary>
        ///
        /// </summary>
        public PaddingMode Padding { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THeader"></typeparam>
        /// <param name="headerBytes"></param>
        /// <returns></returns>
        public override THeader Deserialize<THeader>(IEnumerable<byte> headerBytes)
        {
            return base.Deserialize<THeader>(headerBytes);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THeader"></typeparam>
        /// <returns></returns>
        public override IEnumerable<byte> Serialize<THeader>()
        {
            return base.Serialize<THeader>();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            const int FirstColumnWidth = 19;
            const int SecondColumnWidth = 58;

            string firstColumnFormat = StringTool.BuildFormat(0, -FirstColumnWidth);
            string secondColumnFormat = StringTool.BuildFormat(null, 1, -SecondColumnWidth, null, "\n");
            string propertyFormat = string.Concat(firstColumnFormat, " = ", secondColumnFormat);

            this.buffer.Clear();
            this.buffer.Append(base.ToString());

            StringBuilder AppendProperty(string property, params object[] value)
            {
                return buffer.AppendFormat(CultureInfo.InvariantCulture, propertyFormat, property, value);
            }

            foreach (var item in base.ToString().Split('\n'))
            {
                buffer.Append(item);
            }

            AppendProperty(nameof(Algorithm), this.Algorithm);
            AppendProperty(nameof(IV), ByteArrayTool.ToHexString(this.IV.ToArray(), 0, this.IV.Count()));
            AppendProperty(nameof(Mode), this.Mode);
            AppendProperty(nameof(Padding), this.Padding);

            return buffer.ToString();
        }
    }
}
