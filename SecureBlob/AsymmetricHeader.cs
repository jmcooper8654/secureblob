﻿// ------------------------------------------------------------------------------
// <copyright file="AsymmetricHeader.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-13</date>
// <summary>
//     Description of File Name "AsymmetricHeader.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security;
    using System.Security.Cryptography;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// Class supporting the serialization/deserialization of the Asymmetric Cipher Header.
    /// </summary>
    public class AsymmetricHeader : BaseHeader
    {
        /// <summary>
        ///
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        ///
        /// </summary>
        private StringBuilder buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsymmetricHeader"/> class.
        /// </summary>
        public AsymmetricHeader()
            : this(string.Empty, Enumerable.Empty<byte>(), 1000, HashAlgorithmName.SHA256)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AsymmetricHeader"/> class.
        /// </summary>
        /// <param name="algorithm">The asymmetric cipher used for encryption/decryption.</param>
        /// <param name="salt">The salt needed by the <see cref="Rfc2898DeriveBytes"/> constructor.</param>
        /// <remarks>The <paramref name="salt"/> should be at least 16 bytes.</remarks>
        /// <exception cref="SecurityException">Throws if unmaintained <c>DSA</c> algorithm is passed.</exception>
        public AsymmetricHeader(
            string algorithm,
            IEnumerable<byte> salt)
            : this(algorithm, salt, 1000, HashAlgorithmName.SHA256)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="algorithm"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hash"></param>
        /// <remarks>
        /// The <paramref name="salt"/> should always be at least 16 bytes.
        /// The <paramref name="hash"/> should not use <see cref="HashAlgorithmName.MD5"/> or <see cref="HashAlgorithmName.SHA1"/>
        /// </remarks>
        /// <exception cref="SecurityException">
        /// Throws if the unmaintained <c>DSA</c> algorithm is passed.
        /// </exception>
        public AsymmetricHeader(
            string algorithm,
            IEnumerable<byte> salt,
            int iterations,
            HashAlgorithmName hash)
            : base(salt, iterations, hash)
        {
            this.buffer = new StringBuilder(InitialCapacity);
            this.Algorithm = string.Format(CultureInfo.InvariantCulture, algorithm);
        }

        /// <summary>
        ///
        /// </summary>
        public string Algorithm { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THeader"></typeparam>
        /// <param name="headerBytes"></param>
        /// <returns></returns>
        public override THeader Deserialize<THeader>(IEnumerable<byte> headerBytes)
        {
            return base.Deserialize<THeader>(headerBytes);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THeader"></typeparam>
        /// <returns></returns>
        public override IEnumerable<byte> Serialize<THeader>()
        {
            return base.Serialize<THeader>();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            const int FirstColumnWidth = 19;
            const int SecondColumnWidth = 58;

            string firstColumnFormat = StringTool.BuildFormat(0, -FirstColumnWidth);
            string secondColumnFormat = StringTool.BuildFormat(null, 1, -SecondColumnWidth, null, "\n");
            string propertyFormat = string.Concat(firstColumnFormat, " = ", secondColumnFormat);

            this.buffer.Clear();
            this.buffer.Append(base.ToString());

            StringBuilder AppendProperty(string property, params object[] value)
            {
                return buffer.AppendFormat(CultureInfo.InvariantCulture, propertyFormat, property, value);
            }

            foreach (var item in base.ToString().Split('\n'))
            {
                buffer.Append(item);
            }

            AppendProperty(nameof(Algorithm), this.Algorithm);

            return buffer.ToString();
        }
    }
}
