﻿// ------------------------------------------------------------------------------
// <copyright file="DataEncodingType.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-15</date>
// <summary>
//     Description of File Name "DataEncodingType.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    /// <summary>
    /// Enumeration of data encoding types for encoding and decoding data.
    /// </summary>
    public enum DataEncodingType
    {
        /// <summary>
        /// No encoding.  Raw bytes in and raw bytes out.
        /// </summary>
        None = 0,

        /// <summary>
        /// Base 64 encoding.  Bytes in are converted from Base 64 and
        /// encrypted.  On decryption, bytes out are converted to Base 64
        /// encoding.
        /// </summary>
        Base64,

        /// <summary>
        /// <see cref="System.Text.Encoding"/>.  Bytes in are converted from
        /// the byte text encoding and encrypted.  On decryption, bytes out are
        /// converted to the selected text encoding.
        /// </summary>
        TextEncoding
    }
}
