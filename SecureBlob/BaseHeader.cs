﻿// ------------------------------------------------------------------------------
// <copyright file="BaseHeader.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-14</date>
// <summary>
//     Description of File Name "BaseHeader.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Security.Cryptography;
    using System.Text;

    using Gunfighter.Gplv3.Tool;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Bson;

    /// <summary>
    /// Abstract base class for cipher parameter serialization.
    /// </summary>
    public abstract class BaseHeader
    {
        /// <summary>
        ///
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        ///
        /// </summary>
        private StringBuilder buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseHeader"/> abstract class.
        /// </summary>
        public BaseHeader()
            : this(Enumerable.Empty<byte>(), 1000, HashAlgorithmName.SHA256)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseHeader"/> abstract class.
        /// </summary>
        /// <param name="salt">The salt needed by the <see cref="Rfc2898DeriveBytes"/> constructor.</param>
        /// <remarks>The <paramref name="salt"/> should also be at least 16 bytes.</remarks>
        public BaseHeader(
            IEnumerable<byte> salt)
            : this(salt, 1000, HashAlgorithmName.SHA256)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hash"></param>
        /// <remarks>
        /// The <paramref name="salt"/> should also always be at least 16 bytes.
        /// The <paramref name="hash"/> should not use <see cref="HashAlgorithmName.MD5"/> or <see cref="HashAlgorithmName.SHA1"/>
        /// </remarks>
        public BaseHeader(
            IEnumerable<byte> salt,
            int iterations,
            HashAlgorithmName hash)
        {
            this.buffer = new StringBuilder(InitialCapacity);

            if (hash == HashAlgorithmName.MD5 || hash == HashAlgorithmName.SHA1)
            {
                throw new SecurityException($"Hash Algorithm {hash} is insecure.", typeof(HashAlgorithmName));
            }
            else
            {
                this.Hash = hash;
            }

            this.Rfc2898Iterations = iterations > 1000 ? iterations : 1000;
            this.Salt = salt;
            this.Text = string.Format(CultureInfo.InvariantCulture, ".cipher");
        }

        /// <summary>
        ///
        /// </summary>
        public HashAlgorithmName Hash { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int Rfc2898Iterations { get; set; }

        /// <summary>
        ///
        /// </summary>
        public IEnumerable<byte> Salt { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="headerBytes"></param>
        /// <returns></returns>
        public virtual THeader Deserialize<THeader>(IEnumerable<byte> headerBytes)
            where THeader : class
        {
            // Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(headerBytes.ToArray(), 0, headerBytes.Count());
                ms.Position = sizeof(long);

                using (BsonDataReader binaryReader = new BsonDataReader(ms))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    return serializer.Deserialize<THeader>(binaryReader);
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THeader"></typeparam>
        /// <returns></returns>
        public virtual IEnumerable<byte> Serialize<THeader>()
            where THeader : class
        {
            // Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            using (MemoryStream ms = new MemoryStream())
            using (BsonDataWriter binaryWriter = new BsonDataWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(binaryWriter, this, typeof(THeader));
                return BaseHeader.ToLengthBytes(ms.Length + sizeof(long)).Concat(ms.ToArray().ToList());
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            const int FirstColumnWidth = 19;
            const int SecondColumnWidth = 58;

            string firstColumnFormat = StringTool.BuildFormat(0, -FirstColumnWidth);
            string secondColumnFormat = StringTool.BuildFormat(null, 1, -SecondColumnWidth, null, "\n");
            string headerFormat = string.Concat(firstColumnFormat, "   ", secondColumnFormat);
            string propertyFormat = string.Concat(firstColumnFormat, " = ", secondColumnFormat);

            this.buffer.Clear();
            this.buffer.Append(base.ToString());

            StringBuilder AppendHeader(string column1, string column2)
            {
                const char LineChar = '-';

                this.buffer.AppendFormat(CultureInfo.InvariantCulture, headerFormat, column1, column2);
                return this.buffer.AppendFormat(CultureInfo.InvariantCulture, headerFormat, new string(LineChar, FirstColumnWidth), new string(LineChar, SecondColumnWidth));
            }

            StringBuilder AppendProperty(string property, params object[] value)
            {
                return this.buffer.AppendFormat(CultureInfo.InvariantCulture, propertyFormat, property, value);
            }

            AppendHeader("Property", "Value");
            AppendProperty(nameof(this.Text), this.Text);
            AppendProperty(nameof(this.Rfc2898Iterations), this.Rfc2898Iterations);
            AppendProperty(nameof(this.Hash), this.Hash);
            AppendProperty(nameof(this.Salt), ByteArrayTool.ToHexString(this.Salt.ToArray(), 0, this.Salt.Count()));

            return this.buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static long GetLength(IEnumerable<byte> bytes)
        {
            return BitConverter.IsLittleEndian ? BitConverter.ToInt64(bytes.Take(sizeof(long)).Reverse().ToArray(), 0) : BitConverter.ToInt64(bytes.Take(sizeof(long)).ToArray(), 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static IEnumerable<byte> ToLengthBytes(long length)
        {
            return BitConverter.IsLittleEndian ? BitConverter.GetBytes(length).Reverse() : BitConverter.GetBytes(length).ToList();
        }
    }
}
