﻿// ------------------------------------------------------------------------------
// <copyright file="CipherHash.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-13</date>
// <summary>
//     Description of File Name "CipherHash.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public static class CipherHash
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherName"></param>
        /// <param name="keyLength"></param>
        /// <param name="initializationVector"></param>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetCryptographicKey(string cipherName, int keyLength, byte[] initializationVector, string password, byte[] salt, int iterations, HashAlgorithmName hashAlgorithm)
        {
            using (var db = new Rfc2898DeriveBytes(password, salt, iterations, hashAlgorithm))
            {
                return db.CryptDeriveKey(cipherName, hashAlgorithm.ToString(), keyLength, initializationVector);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherName"></param>
        /// <param name="keyLength"></param>
        /// <param name="initializationVector"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetCryptographicKey(string cipherName, int keyLength, byte[] initializationVector, byte[] key, byte[] salt, int iterations, HashAlgorithmName hashAlgorithm)
        {
            using (var db = new Rfc2898DeriveBytes(key, salt, iterations, hashAlgorithm))
            {
                return db.CryptDeriveKey(cipherName, hashAlgorithm.ToString(), keyLength, initializationVector);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, byte[] salt)
        {
            using (var db = new Rfc2898DeriveBytes(password, salt))
            {
                return db.GetBytes(password.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, int saltLength, out byte[] salt)
        {
            salt = Array.Empty<byte>();

            using (var db = new Rfc2898DeriveBytes(password, saltLength))
            {
                var pseudoKey = db.GetBytes(password.Length);
                salt = db.Salt;
                return pseudoKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(byte[] key, byte[] salt, int iterations)
        {
            using (var db = new Rfc2898DeriveBytes(key, salt, iterations))
            {
                return db.GetBytes(key.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, byte[] salt, int iterations)
        {
            using (var db = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                return db.GetBytes(password.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, int saltLength, int iterations, out byte[] salt)
        {
            salt = Array.Empty<byte>();

            using (var db = new Rfc2898DeriveBytes(password, saltLength, iterations))
            {
                var pseudoKey = db.GetBytes(password.Length);
                salt = db.Salt;
                return pseudoKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(byte[] key, byte[] salt, int iterations, HashAlgorithmName hashAlgorithm)
        {
            using (var db = new Rfc2898DeriveBytes(key, salt, iterations, hashAlgorithm))
            {
                return db.GetBytes(key.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, byte[] salt, int iterations, HashAlgorithmName hashAlgorithm)
        {
            using (var db = new Rfc2898DeriveBytes(password, salt, iterations, hashAlgorithm))
            {
                return db.GetBytes(password.Length);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GetPseudoRandomKey(string password, int saltLength, int iterations, HashAlgorithmName hashAlgorithm, out byte[] salt)
        {
            salt = Array.Empty<byte>();

            using (var db = new Rfc2898DeriveBytes(password, saltLength, iterations, hashAlgorithm))
            {
                var pseudoKey = db.GetBytes(password.Length);
                salt = db.Salt;
                return pseudoKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <returns></returns>
        public static byte[] GetSalt(string password, int saltLength)
        {
            _ = GetPseudoRandomKey(password, saltLength, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static byte[] GetSalt(string password, int saltLength, int iterations)
        {
            _ = GetPseudoRandomKey(password, saltLength, iterations, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetSalt(string password, int saltLength, int iterations, HashAlgorithmName hashAlgorithm)
        {
            _ = GetPseudoRandomKey(password, saltLength, iterations, hashAlgorithm, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="saltLength"></param>
        /// <returns></returns>
        public static byte[] GetSalt(byte[] key, int saltLength)
        {
            _ = GetPseudoRandomKey(Convert.ToBase64String(key), saltLength, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static byte[] GetSalt(byte[] key, int saltLength, int iterations)
        {
            _ = GetPseudoRandomKey(Convert.ToBase64String(key), saltLength, iterations, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="saltLength"></param>
        /// <param name="iterations"></param>
        /// <param name="hashAlgorithm"></param>
        /// <returns></returns>
        public static byte[] GetSalt(byte[] key, int saltLength, int iterations, HashAlgorithmName hashAlgorithm)
        {
            _ = GetPseudoRandomKey(Convert.ToBase64String(key), saltLength, iterations, hashAlgorithm, out byte[] salt);
            return salt.Length == saltLength ? salt : throw new CryptographicException($"Generation of Salt {salt} failed.");
        }
    }
}
