﻿// ------------------------------------------------------------------------------
// <copyright file="SecureBlob.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-3</date>
// <summary>
//     Description of File Name "SecureBlob.cs" in Project "SecureBlob":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Security.SecureBlob
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// Class supporting encryption/decryption with equality based on hash and salt.
    /// </summary>
    public class SecureBlob : IEqualityComparer<SecureBlob>, IEquatable<SecureBlob>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecureBlob"/> class.
        /// </summary>
        public SecureBlob()
        {
            this.Clear();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureBlob"/> class.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        public SecureBlob(byte[] data, byte[] key, byte[] salt)
        {
            this.Encrypt(data, key, salt);
        }

        /// <summary>
        /// Gets a value indicating the encrypted data bytes.
        /// </summary>
        public IEnumerable<byte> Blob { get; private set; }

        /// <summary>
        /// Gets a value indicating the computed <c>SHA256</c> hash of the unencrypted data bytes.
        /// </summary>
        public IEnumerable<byte> Hash { get; private set; }

        /// <summary>
        /// Gets a value indicating the salt for encryption/decryption and hashing.
        /// </summary>
        public IEnumerable<byte> Salt { get; private set; }

        /// <summary>
        /// Method setting the instance properties to empty.
        /// </summary>
        public void Clear()
        {
            this.Blob = Enumerable.Empty<byte>();
            this.Hash = Enumerable.Empty<byte>();
            this.Salt = Enumerable.Empty<byte>();
        }

        /// <summary>
        /// Copies <see cref="Blob"/> to <paramref name="destination"/>.
        /// </summary>
        /// <param name="destination">The destination array of bytes large enough to hold <see cref="Blob"/>.</param>
        /// <param name="undoOnFailure">If <see langref="true"/>, execute a constrained copy will full undo if the copy fails.</param>
        public void CopyTo(byte[] destination, bool undoOnFailure = false)
        {
            if (undoOnFailure)
            {
                Array.ConstrainedCopy(this.Blob.ToArray(), 0, destination, 0, this.Blob.Count());
            }
            else
            {
                Array.Copy(this.Blob.ToArray(), destination, this.Blob.Count());
            }
        }

        /// <summary>
        /// Copies <see cref="Blob"/> to <paramref name="destination"/> starting at <paramref name="destinationIndex"/>.
        /// </summary>
        /// <param name="destination">The destination array of bytes large enough to hold <see cref="Blob"/> starting at <paramref name="destinationIndex"/>..</param>
        /// <param name="destinationIndex">The index in <paramref name="destination"/> that byte[0] of <see cref="Blob"/> is copied to.</param>
        /// <param name="undoOnFailure">If <see langref="true"/>, execute a constrained copy will full undo if the copy fails.</param>
        public void CopyTo(byte[] destination, int destinationIndex, bool undoOnFailure = false)
        {
            if (undoOnFailure)
            {
                Array.ConstrainedCopy(this.Blob.ToArray(), 0, destination, destinationIndex, this.Blob.Count());
            }
            else
            {
                Array.Copy(this.Blob.ToArray(), 0, destination, destinationIndex, this.Blob.Count());
            }
        }

        /// <summary>
        /// Method returning the sum of the lengths of the <see cref="Blob"/>, <see cref="Hash"/>, <see cref="Salt"/>.
        /// </summary>
        /// <returns>Returns the sum of the three property <see cref="Count()"/> values.</returns>
        public int Count()
        {
            return this.Blob.Count() + this.Hash.Count() + this.Salt.Count();
        }

        /// <summary>
        /// Method decrypting the <see cref="Blob"/> and returning it as <paramref name="data"/>.
        /// </summary>
        /// <param name="key">The symmetric key necessary to decrypt the data.</param>
        /// <param name="data">The data returned after decryption.</param>
        /// <returns><see langref="true"/> if decryption was successful; otherwise <see langref="false"/>.</returns>
        public bool Decrypt(byte[] key, out byte[] data)
        {
            return SymmetricCipher.Decrypt(this.Blob.ToArray(), key, out data);
        }

        /// <summary>
        /// Encrypts new <paramref name="data"/> to this instance of the <see cref="SecureBlob"/> using <paramref name="key"/> and creating salt with length <paramref name="saltLength"/>.
        /// </summary>
        /// <param name="key">The symmetric key necessary to decrypt the data.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="saltLength"></param>
        /// <returns><see langref="true"/> if decryption was successful; otherwise <see langref="false"/>.</returns>
        public bool Encrypt(byte[] data, byte[] key, int saltLength)
        {
            var saltBytes = CipherHash.GetSalt(key, saltLength);

            return this.Encrypt(data, key, saltBytes);
        }

        /// <summary>
        /// Encrypts new <paramref name="data"/> to this instance of the <see cref="SecureBlob"/> using <paramref name="key"/> and <paramref name="salt"/>.
        /// </summary>
        /// <param name="key">The symmetric key necessary to decrypt the data.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="salt">The salt for encryption/decryption and hashing</param>
        /// <returns><see langref="true"/> if decryption was successful; otherwise <see langref="false"/>.</returns>
        public bool Encrypt(byte[] data, byte[] key, byte[] salt)
        {
            this.Clear();

            this.Salt = salt.ToList();

            bool result = SymmetricCipher.Encrypt(data, key, salt, out byte[] secret);
            this.Blob = result ? secret.ToList() : Enumerable.Empty<byte>();

            this.Hash = ByteArrayTool.ComputeHash<SHA256Cng>(data, salt).ToList();

            return result;
        }

        /// <summary>
        /// Encrypts new <paramref name="data"/> to this instance of the <see cref="SecureBlob"/> using <paramref name="password"/> and creating a salt of length <paramref name="saltLength"/>.
        /// </summary>
        /// <param name="password">The symmetric key necessary to decrypt the data.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="saltLength"></param>
        /// <returns><see langref="true"/> if decryption was successful; otherwise <see langref="false"/>.</returns>
        public bool Encrypt(byte[] data, string password, int saltLength)
        {
            var saltBytes = CipherHash.GetSalt(password, saltLength);

            return this.Encrypt(data, password, saltBytes);
        }

        /// <summary>
        /// Encrypts new <paramref name="data"/> to this instance of the <see cref="SecureBlob"/> using <paramref name="password"/> and <paramref name="salt"/>.
        /// </summary>
        /// <param name="password">The symmetric key necessary to decrypt the data.</param>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="salt">The salt for encryption/decryption and hashing</param>
        /// <returns><see langref="true"/> if decryption was successful; otherwise <see langref="false"/>.</returns>
        public bool Encrypt(byte[] data, string password, byte[] salt)
        {
            this.Clear();

            this.Salt = salt.ToList();

            bool result = SymmetricCipher.Encrypt(data, password, salt, out byte[] secret);
            this.Blob = result ? secret.ToList() : Enumerable.Empty<byte>();

            this.Hash = ByteArrayTool.ComputeHash<SHA256Cng>(data, salt).ToList();

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SecureBlob other)
        {
            return this.Equals(this, other);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public bool Equals(SecureBlob left, SecureBlob right)
        {
            if ((left is null) && (right is null))
            {
                return true;
            }
            else if ((left is null) ^ (right is null))
            {
                return false;
            }
            else if (left.Count() != right.Count())
            {
                return false;
            }
            else if (this.GetHashCode(left) != this.GetHashCode(right))
            {
                return false;
            }
            else if (!left.Salt.SequenceEqual(right.Salt))
            {
                // not encrypted with the same salt
                return false;
            }
            else
            {
                return left.Hash.SequenceEqual(right.Hash);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public int GetHashCode(SecureBlob instance)
        {
            List<int> hashList = new List<int>()
            {
                this.Hash.GetHashCode(),
                this.Salt.GetHashCode()
            };

            int Sum(int hash1, int hash2)
            {
                const int HashMultiplier = 17;

                unchecked
                {
                    return hash1 * HashMultiplier + hash2;
                }
            }

            return hashList.Aggregate(Sum);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            IFormatProvider provider = CultureInfo.CurrentCulture;
            string propertyFormat = "{0:-25} = {1}";

            StringBuilder builder = new StringBuilder(base.ToString());

            builder.AppendFormat(provider, propertyFormat, nameof(Blob), this.Blob.Count());
            builder.AppendFormat(provider, propertyFormat, nameof(Hash), ByteArrayTool.ToHexString(this.Hash.ToArray(), 0, this.Hash.Count()));
            builder.AppendFormat(provider, propertyFormat, nameof(Salt), ByteArrayTool.ToHexString(this.Salt.ToArray(), 0, this.Salt.Count()));

            return builder.ToString();
        }
    }
}