﻿// ------------------------------------------------------------------------------
// <copyright file="Fail.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-18</date>
// <summary>
//     Description of File Name "Fail.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------


namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Static container class supporting methods to fail for code contract.
    /// </summary>
    public static class Fail
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        public static void AndThrow<TException>()
            where TException : Exception, new()
        {
            throw Activator.CreateInstance<TException>();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        public static void AndThrow<TException>(string message)
            where TException : Exception, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public static void AndThrow<TException>(string message, Exception innerException)
            where TException : Exception, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, innerException);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="parameterName"></param>
        public static void AndThrow<TException>(string message, string parameterName)
            where TException : ArgumentException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, parameterName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="parameterName"></param>
        /// <param name="innerException"></param>
        public static void AndThrow<TException>(string message, string parameterName, Exception innerException)
            where TException : ArgumentException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, parameterName, innerException);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="parameterName"></param>
        /// <param name="actual"></param>
        /// <param name="message"></param>
        public static void AndThrow<TException>(string parameterName, object actual, string message)
            where TException : ArgumentOutOfRangeException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), parameterName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="source"></param>
        public static void AndThrow<TException>(string message, Type source)
            where TException : SecurityException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, source);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="source"></param>
        /// <param name="state"></param>
        public static void AndThrow<TException>(string message, Type source, string state)
            where TException : SecurityException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, source, state);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="hr"></param>
        public static void AndThrow<TException>(string message, int hr)
            where TException : IOException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, hr);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <param name="path"></param>
        public static void AndThrow<TException>(string message, FileInfo path)
            where TException : FileNotFoundException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), message, path.Name);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="parameterName"></param>
        public static void AndThrowIfNull<TException>(string parameterName)
            where TException : ArgumentNullException, new()
        {
            throw (TException)Activator.CreateInstance(typeof(TException), parameterName);
        }

        public static void NotANaturalNumber(string parameterName, int value)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, $"Parameter {parameterName} with {value} is out of range [1, {int.MaxValue}].");
            }
        }

        public static void NotANaturalNumber(long value)
        {
            if (value <= 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is out of range [1, {long.MaxValue}].");
            }
        }

        public static void NotAWholeNumber(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is out of range [0, {int.MaxValue}].");
            }
        }

        public static void NotAWholeNumber(long value)
        {
            if (value < 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is out of range [0, {long.MaxValue}].");
            }
        }

        public static void NotOnExclusiveInterval<TValue>(TValue value, TValue minExclusiveValue, TValue maxExclusiveValue)
            where TValue : IConvertible, IComparable
        {
            if (value.CompareTo(minExclusiveValue) <= 0 || value.CompareTo(maxExclusiveValue) >= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is not on the interval ({minExclusiveValue}, {maxExclusiveValue}).");
            }
        }

        public static void NotOnLeftExclusiveInterval<TValue>(TValue value, TValue minExclusiveValue, TValue maxValue)
            where TValue : IConvertible, IComparable
        {
            if (value.CompareTo(minExclusiveValue) <= 0 || value.CompareTo(maxValue) > 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is not on the interval ({minExclusiveValue}, {maxValue}].");
            }
        }

        public static void NotOnRightExclusiveInterval<TValue>(TValue value, TValue minValue, TValue maxExclusiveValue)
            where TValue : IConvertible, IComparable
        {
            if (value.CompareTo(minValue) < 0 || value.CompareTo(maxExclusiveValue) >= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is not on the interval [{minValue}, {maxExclusiveValue}).");
            }
        }

        public static void NotOnInterval<TValue>(TValue value, TValue minValue, TValue maxValue)
            where TValue : IConvertible, IComparable
        {
            if (value.CompareTo(minValue) < 0 || value.CompareTo(maxValue) > 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Value {value} is not on the interval [{minValue}, {maxValue}].");
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="errorCode"></param>
        public static void OnErrorCode(int errorCode)
        {
            if (WindowsError.Failed(errorCode))
            {
                Marshal.ThrowExceptionForHR(errorCode);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public static void OnLastErrorCode()
        {
            Fail.OnErrorCode(Marshal.GetHRForLastWin32Error());
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reference"></param>
        public static void OnNull<T>(T reference)
            where T : class
        {
            if (reference is null)
            {
                throw new ArgumentNullException(nameof(reference));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="vector"></param>
        public static void OnNull<T>(T[] vector)
        {
            if (vector is null)
            {
                throw new ArgumentNullException(nameof(vector));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        public static void OnNull<T>(ICollection<T> collection)
        {
            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        public static void OnNull<T>(IEnumerable<T> sequence)
        {
            if (sequence is null)
            {
                throw new ArgumentNullException(nameof(sequence));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public static void OnNull<T>(T? value)
            where T : struct, IConvertible
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        public static void OnNullOrEmpty(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        public static void OnNullOrWhiteSpace(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="vector"></param>
        public static void OnNullOrEmpty<T>(T[] vector)
        {
            Fail.OnNull(vector);

            if (vector.Length < 1)
            {
                throw new ArgumentNullException(nameof(vector));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        public static void OnNullOrEmpty<T>(ICollection<T> collection)
        {
            Fail.OnNull(collection);

            if (collection.Count < 1)
            {
                throw new ArgumentNullException(nameof(collection));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        public static void OnNullOrEmpty<T>(IEnumerable<T> sequence)
        {
            Fail.OnNull(sequence);

            if (sequence.Count() < 1)
            {
                throw new ArgumentNullException(nameof(sequence));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        public static void OutOfRange<TValue>(TValue[] vector, int index)
        {
            Fail.OnNullOrEmpty(vector);
            Fail.OutOfRange(index, vector.GetLowerBound(0), vector.GetUpperBound(0));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        public static void OutOfRange<TValue>(TValue[] vector, int startIndex, int length)
        {
            Fail.OnNullOrEmpty(vector);
            Fail.OutOfRange(startIndex, vector.GetLowerBound(0), Math.Min(length - 1, vector.GetUpperBound(0)));
            Fail.OutOfRange(length, 1, vector.GetUpperBound(0) + 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        public static void OutOfRange<TValue>(TValue[] vector, long startIndex, long length)
        {
            Fail.OnNullOrEmpty(vector);
            Fail.OutOfRange(startIndex, 0L, Math.Min(length, vector.LongLength) - 1L);
            Fail.OutOfRange(length, 1L, vector.LongLength);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="index"></param>
        public static void OutOfRange<TValue>(IEnumerable<TValue> sequence, int index)
        {
            Fail.OnNullOrEmpty(sequence);
            Fail.OutOfRange(index, 0, sequence.Count() - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="index"></param>
        public static void OutOfRange<TValue>(IEnumerable<TValue> sequence, long index)
        {
            Fail.OnNullOrEmpty(sequence);
            Fail.OutOfRange(index, 0L, sequence.LongCount() - 1L);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        public static void OutOfRange<TValue>(IEnumerable<TValue> sequence, int startIndex, int count)
        {
            Fail.OnNullOrEmpty(sequence);
            Fail.OutOfRange(startIndex, 0, Math.Min(count, sequence.Count()) - 1);
            Fail.OutOfRange(count, 1, sequence.Count());
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        public static void OutOfRange<TValue>(IEnumerable<TValue> sequence, long startIndex, long count)
        {
            Fail.OnNullOrEmpty(sequence);
            Fail.OutOfRange(startIndex, 0L, Math.Min(count, sequence.LongCount()) - 1);
            Fail.OutOfRange(count, 1L, sequence.LongCount());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="index"></param>
        /// <param name="lower"></param>
        /// <param name="upper"></param>
        public static void OutOfRange(int index, int lower, int upper)
        {
            if (index < lower || index > upper)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range [{lower}, {upper}].");
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="index"></param>
        /// <param name="lower"></param>
        /// <param name="upper"></param>
        public static void OutOfRange(long index, long lower, long upper)
        {
            if (index < lower || index > upper)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Long Index {index} is out of range [{lower}, {upper}].");
            }
        }
    }
}
