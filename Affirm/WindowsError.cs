﻿// ------------------------------------------------------------------------------
// <copyright file="WindowsError.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-22</date>
// <summary>
//     Description of File Name "WindowsError.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    /// <summary>
    /// Static container class of methods to process <c>HRESULT</c>, status code, and Windows error codes.
    /// </summary>
    public static class WindowsError
    {
        /// <summary>
        /// Facility code representing the Windows Operating System.
        /// </summary>
        public const int FacilityWin32 = 0x0007;

        /// <summary>
        /// <c>HRESULT</c>, status code, or Windows Error code indicated 'OK' or 'SUCCESS'.
        /// </summary>
        public const int OK = 0;

        /// <summary>
        /// Combined severity and facility usually associated with an <c>HRESULT</c>.
        /// </summary>
        public const uint SeverityFacilityWin32 = WindowsError.SeverityWin32 | (uint)WindowsError.FacilityWin32;

        /// <summary>
        /// Severity code representing a Windows Operating System error.
        /// </summary>
        public const uint SeverityWin32 = 0x80000000;

        /// <summary>
        /// Mask for the code portion of an <c>HRESULT</c> or status code.
        /// </summary>
        private const int CodeMask = 0xffff;

        /// <summary>
        /// Mask for the facility portion of an <c>HRESULT</c> or status code.
        /// </summary>
        private const int FacilityMask = 0x1fff;

        /// <summary>
        /// Bit which, when set, indicates an error instead of a warning or an informational code.
        /// </summary>
        private const uint SeverityError = 1;

        /// <summary>
        /// Facility bit identifying the facility as belonging to a modern Windows operating system.
        /// </summary>
        private const int FacilityNtBit = 0x10000000;

        /// <summary>
        /// Tests whether the <c>HRESULT</c> or status code indicates failure.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> or status code to test.</param>
        /// <returns>Returns <see langref="true"/> if a failure occurred; otherwise <see langref="false"/>.</returns>
        public static bool Failed(int hr)
        {
            return WindowsError.IsError(hr) || hr < WindowsError.OK;
        }

        /// <summary>
        /// Tests whether a status code is an error.
        /// </summary>
        /// <param name="status">The status code to test.</param>
        /// <returns>Returns <see langref="true"/> if the status code is an error; otherwise <see langref="false"/>.</returns>
        /// <remarks>Probably works for <c>HRESULT</c> too since the bits are identical.</remarks>
        public static bool IsError(int status)
        {
            return WindowsError.ToSeverityFromStatusCode(status) == WindowsError.SeverityError;
        }

        /// <summary>
        /// Create an <c>HRESULT</c> from assembling a <paramref name="severity"/>, <paramref name="facility"/>, and <paramref name="code"/>.
        /// </summary>
        /// <param name="severity">The severity code for the <c>HRESULT</c>.</param>
        /// <param name="facility">The facility code for the <c>HRESULT</c>.</param>
        /// <param name="code">The error code for the <c>HRESULT</c>.</param>
        /// <returns>Returns the assembled <c>HRESULT</c>.</returns>
        public static int MakeHResult(uint severity, int facility, ushort code)
        {
            return (int)(severity << 31) | (facility << 16) | code;
        }

        /// <summary>
        /// Create an status code from assembling a <paramref name="severity"/>, <paramref name="facility"/>, and <paramref name="code"/>.
        /// </summary>
        /// <param name="severity">The severity code for the status code.</param>
        /// <param name="facility">The facility code for the status code.</param>
        /// <param name="code">The error code for the status code.</param>
        /// <returns>Returns the assembled status code.</returns>
        public static int MakeStatusCode(uint severity, int facility, ushort code)
        {
            return WindowsError.MakeHResult(severity, facility, code);
        }

        /// <summary>
        /// Tests whether the <c>HRESULT</c> or status code indicates success.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> or status code to test.</param>
        /// <returns>Returns <see langref="true"/> if a success is detected; otherwise <see langref="false"/>.</returns>
        public static bool Succeeded(int hr)
        {
            return hr >= WindowsError.OK;
        }

        /// <summary>
        /// Extracts the error code from the <c>HRESULT</c>.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> to parse.</param>
        /// <returns>Returns the error code portion of the <c>HRESULT</c>.</returns>
        public static ushort ToCodeFromHResult(int hr)
        {
            return (ushort)(hr & WindowsError.CodeMask);
        }

        /// <summary>
        /// Extracts the error code from the status code.
        /// </summary>
        /// <param name="status">The status code to parse.</param>
        /// <returns>Returns the error code portion of the <c>HRESULT</c>.</returns>
        public static ushort ToCodeFromStatusCode(int status)
        {
            return WindowsError.ToCodeFromHResult(status);
        }

        /// <summary>
        /// Map <c>HRESULT</c> to <see cref="Exception"/>.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> to map.</param>
        /// <returns>Returns the <see cref="Exception"/> corresponding to <paramref name="hr"/>.</returns>
        public static Exception ToExceptionFromHResult(int hr) => Marshal.GetExceptionForHR(hr);

        /// <summary>
        /// Extracts the error code from the <c>HRESULT</c>.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> to parse.</param>
        /// <returns>Returns the facility code portion of the <c>HRESULT</c>.</returns>
        public static int ToFacilityFromHResult(int hr)
        {
            return (hr >> 16) & WindowsError.FacilityMask;
        }

        /// <summary>
        /// Extracts the error code from the status code.
        /// </summary>
        /// <param name="status">The status code to parse.</param>
        /// <returns>Returns the facility code portion of the status code.</returns>
        public static int ToFacilityFromStatusCode(int status)
        {
            return WindowsError.ToFacilityFromHResult(status);
        }

        /// <summary>
        /// Convert last Windows Error to an <c>HRESULT</c>.
        /// </summary>
        /// <returns>Returns the <c>HRESULT</c> corresponding to the last Windows error.</returns>
        public static int ToHResultFromLastWin32Error() => Marshal.GetHRForLastWin32Error();

        /// <summary>
        /// Convert an NT System Error to an <c>HRESULT</c>.
        /// </summary>
        /// <param name="systemError">The <c>NT</c> System Error to convert.</param>
        /// <returns>Returns the <c>HRESULT</c> equivalent to the Windows System Error.</returns>
        public static int ToHResultFromNt(int systemError)
        {
            return systemError | WindowsError.FacilityNtBit;
        }

        /// <summary>
        /// Convert a Windows System Error to an <c>HRESULT</c>.
        /// </summary>
        /// <param name="systemError">The Windows System Error to convert.</param>
        /// <returns>Returns the <c>HRESULT</c> equivalent to the Windows System Error.</returns>
        /// <remarks>The error code is retrieved and assembled with standard severity and facility values.</remarks>
        public static int ToHResultFromWin32(int systemError)
        {
            if (WindowsError.Failed(systemError) || systemError == 0)
            {
                return systemError;
            }
            else
            {
                return WindowsError.MakeHResult(WindowsError.SeverityWin32, WindowsError.FacilityWin32, WindowsError.ToCodeFromHResult(systemError));
            }
        }

        /// <summary>
        /// Extracts the severity code from the <c>HRESULT</c>.
        /// </summary>
        /// <param name="hr">The <c>HRESULT</c> to parse.</param>
        /// <returns>Returns the severity code portion of the <c>HRESULT</c>.</returns>
        public static uint ToSeverityFromHResult(int hr) => ((uint)hr >> 31) & WindowsError.SeverityError;

        /// <summary>
        /// Extracts the severity code from the status code.
        /// </summary>
        /// <param name="status">The status code to parse.</param>
        /// <returns>Returns the severity code portion of the status code.</returns>
        public static uint ToSeverityFromStatusCode(int status) => WindowsError.ToSeverityFromHResult(status);

        /// <summary>
        /// Converts an <c>HRESULT</c>, status code, or Windows System Error into a hex string.
        /// </summary>
        /// <param name="hr"><c>The HRESULT</c>, status code, or Windows System Error to format.</param>
        /// <returns>Returns the formatted hex string for <paramref name="hr"/>.</returns>
        public static string ToString(int hr)
        {
            return string.Format(CultureInfo.InvariantCulture, "0x{0:X8}", hr);
        }
    }
}
