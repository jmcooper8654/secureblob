﻿// ------------------------------------------------------------------------------
// <copyright file="MethodInfoTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-19</date>
// <summary>
//     Description of File Name "MethodInfoTool.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public static class MethodInfoTool
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Assembly GetMethodAssembly() => MethodInfoTool.GetMethodModule().Assembly;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static FileInfo GetMethodAssemblyLocation() => new FileInfo(MethodInfoTool.GetMethodAssembly().Location);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static AssemblyName GetMethodAssemblyName() => MethodInfoTool.GetMethodAssembly().GetName();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Version GetMethodAssemblyVersion() => MethodInfoTool.GetMethodAssemblyName().Version;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Version GetMethodAssemblyFileVersion()
        {
            return new Version(MethodInfoTool.GetMethodAssemblyFileVersionInfo().FileVersion);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static FileVersionInfo GetMethodAssemblyFileVersionInfo()
        {
            return FileVersionInfo.GetVersionInfo(MethodInfoTool.GetMethodAssemblyLocation().FullName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodAssemblyIsDebug()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().IsDebug;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodAssemblyIsPatched()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().IsPatched;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodAssemblyIsPreRelease()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().IsPreRelease;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodAssemblyIsPrivateBuild()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().IsPrivateBuild;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodAssemblyIsSpecialBuild()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().IsSpecialBuild;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetMethodAssemblyProductName()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().ProductName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetMethodAssemblyProductVersion()
        {
            return MethodInfoTool.GetMethodAssemblyFileVersionInfo().ProductVersion;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Type GetMethodDeclaringType() => MethodInfoTool.GetMethodInfo().DeclaringType;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetMethodDeclaringTypeName() => MethodInfoTool.GetMethodInfo().DeclaringType.Name;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MethodBody GetMethodBody() => MethodInfoTool.GetMethodInfo().GetMethodBody();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IList<ExceptionHandlingClause> GetMethodBodyExceptionHandlingClauses() => MethodInfoTool.GetMethodBody().ExceptionHandlingClauses;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IList<LocalVariableInfo> GetMethodBodyLocalVariables() => MethodInfoTool.GetMethodBody().LocalVariables;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static RuntimeMethodHandle GetMethodHandle() => MethodInfoTool.GetMethodInfo().MethodHandle;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo() => MethodBase.GetCurrentMethod() as MethodInfo;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDelegate"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo<TDelegate>(Expression<TDelegate> expression)
        {
            var member = expression.Body as MethodCallExpression;

            return !(member is null) ? member.Method : throw new ArgumentException("Expression {member.ToString()} is not a method.", nameof(expression));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsAbstract() => MethodInfoTool.GetMethodInfo().IsAbstract;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsConstructor() => MethodInfoTool.GetMethodInfo().IsConstructor;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsGenericMethod() => MethodInfoTool.GetMethodInfo().IsGenericMethod;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsPrivate() => MethodInfoTool.GetMethodInfo().IsPrivate;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsPublic() => MethodInfoTool.GetMethodInfo().IsPublic;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsSecurityCritical() => MethodInfoTool.GetMethodInfo().IsSecurityCritical;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsSecuritySafeCritical() => MethodInfoTool.GetMethodInfo().IsSecuritySafeCritical;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsSecurityTransparent() => MethodInfoTool.GetMethodInfo().IsSecurityTransparent;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsStatic() => MethodInfoTool.GetMethodInfo().IsStatic;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetMethodIsVirtual() => MethodInfoTool.GetMethodInfo().IsVirtual;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Module GetMethodModule() => MethodInfoTool.GetMethodInfo().Module;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetMethodName([CallerMemberName] string name = null)
        {
            return MethodInfoTool.GetMethodName(MethodInfoTool.GetMethodInfo(), name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetMethodName(MethodInfo method, [CallerMemberName] string name = null)
        {
            return !(method is null) ? method.Name : name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ParameterInfo> GetMethodParameters() => MethodInfoTool.GetMethodInfo().GetParameters().ToList();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetMethodBaseDefinition() => MethodInfoTool.GetMethodInfo().GetBaseDefinition();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ParameterInfo GetMethodReturnParameter() => MethodInfoTool.GetMethodInfo().ReturnParameter;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Type GetMethodReturnType() => MethodInfoTool.GetMethodInfo().ReturnType;
    }
}
