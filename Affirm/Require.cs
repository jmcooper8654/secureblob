﻿// ------------------------------------------------------------------------------
// <copyright file="Require.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-18</date>
// <summary>
//     Description of File Name "Require.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Security;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Static container class of methods to be called requiring certain conditions before method execution.
    /// </summary>
    public static class Require
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="throwIfFalse"></param>
        /// <param name="message"></param>
        public static void Conform<TException>(bool throwIfFalse, string message)
            where TException : Exception, new()
        {
            Require.Conform<TException>(MethodInfoTool.GetMethodInfo(), throwIfFalse, message);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="method"></param>
        /// <param name="throwIfFalse"></param>
        /// <param name="message"></param>
        public static void Conform<TException>(MethodInfo method, bool throwIfFalse, string message)
            where TException : Exception, new()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="parameter"></param>
        /// <param name="throwIfFalse"></param>
        /// <param name="message"></param>
        public static void Conform<TException>(ParameterInfo parameter, bool throwIfFalse, string message)
            where TException : Exception, new()
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="name"></param>
        /// <param name="throwIfFalse"></param>
        /// <param name="message"></param>
        public static void Conform<TException>(string name, bool throwIfFalse, string message)
            where TException : Exception, new()
        {
            Require.Conform<TException>(MethodInfoTool.GetMethodParameters().Where(p => p.Name.Equals(name)).FirstOrDefault(), throwIfFalse, message);
        }

        /// <summary>
        /// Require that the <paramref name="method"/> have declaring type <paramref name="expected"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="expected">The expected declaring <see cref="Type"/> of <paramref name="method"/>.</param>
        /// <param name="message">Message passed to the <see cref="InvalidCastException"/>.</param>
        /// <exception cref="InvalidCastException">Thrown if <paramref name="expected"/> is not equal to the actual declaring <see cref="Type"/>.</exception>
        public static void MethodHasDeclaringType(MethodInfo method, Type expected, string message)
        {
            if (!method.DeclaringType.Equals(expected))
            {
                Affirm.Fail.AndThrow<InvalidCastException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> have declaring type <paramref name="className"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="className">The class name of the declaring type for <paramref name="method"/>.</param>
        /// <param name="message">Message passed to the <see cref="InvalidCastException"/>.</param>
        public static void MethodHasDeclaringType(MethodInfo method, string className, string message)
        {
            if (!method.DeclaringType.Name.Equals(className))
            {
                Affirm.Fail.AndThrow<InvalidCastException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> have parameter <paramref name="name"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="name">The parameter name of the <see cref="ParameterInfo"/> to test.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        public static void MethodHasParameter(MethodInfo method, string name, string message)
        {
            if (!method.GetParameters().Any(p => p.Name.Equals(name)))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message, name);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> have parameter <paramref name="name"/> with type <paramref name="expected"/>.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="name">The parameter name of the <see cref="ParameterInfo"/> to test.</param>
        /// <param name="expected">The expected type of the <paramref name="name"/> parameter.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        public static void MethodHasParameter(MethodInfo method, string name, Type expected, string message)
        {
            if (method.GetParameters().Any(p => p.Name.Equals(name) && p.ParameterType.Equals(expected)))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message, name);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is abstract.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsAbstract(MethodInfo method, string message)
        {
            if (!method.IsAbstract)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is a constructor.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsConstructor(MethodInfo method, string message)
        {
            if (!method.IsConstructor)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is a generic method.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsGenericMethod(MethodInfo method, string message)
        {
            if (!method.IsGenericMethod)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is private.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsPrivate(MethodInfo method, string message)
        {
            if (!method.IsPrivate)
            {
                Affirm.Fail.AndThrow<ArgumentException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is public.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsPublic(MethodInfo method, string message)
        {
            if (!method.IsPublic)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is marked security critical.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="SecurityException"/>.</param>
        public static void MethodIsSecurityCritical(MethodInfo method, string message)
        {
            if (!method.IsSecurityCritical)
            {
                Affirm.Fail.AndThrow<SecurityException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is marked security safe critical.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="SecurityException"/>.</param>
        public static void MethodIsSecuritySafeCritical(MethodInfo method, string message)
        {
            if (!method.IsSecuritySafeCritical)
            {
                Affirm.Fail.AndThrow<SecurityException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is marked security transparent.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="SecurityException"/>.</param>
        public static void MethodIsSecurityTransparent(MethodInfo method, string message)
        {
            if (!method.IsSecurityTransparent)
            {
                Affirm.Fail.AndThrow<SecurityException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is signed.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        public static void MethodModuleIsSigned(MethodInfo method)
        {
            if (method.Module.GetSignerCertificate() == null)
            {
                Affirm.Fail.AndThrow<SecurityException>($"Module containing {method.Name} is not signed.");
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is signed.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="SecurityException"/>.</param>
        public static void MethodModuleIsSigned(MethodInfo method, string message)
        {
            if (method.Module.GetSignerCertificate() == null)
            {
                Affirm.Fail.AndThrow<SecurityException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is static.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsStatic(MethodInfo method, string message)
        {
            if (!method.IsStatic)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Require that the <paramref name="method"/> is virtual.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="InvalidOperationException"/>.</param>
        public static void MethodIsVirtual(MethodInfo method, string message)
        {
            if (!method.IsVirtual)
            {
                Affirm.Fail.AndThrow<InvalidOperationException>(message);
            }
        }

        /// <summary>
        /// Requires parameter <paramref name="name"/> of <paramref name="method"/> be an input parameter.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="name">The parameter name of the <see cref="ParameterInfo"/> to test.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        public static void ParameterIsIn(MethodInfo method, string name, string message)
        {
            if (!method.GetParameters().Any(p => p.IsIn && p.Name.Equals(name)))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message, name);
            }
        }

        /// <summary>
        /// Requires parameter <paramref name="name"/> of <paramref name="method"/> be an optional parameter.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="name">The parameter name of the <see cref="ParameterInfo"/> to test.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        public static void ParameterIsOptional(MethodInfo method, string name, string message)
        {
            if (!method.GetParameters().Any(p => p.IsOptional && p.Name.Equals(name)))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message, name);
            }
        }
    }
}
