﻿// ------------------------------------------------------------------------------
// <copyright file="Ensure.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-19</date>
// <summary>
//     Description of File Name "Ensure.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public class Ensure
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="expected"></param>
        /// <param name="message"></param>
        public static void MethodHasReturnType(MethodInfo method, Type expected, string message)
        {
            if (!method.ReturnType.Equals(expected))
            {
                Fail.AndThrow<InvalidCastException>(message);
            }
        }

        public static void OutIsNotNull<TValue>(ParameterInfo parameter, TValue reference)
            where TValue : class
        {
            if (reference is null)
            {
                Fail.AndThrowIfNull<ArgumentNullException>(parameter.Name);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="expected"></param>
        /// <param name="message"></param>
        public static void ParameterHasReturnType(MethodInfo method, Type expected, string message)
        {
            Ensure.ParameterIsReturnValue(method, message, out ParameterInfo returnValue);

            if (returnValue is null || !returnValue.ParameterType.Equals(expected))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message);
            }
        }

        /// <summary>
        /// Requires parameter <paramref name="name"/> of <paramref name="method"/> be an output parameter.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="name">The parameter name of the <see cref="ParameterInfo"/> to test.</param>
        /// <param name="expected">The expected type of the <paramref name="name"/> parameter.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        public static void ParameterIsOut(MethodInfo method, string name, Type expected, string message)
        {
            if (!method.GetParameters().Any(p => p.IsOut && p.Name.Equals(name) && p.ParameterType.Equals(expected)))
            {
                Affirm.Fail.AndThrow<ArgumentException>(message, name);
            }
        }

        /// <summary>
        /// Requires a parameter of <paramref name="method"/> be a return value.
        /// </summary>
        /// <param name="method">The <see cref="MethodInfo"/> instance to test for requirements.</param>
        /// <param name="message">Message passed to the <see cref="ArgumentException"/>.</param>
        /// <param name="returnValue">Out parameter with the <see cref="ParameterInfo"/> instance for the return value.</param>
        public static void ParameterIsReturnValue(MethodInfo method, string message, out ParameterInfo returnValue)
        {
            returnValue = method.GetParameters().Where(p => p.IsRetval).FirstOrDefault();

            if (returnValue is null)
            {
                Affirm.Fail.AndThrow<ArgumentException>(message);
            }
        }
    }
}
