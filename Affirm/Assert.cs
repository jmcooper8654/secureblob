﻿// ------------------------------------------------------------------------------
// <copyright file="Assert.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-23</date>
// <summary>
//     Description of File Name "Assert.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq.Expressions;
    using System.Runtime.CompilerServices;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Static container class for assertion methods.
    /// </summary>
    public static class Assert
    {
        /// <summary>
        /// Assert <paramref name="unaryAssert"/> is <see langref="true"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="unaryAssert"/>.</typeparam>
        /// <param name="unaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="binaryAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, bool> unaryAssert,
            TValue one,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, bool>> funcExpression = v => unaryAssert(v);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (funcExpression.Compile().Invoke(one))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to false in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message ?? string.Empty),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="binaryAssert"/> is <see langref="true"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="binaryAssert"/>.</typeparam>
        /// <param name="binaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="binaryAssert"/>.</param>
        /// <param name="two">Second <typeparamref name="TValue"/> parameter to <paramref name="binaryAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, bool> binaryAssert,
            TValue one,
            TValue two,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, bool>> funcExpression = (l, r) => binaryAssert(l, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (funcExpression.Compile().Invoke(one, two))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to false in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="tertiaryAssert"/> is <see langref="true"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="tertiaryAssert"/>.</typeparam>
        /// <param name="tertiaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="tertiaryAssert"/>.</param>
        /// <param name="two">Second <typeparamref name="TValue"/> parameter to <paramref name="tertiaryAssert"/>.</param>
        /// <param name="three">Third <typeparamref name="TValue"/> parameter to <paramref name="tertiaryAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, TValue, bool> tertiaryAssert,
            TValue one,
            TValue two,
            TValue three,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, TValue, bool>> funcExpression = (l, c, r) => tertiaryAssert(l, c, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (funcExpression.Compile().Invoke(one, two, three))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to false in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="quadAssert"/> is <see langref="true"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="quadAssert"/>.</typeparam>
        /// <param name="quadAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="two">Second <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="three">Third <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="four">Fourth <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, TValue, TValue, bool> quadAssert,
            TValue one,
            TValue two,
            TValue three,
            TValue four,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, TValue, TValue, bool>> funcExpression = (l, cl, cr, r) => quadAssert(l, cl, cr, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (funcExpression.Compile().Invoke(one, two, three, four))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to false in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="unaryAssert"/> returns <c>HRESULT</c> <see cref="WindowsError.Succeeded(int)"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="unaryAssert"/>.</typeparam>
        /// <param name="unaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one"></param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, int> unaryAssert,
            TValue one,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, int>> funcExpression = v => unaryAssert(v);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (WindowsError.Failed(funcExpression.Compile().Invoke(one)))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to false in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message ?? string.Empty),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="binaryAssert"/> returns <c>HRESULT</c> <see cref="WindowsError.Succeeded(int)"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="binaryAssert"/>.</typeparam>
        /// <param name="binaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="binaryAssert"/>.</param>
        /// <param name="two">Second <typeparamref name="TValue"/> parameter to <paramref name="binaryAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, int> binaryAssert,
            TValue one,
            TValue two,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, int>> funcExpression = (l, r) => binaryAssert(l, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (WindowsError.Failed(funcExpression.Compile().Invoke(one, two)))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to failed in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="tertiaryAssert"/> returns <c>HRESULT</c> <see cref="WindowsError.Succeeded(int)"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="tertiaryAssert"/>.</typeparam>
        /// <param name="tertiaryAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <param name="three"></param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, TValue, int> tertiaryAssert,
            TValue one,
            TValue two,
            TValue three,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, TValue, int>> funcExpression = (l, c, r) => tertiaryAssert(l, c, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (WindowsError.Failed(funcExpression.Compile().Invoke(one, two, three)))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to failed in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }

        /// <summary>
        /// Assert <paramref name="quadAssert"/> returns <c>HRESULT</c> <see cref="WindowsError.Succeeded(int)"/> or throw <see cref="AssertFailedException"/>.
        /// </summary>
        /// <typeparam name="TValue">Value type of the parameters to delegate <paramref name="quadAssert"/>.</typeparam>
        /// <param name="quadAssert">The delegate to be both evaluated and displayed.</param>
        /// <param name="one">First <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="two">Second <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="three">Third <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="four">Fourth <typeparamref name="TValue"/> parameter to <paramref name="quadAssert"/>.</param>
        /// <param name="message">An additional message string to display.</param>
        /// <param name="filePath">The caller file path.</param>
        /// <param name="line">The caller line number.</param>
        /// <param name="name">The caller member name.</param>
        [Conditional("DEBUG")]
        public static void OnAssert<TValue>(
            Func<TValue, TValue, TValue, TValue, int> quadAssert,
            TValue one,
            TValue two,
            TValue three,
            TValue four,
            string message,
            [CallerFilePath] string filePath = null,
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string name = null)
        {
            Expression<Func<TValue, TValue, TValue, TValue, int>> funcExpression = (l, cl, cr, r) => quadAssert(l, cl, cr, r);
            string expressionBody = ((LambdaExpression)funcExpression).Body.ToString().Replace("AndAlso", "&&").Replace("OrElse", "||");

            if (WindowsError.Failed(funcExpression.Compile().Invoke(one, two, three, four)))
            {
                List<string> parts = new List<string>()
                {
                    string.Format(CultureInfo.InvariantCulture, "{0}({1})", Path.GetFileName(filePath), line),
                    string.Format(CultureInfo.InvariantCulture, "{0} {1}", typeof(AssertFailedException).Name, MessageCategoryName.Error.ToString().ToUpperInvariant()),
                    string.Format(CultureInfo.CurrentCulture, "Assert for '{0}' evaluated to failed in {1}.", expressionBody, name),
                    string.Format(CultureInfo.CurrentCulture, message),
                };

                Fail.AndThrow<AssertFailedException>(string.Join(" : ", parts));
            }
        }
    }
}
