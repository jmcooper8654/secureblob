﻿// ------------------------------------------------------------------------------
// <copyright file="RequireFailedException.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-19</date>
// <summary>
//     Description of File Name "RequireFailedException.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Security;
    using System.Text;

    /// <summary>
    /// Class implementing the <see cref="RequireFailedException"/> exception.
    /// </summary>
    [Serializable]
    public class RequireFailedException : ArgumentException
    {
        /// <summary>
        /// Field memory-backing the <see cref="HResult"/> property.
        /// </summary>
        private int hr = 0;

        /// <summary>
        /// Field memory-backing the <see cref="Message"/> property.
        /// </summary>
        private string message = string.Empty;

        /// <summary>
        /// Field memory-backing the <see cref="ParameterName"/> property.
        /// </summary>
        private string parameterName = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireFailedException"/> class.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="line"></param>
        /// <param name="name"></param>
        public RequireFailedException([CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : base()
        {
            this.FilePath = new FileInfo(path);
            this.LineNumber = line;
            this.MemberName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireFailedException"/> class.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="path"></param>
        /// <param name="line"></param>
        /// <param name="name"></param>
        public RequireFailedException(string message, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : base(message)
        {
            this.FilePath = new FileInfo(path);
            this.LineNumber = line;
            this.MemberName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireFailedException"/> class.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        /// <param name="path"></param>
        /// <param name="line"></param>
        /// <param name="name"></param>
        public RequireFailedException(string message, Exception innerException, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : base(message, innerException)
        {
            this.FilePath = new FileInfo(path);
            this.LineNumber = line;
            this.MemberName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireFailedException"/> class.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameterName"></param>
        /// <param name="innerException"></param>
        /// <param name="path"></param>
        /// <param name="line"></param>
        /// <param name="name"></param>
        public RequireFailedException(string message, string parameterName, Exception innerException, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : base(message, parameterName, innerException)
        {
            this.FilePath = new FileInfo(path);
            this.LineNumber = line;
            this.MemberName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireFailedException"/> class.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecuritySafeCritical]
        protected RequireFailedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            info.AddValue(nameof(this.FilePath), this.FilePath.FullName);
            info.AddValue(nameof(this.LineNumber), this.LineNumber);
            info.AddValue(nameof(this.MemberName), this.MemberName);
        }

        /// <summary>
        /// Gets or sets a value indicating the caller file path which is the origin of this exception.
        /// </summary>
        public FileInfo FilePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the <c>HRESULT</c> specific to the base class exception.
        /// </summary>
        public new int HResult
        {
            get
            {
                if (this.hr == 0)
                {
                    this.hr = base.HResult;
                }

                return this.hr;
            }

            set
            {
                this.hr = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the line number in <see cref="FilePath"/> where the exception occurred.
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the member name related to this exception.
        /// </summary>
        public string MemberName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the message text.
        /// </summary>
        public new string Message
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.message))
                {
                    this.message = base.Message;
                }

                return this.message;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.message = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicate the parameter name that is related to this exception.
        /// </summary>
        public string ParameterName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.parameterName))
                {
                    this.parameterName = base.ParamName;
                }

                return this.parameterName;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.parameterName = value;
                }
            }
        }

        /// <summary>
        /// Method that pushes <see cref="RequireFailedException" /> data onto the serialization stream.
        /// </summary>
        /// <param name="info">The serialization information storing information about the <see cref="RequireFailedException"/>.</param>
        /// <param name="context">The serialization stream context object.</param>
        /// <exception cref="ArgumentNullException">Thrown if the <paramref name="info"/> is null.</exception>
        [SecurityCritical]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info is null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            this.FilePath = new FileInfo(info.GetString(nameof(FilePath)));
            this.LineNumber = (int)info.GetValue(nameof(LineNumber), typeof(int));
            this.MemberName = info.GetString(nameof(MemberName));
            base.GetObjectData(info, context);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(base.ToString().TrimEnd(new char[] { '\n' }));

            void MakeOrigin()
            {
                builder.AppendFormat(CultureInfo.InvariantCulture, "{0}({1})", this.FilePath.Name ?? this.MemberName, this.LineNumber);
            }

            void MakeMetadata(string subcategory, MessageCategoryName category)
            {
                builder.Append(' ').Append(':').Append(' ');
                builder.AppendFormat(CultureInfo.InvariantCulture, "0x{0:X8} {1} {2}", this.HResult, nameof(RequireFailedException), category.ToString().ToUpperInvariant());
            }

            void MakeMessage()
            {
                builder.Append(' ').Append(':').Append(' ');
                builder.AppendFormat(CultureInfo.InvariantCulture, "{0}", this.MemberName);
                builder.Append(' ').Append('-').Append('>').Append(' ');
                builder.AppendFormat(CultureInfo.CurrentCulture, "{0}", this.Message);
            }

            builder.AppendLine();
            MakeOrigin();
            MakeMetadata(nameof(RequireFailedException), MessageCategoryName.Error);
            MakeMessage();

            this.Message = builder.ToString();
            return this.Message;
        }
    }
}
