﻿// ------------------------------------------------------------------------------
// <copyright file="MessageCategoryName.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-22</date>
// <summary>
//     Description of File Name "MessageCategoryName.cs" in Project "Affirm":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.CodeContract.Affirm
{
    /// <summary>
    /// Enumeration of message category names.
    /// </summary>
    public enum MessageCategoryName
    {
        /// <summary>
        /// No message category name.
        /// </summary>
        None = 0,

        /// <summary>
        /// Critical error message category name.
        /// </summary>
        Critical,

        /// <summary>
        /// Error message category name.
        /// </summary>
        Error,

        /// <summary>
        /// Warning message category name.
        /// </summary>
        Warning,

        /// <summary>
        /// Verbose message category name.
        /// </summary>
        Verbose,

        /// <summary>
        /// Debug message category name.
        /// </summary>
        Debug,
    }
}
