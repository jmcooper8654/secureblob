﻿// ------------------------------------------------------------------------------
// <copyright file="ByteArrayTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-3</date>
// <summary>
//     Description of File Name "ByteArrayTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    ///
    /// </summary>
    public static class ByteArrayTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THash"></typeparam>
        /// <param name="data"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] ComputeHash<THash>(byte[] data, IEnumerable<byte> salt)
            where THash : HashAlgorithm, new()
        {
            if (GenericTool.IsNullOrDefault(salt))
            {
                throw new ArgumentNullException(nameof(salt));
            }

            if (salt.Count() < 16)
            {
                throw new ArgumentOutOfRangeException(nameof(salt), salt.Count(), $"Salt length {salt.Count()} is out of range [16, {int.MaxValue}).");
            }

            using (MemoryStream ms = new MemoryStream())
            {
                // data to hash includes the salt and the unencrypted data bytes
                ms.Write(salt.ToArray(), 0, salt.Count());

                ms.Write(data, 0, data.Length);

                ms.Position = 0L;

                using (HashAlgorithm hasher = Activator.CreateInstance<THash>())
                {
                    return hasher.ComputeHash(ms);
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(bool value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(char value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(DateTime value)
        {
            return ByteArrayTool.GetBytes(value.Ticks);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(Decimal value)
        {
            return BitConverter.GetBytes(Convert.ToDouble(value));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(double value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(float value)
        {
            return BitConverter.GetBytes(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(int value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(long value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(short value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(uint value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(ulong value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetBytes(ushort value)
        {
            var bytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return bytes;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="base64"></param>
        /// <returns></returns>
        public static byte[] GetBytesBase64String(string base64)
        {
            return Convert.FromBase64String(base64);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static string ToBase64String(byte[] vector)
        {
            return Convert.ToBase64String(vector, Base64FormattingOptions.None);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string ToBase64String(byte[] vector, int offset, int length)
        {
            return Convert.ToBase64String(vector, offset, length, Base64FormattingOptions.None);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool ToBool(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(bool))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            return BitConverter.ToBoolean(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static char ToChar(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(char))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            return BitConverter.ToChar(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(long))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return new DateTime(ticks: BitConverter.ToInt64(bytes, 0));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static decimal ToDecimal(byte[] bytes)
        {
            return new decimal(ByteArrayTool.ToDouble(bytes));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static double ToDouble(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(double))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            return BitConverter.ToDouble(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static float ToFloat(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(float))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            return BitConverter.ToSingle(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static int ToInt(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(int))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static long ToLong(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(long))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToInt64(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static short ToShort(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(short))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToInt16(bytes, 0);
        }

        /// <summary>
        /// Converts the numeric vector of the byte array's elements to the equivalent hexadecimal string representation.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns>Returns a string of hexadecimal pairs separated by hyphens.</returns>
        public static string ToHexString(byte[] vector, int startIndex, int length)
        {
            return BitConverter.ToString(vector, startIndex, length);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <param name="byteEncoding"></param>
        /// <returns></returns>
        public static string ToString(byte[] vector, int index, int length, Encoding byteEncoding)
        {
            return byteEncoding.GetString(vector, index, length);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static uint ToUInt(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(uint))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToUInt32(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static ulong ToULong(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(ulong))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToUInt64(bytes, 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static ushort ToUShort(byte[] bytes)
        {
            if (GenericTool.IsNull(bytes) || bytes.Length < sizeof(ushort))
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            return BitConverter.ToUInt16(bytes, 0);
        }
    }
}