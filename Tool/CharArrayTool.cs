﻿// ------------------------------------------------------------------------------
// <copyright file="CharArrayTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-3</date>
// <summary>
//     Description of File Name "CharArrayTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Static container class with methods for character array manipulation.
    /// </summary>
    public static class CharArrayTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static byte[] GetBytesBase64String(char[] vector, int offset, int length)
        {
            return Convert.FromBase64CharArray(vector, offset, length);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasAlpha(this char[] vector) => vector.Any(c => c.IsAlpha());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasAlphaNumeric(this char[] vector) => vector.Any(c => c.IsAlphaNumeric());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasControl(this char[] vector) => vector.Any(c => c.IsControl());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasDigit(this char[] vector) => vector.Any(c => c.IsDigit());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool HasHexDigit(this char[] vector) => vector.Any(c => c.IsHexDigit());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasLower(this char[] vector) => vector.Any(c => c.IsLower());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasNumber(this char[] vector) => vector.Any(c => c.IsNumber());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool HasPrintable(this char[] vector) => vector.Any(c => c.IsPrintable());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasPunctuation(this char[] vector) => vector.Any(c => c.IsPunctuation());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasSeperator(this char[] vector) => vector.Any(c => c.IsSeparator());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasSymbol(this char[] vector) => vector.Any(c => c.IsSymbol());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasWhiteSpace(this char[] vector) => vector.Any(c => c.IsWhiteSpace());

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsAlpha(this char value) => char.IsLetter(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsAlpha(this char[] vector, int index)
        {
            return !CharArrayTool.IsNullOrEmpty(vector) && index >= vector.GetLowerBound(0) && index <= vector.GetUpperBound(0) && vector[index].IsAlpha();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsAlphaNumeric(this char value) => char.IsLetterOrDigit(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsAlphaNumeric(this char[] vector, int index)
        {
            return !CharArrayTool.IsNullOrEmpty(vector) && index >= vector.GetLowerBound(0) && index <= vector.GetUpperBound(0) && vector[index].IsAlphaNumeric();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsControl(this char value) => char.IsControl(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsControl(this char[] vector, int index)
        {
            return !GenericTool.IndexIsInRange(vector, index) && vector[index].IsControl();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsDigit(this char value) => char.IsDigit(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsDigit(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsDigit();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsGraphic(this char value)
        {
            return value.IsAlphaNumeric() || value.IsPunctuation() || value.IsSymbol();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsGraphic(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsGraphic();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsHexDigit(this char value)
        {
            char[] hex = { 'a', 'A', 'b', 'B', 'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F' };

            return CharArrayTool.IsDigit(value) || hex.Contains(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyHexDigit(this char[] vector) => vector.All(c => c.IsHexDigit());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsHexDigit(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsHexDigit();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsLower(this char value) => char.IsLower(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyLower(this char[] vector) => vector.All(c => c.IsLower());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsLower(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsLower();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value">Character array to be tested.</param>
        /// <returns></returns>
        public static bool IsNumber(this char value) => char.IsNumber(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyNumber(this char[] vector) => vector.All(c => c.IsNumber());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsNumber(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsNumber();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPrintable(this char value)
        {
            return value.IsGraphic() || value.IsWhiteSpace();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyPrintable(this char[] vector) => vector.All(c => c.IsPrintable());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsPrintable(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsPrintable();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPunctuation(this char value) => char.IsPunctuation(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyPunctuation(this char[] vector) => vector.All(c => c.IsPunctuation());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsPunctuation(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsPunctuation();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsSeparator(this char value) => char.IsSeparator(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlySeperator(this char[] vector) => vector.All(c => c.IsSeparator());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsSeparator(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsSeparator();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsSymbol(this char value) => char.IsSymbol(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlySymbol(this char[] vector) => vector.All(c => c.IsSymbol());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsSymbol(this char[] vector, int index)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                return false;
            }

            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsSymbol();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value">Character array to be tested.</param>
        /// <returns></returns>
        public static bool IsUpper(this char value) => char.IsUpper(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool HasUpper(this char[] vector) => vector.Any(c => c.IsUpper());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyUpper(this char[] vector) => vector.All(c => c.IsUpper());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsUpper(this char[] vector, int index)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                return false;
            }

            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsUpper();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsWhiteSpace(this char value) => char.IsWhiteSpace(value);

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyWhiteSpace(this char[] vector) => vector.All(c => c.IsWhiteSpace());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <param name="index">Index into character array <paramref name="vector"/>.</param>
        /// <returns></returns>
        public static bool IsWhiteSpace(this char[] vector, int index)
        {
            return GenericTool.IndexIsInRange(vector, index) && vector[index].IsWhiteSpace();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns><see langref="true"/> if <paramref name="vector"/> is <see langref="null"/> or empty; otherwise <see langref="false"/>.</returns>
        public static bool IsNullOrEmpty(char[] vector)
        {
            return GenericTool.IsNullOrEmpty(vector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be tested.</param>
        /// <returns><see langref="true"/> if <paramref name="collection"/> is <see langref="null"/> or empty; otherwise <see langref="false"/>.</returns>
        public static bool IsNullOrEmpty(IEnumerable<char> collection)
        {
            return CharArrayTool.IsNullOrEmpty(collection.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns><see langref="true"/> if <paramref name="vector"/> is <see langref="null"/>, empty, or all whitespace; otherwise <see langref="false"/>.</returns>
        public static bool IsNullOrWhiteSpace(char[] vector)
        {
            return CharArrayTool.IsNullOrEmpty(vector) || vector.OnlyWhiteSpace();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be tested.</param>
        /// <returns><see langref="true"/> if <paramref name="collection"/> is <see langref="null"/>, empty, or all whitespace; otherwise <see langref="false"/>.</returns>
        public static bool IsNullOrWhiteSpace(IEnumerable<char> collection)
        {
            return CharArrayTool.IsNullOrWhiteSpace(collection.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyAlpha(this char[] vector) => vector.All(c => c.IsAlpha());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyAlphaNumeric(this char[] vector) => vector.All(c => c.IsAlphaNumeric());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyControl(this char[] vector) => vector.All(c => c.IsControl());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be tested.</param>
        /// <returns></returns>
        public static bool OnlyDigit(this char[] vector) => vector.All(c => c.IsDigit());

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be converted.</param>
        /// <param name="byteEncoding"></param>
        /// <returns>Returns a <see cref="byte"/> array with encoding <paramref name="byteEncoding"/> converted from <paramref name="vector"/>.</returns>
        public static byte[] ToArray(char[] vector, Encoding byteEncoding)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            return byteEncoding.GetBytes(vector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be converted.</param>
        /// <param name="byteEncoding"></param>
        /// <returns>Returns a <see cref="byte"/> array with encoding <paramref name="byteEncoding"/> converted from <paramref name="collection"/>.</returns>
        public static byte[] ToArray(IEnumerable<char> collection, Encoding byteEncoding)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            return byteEncoding.GetBytes(collection.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be converted.</param>
        /// <returns>Returns a <see cref="SecureString"/> converted from <paramref name="vector"/>.</returns>
        public static SecureString ToSecureString(char[] vector)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            SecureString result = new SecureString();

            Array.ForEach(vector, c => result.AppendChar(c));
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be tested.</param>
        /// <returns>Returns a <see cref="SecureString"/> converted from <paramref name="collection"/>.</returns>
        public static SecureString ToSecureString(IEnumerable<char> collection)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            SecureString result = new SecureString();

            collection.ToList().ForEach(c => result.AppendChar(c));
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be tested.</param>
        /// <param name="startIndex"></param>
        /// <returns>Returns a <see cref="SecureString"/> converted from <paramref name="collection"/>.</returns>
        public static SecureString ToSecureString(IEnumerable<char> collection, int startIndex)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            SecureString result = new SecureString();

            collection.Skip(startIndex).ToList().ForEach(c => result.AppendChar(c));
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be tested.</param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns>Returns a <see cref="SecureString"/> converted from <paramref name="collection"/>.</returns>
        public static SecureString ToSecureString(IEnumerable<char> collection, int startIndex, int count)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            if (!GenericTool.CountIsInRange(collection.ToArray(), startIndex, count))
            {
                throw new ArgumentOutOfRangeException(nameof(count), count, $"Count {count} is out of range [1, {collection.Count()}).");
            }

            SecureString result = new SecureString();

            collection.Skip(startIndex).Take(count).ToList().ForEach(c => result.AppendChar(c));
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be converted.</param>
        /// <returns>Returns a <see cref="string"/> converted from <paramref name="vector"/>.</returns>
        public static string ToString(char[] vector)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            return new string(vector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be processed.</param>
        /// <returns>Returns a <see cref="string"/> converted from <paramref name="collection"/>.</returns>
        public static string ToString(IEnumerable<char> collection)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            return new string(collection.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be processed.</param>
        /// <param name="startIndex"></param>
        /// <returns>Returns a <see cref="string"/> converted from <p1aramref name="collection"/>.</returns>
        public static string ToString(IEnumerable<char> collection, int startIndex)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            return new string(collection.Skip(startIndex).ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be converted.</param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns>Returns a <see cref="string"/> converted from <paramref name="vector"/>.</returns>
        public static string ToString(char[] vector, int startIndex, int length)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (!GenericTool.StartIndexIsInRange(vector, startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {vector.Length}).");
            }

            if (!GenericTool.CountIsInRange(vector, startIndex, length))
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, $"Length {length} is out of range [1, {vector.Length}).");
            }

            return new string(vector, startIndex, length);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="vector">Character array to be converted.</param>
        /// <returns>Returns a <see cref="StringBuilder"/> converted from <paramref name="vector"/>.</returns>
        public static StringBuilder ToStringBuilder(char[] vector)
        {
            if (CharArrayTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            StringBuilder result = new StringBuilder();

            return result.Append(vector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be converted.</param>
        /// <returns>Returns a <see cref="StringBuilder"/> converted from <paramref name="collection"/>.</returns>
        public static StringBuilder ToStringBuilder(IEnumerable<char> collection)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            StringBuilder result = new StringBuilder();

            return result.Append(collection.ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be converted.</param>
        /// <param name="startIndex"></param>
        /// <returns>Returns a <see cref="StringBuilder"/> converted from <paramref name="collection"/>.</returns>
        public static StringBuilder ToStringBuilder(IEnumerable<char> collection, int startIndex)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            StringBuilder result = new StringBuilder();

            return result.Append(collection.Skip(startIndex).ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection">Character collection to be converted.</param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns>Returns a <see cref="StringBuilder"/> converted from <paramref name="collection"/>.</returns>
        public static StringBuilder ToStringBuilder(IEnumerable<char> collection, int startIndex, int count)
        {
            if (CharArrayTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            if (!GenericTool.CountIsInRange(collection.ToArray(), startIndex, count))
            {
                throw new ArgumentOutOfRangeException(nameof(count), count, $"Count {count} is out of range [1, {collection.Count()}).");
            }

            StringBuilder result = new StringBuilder();

            return result.Append(collection.Skip(startIndex).Take(count).ToArray());
        }
    }
}
