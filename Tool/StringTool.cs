﻿// ------------------------------------------------------------------------------
// <copyright file="StringTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-7</date>
// <summary>
//     Description of File Name "StringTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    ///
    /// </summary>
    public static class StringTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Append(this string source, char value)
        {
            return source.Append(new string(value, 1));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Append(this string source, char value, int count)
        {
            if (count < 1 || count >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count, $"Count {count} is out of range [1, {int.MaxValue}).");
            }

            return source.Append(new string(value, count));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Append(this string source, char[] value)
        {
            return source.Append(new string(value));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Append(this string source, char[] value, int startIndex, int length)
        {
            if (!GenericTool.StartIndexIsInRange(value, startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"StartIndex {startIndex} is out of range [0, Length - 1).");
            }

            if (!GenericTool.CountIsInRange(value, startIndex, length))
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, $"Length {length} is out of range [1, Length).");
            }

            return source.Append(new string(value, startIndex, length));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Append(this string source, string value)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (string.IsNullOrEmpty(value))
            {
                return source;
            }

            return new string(source.Concat(value).ToArray());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="format"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static string AppendFormat(this string source, string format, params object[] arguments)
        {
            return source.AppendFormat(CultureInfo.CurrentCulture, format ?? string.Empty, arguments);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="provider"></param>
        /// <param name="format"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static string AppendFormat(this string source, IFormatProvider provider, string format, params object[] arguments)
        {
            return source.Append(string.Format(provider ?? CultureInfo.CurrentCulture, format ?? string.Empty, arguments));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="index"></param>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public static string BuildFormat(int index, int alignment)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append('{').Append(index);

            if (alignment != 0)
            {
                builder.Append(',').Append(alignment);
            }

            builder.Append('}');

            return builder.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="index"></param>
        /// <param name="alignment"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string BuildFormat(int index, int alignment, string format)
        {
            StringBuilder builder = new StringBuilder(StringTool.BuildFormat(index, alignment).TrimEnd('}'));

            if (!string.IsNullOrWhiteSpace(format))
            {
                builder.Append(':').Append(format);
            }

            builder.Append('}');

            return builder.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="prologue"></param>
        /// <param name="index"></param>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public static string BuildFormat(string prologue, int index, int alignment)
        {
            StringBuilder builder = new StringBuilder();

            if (!string.IsNullOrEmpty(prologue))
            {
                builder.Append(prologue).Append(' ');
            }

            builder.Append('{').Append(index);

            if (alignment != 0)
            {
                builder.Append(',').Append(alignment);
            }

            builder.Append('}');

            return builder.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="prologue"></param>
        /// <param name="index"></param>
        /// <param name="alignment"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string BuildFormat(string prologue, int index, int alignment, string format)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(StringTool.BuildFormat(prologue, index, alignment).TrimEnd('}'));

            if (!string.IsNullOrWhiteSpace(format))
            {
                builder.Append(':').Append(format);
            }

            builder.Append('}');

            return builder.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="prologue"></param>
        /// <param name="index"></param>
        /// <param name="alignment"></param>
        /// <param name="format"></param>
        /// <param name="epilogue"></param>
        /// <returns></returns>
        public static string BuildFormat(string prologue, int index, int alignment, string format, string epilogue)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(StringTool.BuildFormat(prologue, index, alignment).TrimEnd('}'));

            if (!string.IsNullOrWhiteSpace(format))
            {
                builder.Append(':').Append(format);
            }

            builder.Append('}');

            if (!string.IsNullOrEmpty(epilogue))
            {
                builder.Append(' ').Append(epilogue);
            }

            return builder.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THash"></typeparam>
        /// <param name="value"></param>
        /// <param name="byteEncoding"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] ComputeHash<THash>(string value, Encoding byteEncoding, IEnumerable<byte> salt)
            where THash : HashAlgorithm, new()
        {
            byte[] stringBytes = byteEncoding.GetBytes(value);

            return ByteArrayTool.ComputeHash<THash>(stringBytes, salt);
        }

        /// <summary>
        /// Convert the <paramref name="source"/> string from <paramref name="sourceCodePageEncoding"/> to <paramref name="destinationCodePageEncoding"/>.
        /// </summary>
        /// <param name="sourceCodePageEncoding">The current encoding name of <paramref name="source"/>.</param>
        /// <param name="destinationCodePageEncoding">The desired encoding name to convert <paramref name="source"/> to and return.</param>
        /// <param name="source">The string encoded in <paramref name="sourceCodePageEncoding"/>.</param>
        /// <returns>Returns the <paramref name="source"/> string converted to <paramref name="destinationCodePageEncoding"/>.</returns>
        public static string ConvertFrom(Encoding sourceCodePageEncoding, Encoding destinationCodePageEncoding, string source)
        {
            byte[] data = sourceCodePageEncoding.GetBytes(source);
            byte[] converted = Encoding.Convert(sourceCodePageEncoding, destinationCodePageEncoding, data);
            return destinationCodePageEncoding.GetString(converted);
        }

        /// <summary>
        /// Convert the <paramref name="data"/> bytes from <paramref name="sourceCodePageEncoding"/> to <paramref name="destinationCodePageEncoding"/>.
        /// </summary>
        /// <param name="sourceCodePageEncoding">The current encoding name of <paramref name="data"/>.</param>
        /// <param name="destinationCodePageEncoding">The desired encoding name to convert <paramref name="data"/> to and return.</param>
        /// <param name="data">The string encoded in <paramref name="sourceCodePageEncoding"/>.</param>
        /// <returns>Returns the <paramref name="data"/> string converted to <paramref name="destinationCodePageEncoding"/>.</returns>
        public static byte[] ConvertFrom(Encoding sourceCodePageEncoding, Encoding destinationCodePageEncoding, byte[] data)
        {
            return Encoding.Convert(sourceCodePageEncoding, destinationCodePageEncoding, data);
        }

        /// <summary>
        /// Convert the <paramref name="data"/> bytes from <paramref name="sourceEncoding"/> to <paramref name="destinationEncoding"/>.
        /// </summary>
        /// <param name="sourceEncoding">The current encoding name of <paramref name="data"/>.</param>
        /// <param name="destinationEncoding">The desired encoding name to convert <paramref name="data"/> to and return.</param>
        /// <param name="data">The string encoded in <paramref name="sourceEncoding"/>.</param>
        /// <returns>Returns the <paramref name="data"/> string converted to <paramref name="destinationEncoding"/>.</returns>
        public static byte[] ConvertFrom(string sourceEncoding, string destinationEncoding, byte[] data)
        {
            return StringTool.ConvertFrom(Encoding.GetEncoding(sourceEncoding), Encoding.GetEncoding(destinationEncoding), data);
        }

        /// <summary>
        /// Convert the <paramref name="source"/> string from <paramref name="sourceEncoding"/> to <paramref name="destinationEncoding"/>.
        /// </summary>
        /// <param name="source">The string encoded in <paramref name="sourceEncoding"/>.</param>
        /// <param name="sourceEncoding">The current encoding name of <paramref name="source"/>.</param>
        /// <param name="destinationEncoding">The desired encoding name to convert <paramref name="source"/> to and return.</param>
        /// <returns>Returns the <paramref name="source"/> string converted to <paramref name="destinationEncoding"/>.</returns>
        public static string ConvertFrom(string sourceEncoding, string destinationEncoding, string source)
        {
            return StringTool.ConvertFrom(Encoding.GetEncoding(sourceEncoding), Encoding.GetEncoding(destinationEncoding), source);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool CountIsInRange(
            string source,
            int startIndex,
            int length)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (length < 1 || length > source.Length)
            {
                return false;
            }
            else if (length + startIndex > source.Length)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool EndsWith(this string source, char value)
        {
            return !string.IsNullOrEmpty(source) && source.LastOrDefault() == value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Prepend(this string source, char value)
        {
            return source.Prepend(new string(value, 1));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Prepend(this string source, char value, int count)
        {
            if (count < 1 || count >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count, $"Count {count} is out of range [1, {int.MaxValue}).");
            }

            return source.Prepend(new string(value, count));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Prepend(this string source, char[] value)
        {
            return source.Prepend(new string(value));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Prepend(this string source, char[] value, int startIndex, int length)
        {
            if (!GenericTool.StartIndexIsInRange(value, startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"StartIndex {startIndex} is out of range [0, Length - 1).");
            }

            if (!GenericTool.CountIsInRange(value, startIndex, length))
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, $"Length {length} is out of range [1, Length).");
            }

            return source.Prepend(new string(value, startIndex, length));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Prepend(this string source, string value)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (string.IsNullOrEmpty(value))
            {
                return source;
            }

            return source.Insert(0, value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool StartIndexIsInRange(string source, int startIndex)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            return startIndex >= 0 && startIndex < source.Length;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool StartsWith(this string source, char value)
        {
            return !string.IsNullOrEmpty(source) && source.FirstOrDefault() == value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="byteEncoding"></param>
        /// <returns></returns>
        public static byte[] ToArrayEncoded(this string value, Encoding byteEncoding)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return byteEncoding.GetBytes(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] ToArrayFromBase64(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return Convert.FromBase64String(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static StringBuilder ToStringBuilder(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new StringBuilder(source);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static StringBuilder ToStringBuilder(this string source, int startIndex)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (!GenericTool.StartIndexIsInRange(source, startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"StartIndex {startIndex} is out of range [0, {source.Length - 1}).");
            }

            return source.Substring(startIndex).ToStringBuilder();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static StringBuilder ToStringBuilder(this string source, int startIndex, int length)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (!GenericTool.StartIndexIsInRange(source, startIndex))
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, $"StartIndex {startIndex} is out of range [0, {source.Length - 1}).");
            }

            if (!GenericTool.CountIsInRange(source, startIndex, length))
            {
                throw new ArgumentOutOfRangeException(nameof(length), length, $"Length {length} is out of range [1, {source.Length}).");
            }

            return source.Substring(startIndex, length).ToStringBuilder();
        }
    }
}
