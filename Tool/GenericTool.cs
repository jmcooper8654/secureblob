﻿// ------------------------------------------------------------------------------
// <copyright file="GenericTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-4</date>
// <summary>
//     Description of File Name "GenericTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Static container class holding generic methods and extension methods.
    /// </summary>
    public static class GenericTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static bool CountIsInRange<TValue>(
            IEnumerable<TValue> collection,
            int startIndex,
            int count)
        {
            if (GenericTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (count < 1 || count > collection.Count())
            {
                return false;
            }
            else if (count + startIndex > collection.Count())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static bool CountIsInRange<TValue>(
            IEnumerable<TValue> collection,
            long startIndex,
            long count)
        {
            if (count < 1L || count > collection.LongCount())
            {
                return false;
            }
            else if (count + startIndex > collection.LongCount())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="match"></param>
        /// <returns></returns>
        public static bool Contains<TValue>(this IEnumerable<TValue> collection, Func<TValue, bool> match)
        {
            return collection.Any(match);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            int index,
            int startIndex) =>
            GenericTool.IndexIsInRange(collection, index, startIndex, GenericTool.IsNullOrEmpty(collection) ? 0 : collection.Count());

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            int index,
            int startIndex,
            int count)
        {
            if (GenericTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(startIndex),
                    startIndex,
                    $"Start Index {startIndex} is out of range [0, {collection.Count()}).");
            }

            if (!GenericTool.CountIsInRange(collection.ToArray(), startIndex, count))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(count),
                    count,
                    $"Count {count} is out of range [0, {collection.Count()}).");
            }

            return index >= startIndex && index < count + startIndex;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            long index) =>
            GenericTool.IndexIsInRange(collection, index, 0L, GenericTool.IsNullOrEmpty(collection) ? 0L : collection.LongCount());

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            long index,
            long startIndex) =>
            GenericTool.IndexIsInRange(collection, index, startIndex, GenericTool.IsNullOrEmpty(collection) ? 0L : collection.LongCount());

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            long index,
            long startIndex,
            long count)
        {
            if (GenericTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (!GenericTool.StartIndexIsInRange(collection.ToArray(), startIndex))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(startIndex),
                    startIndex,
                    $"Start Index {startIndex} is out of range [0, {collection.LongCount()}).");
            }

            if (!GenericTool.CountIsInRange(collection.ToArray(), startIndex, count))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(count),
                    count,
                    $"Count {count} is out of range [0, {collection.LongCount()}).");
            }

            return index >= startIndex && index < count + startIndex;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="pair"></param>
        /// <returns></returns>
        public static bool IsDefault<TKey, TValue>(KeyValuePair<TKey, TValue> pair) =>
            GenericTool.IsDefault(pair, EqualityComparer<TValue>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="pair"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsDefault<TKey, TValue>(
            KeyValuePair<TKey, TValue> pair,
            IEqualityComparer<TValue> comparer) =>
            (comparer ?? EqualityComparer<TValue>.Default).Equals(pair.Value, default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static bool IsNotNull<TValue>(TValue reference) =>
            typeof(TValue).IsEnum || typeof(TValue).IsValueType || reference != null;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNotNull<TValue>(IEnumerable<TValue> collection) => !GenericTool.IsNull(collection);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNotNull<TValue>(TValue? value)
            where TValue : struct, IComparable, IFormattable, IConvertible => !GenericTool.IsNull(value);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static bool IsNull<TValue>(TValue reference) => !GenericTool.IsNotNull(reference);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNull<TValue>(IEnumerable<TValue> collection) => (collection is null);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNull<TValue>(TValue? value)
            where TValue : struct, IComparable, IFormattable, IConvertible => (value is null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="pointer"></param>
        /// <returns></returns>
        public static bool IsNull(IntPtr pointer) => (pointer == IntPtr.Zero);

        /// <summary>
        ///
        /// </summary>
        /// <param name="pointer"></param>
        /// <returns></returns>
        public unsafe static bool IsNull(void* pointer) => (pointer == null);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(IEnumerable<TValue> collection) =>
            GenericTool.IsNullOrDefault(collection, EqualityComparer<TValue>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(TValue? value)
            where TValue : struct, IComparable, IFormattable, IConvertible =>
            GenericTool.IsNullOrDefault(value, EqualityComparer<TValue?>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(
            TValue? value,
            IEqualityComparer<TValue?> comparer)
            where TValue : struct, IComparable, IFormattable, IConvertible =>
            GenericTool.IsNull(value) || (comparer ?? EqualityComparer<TValue?>.Default).Equals(value, default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TKey, TValue>(
            IEnumerable<KeyValuePair<TKey, TValue>> collection) =>
            GenericTool.IsNullOrDefault(collection.ToDictionary(p => p.Key, p => p.Value), EqualityComparer<TValue>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TKey, TValue>(
            IEnumerable<KeyValuePair<TKey, TValue>> collection,
            IEqualityComparer<TValue> comparer) =>
            GenericTool.IsNullOrDefault(collection.ToDictionary(p => p.Key, p => p.Value), comparer ?? EqualityComparer<TValue>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(
            IEnumerable<TValue> collection,
            IEqualityComparer<TValue> comparer) =>
            GenericTool.IsNullOrEmpty(collection) || collection.All(s => (comparer ?? EqualityComparer<TValue>.Default).Equals(s, default));

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
            where TValue : IEqualityComparer<TValue> => GenericTool.IsNullOrDefault(dictionary, EqualityComparer<TValue>.Default);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TKey, TValue>(
            Dictionary<TKey, TValue> dictionary,
            IEqualityComparer<TValue> comparer) =>
            GenericTool.IsNullOrEmptyValues(dictionary) || dictionary.Values.All(v => (comparer ?? EqualityComparer<TValue>.Default).Equals(v, default));

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<TValue>(IEnumerable<TValue> collection) =>
            GenericTool.IsNull(collection) || !collection.Any();

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> collection) =>
            GenericTool.IsNull(collection) || !collection.Any();

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<TKey, TValue>(IDictionary<TKey, TValue> dictionary) =>
            GenericTool.IsNull(dictionary) || !dictionary.Keys.Any();

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public static bool IsNullOrEmptyValues<TKey, TValue>(IDictionary<TKey, TValue> dictionary) =>
            GenericTool.IsNullOrEmpty(dictionary) || !dictionary.Values.Any();

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool StartIndexIsInRange<TValue>(IEnumerable<TValue> collection, int startIndex)
        {
            if (GenericTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            return startIndex >= 0 && startIndex < collection.Count();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool StartIndexIsInRange<TValue>(IEnumerable<TValue> collection, long startIndex)
        {
            if (GenericTool.IsNullOrEmpty(collection))
            {
                throw new ArgumentNullException(nameof(collection));
            }

            return startIndex >= 0 && startIndex < collection.LongCount();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static Func<TValue, bool> ToFunc<TValue>(Predicate<TValue> predicate)
        {
            return new Func<TValue, bool>(predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="functor"></param>
        /// <returns></returns>
        public static Predicate<TValue> ToPredicate<TValue>(Func<TValue, bool> functor)
        {
            return new Predicate<TValue>(functor);
        }
    }
}
