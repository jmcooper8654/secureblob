﻿// ------------------------------------------------------------------------------
// <copyright file="StringBuilderTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-5</date>
// <summary>
//     Description of File Name "StringBuilderTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public static class StringBuilderTool
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="accumulator"></param>
        /// <returns></returns>
        public static char Aggregate(this StringBuilder builder, Func<char, char, char> accumulator)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(accumulator))
            {
                throw new ArgumentNullException(nameof(accumulator));
            }

            return builder.ToCharArray().Aggregate(accumulator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool All(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return false;
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) != true)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static bool Any(this StringBuilder builder)
        {
            return !StringBuilderTool.IsNullOrEmpty(builder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool Any(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return false;
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="builder"></param>
        /// <param name="condition"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder AppendIfCondition<TValue>(this StringBuilder builder, bool condition, TValue value)
            where TValue : struct, IConvertible
        {
            return GenericTool.IsNotNull(builder) && condition ? builder.Append(value) : builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="condition"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder AppendIfCondition(this StringBuilder builder, bool condition, string value)
        {
            return GenericTool.IsNotNull(builder) && condition ? builder.Append(value) : builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="builder"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static StringBuilder AppendRange<TValue>(this StringBuilder builder, TValue[] vector)
            where TValue : struct, IConvertible
        {
            if (StringBuilderTool.IsNull(builder))
            {
                return builder;
            }

            Array.ForEach(vector, v => builder.Append(v));
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static StringBuilder AppendRange(this StringBuilder builder, string[] vector)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                return builder;
            }

            Array.ForEach(vector, v => builder.Append(v));
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IEnumerable<char> AsEnumerable(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Enumerable.Empty<char>();
            }

            return builder.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static StringBuilder Concat(this StringBuilder first, StringBuilder second)
        {
            if (StringBuilderTool.IsNullOrEmpty(first) && StringBuilderTool.IsNullOrEmpty(second))
            {
                return first;
            }
            else if (!StringBuilderTool.IsNullOrEmpty(first) && StringBuilderTool.IsNullOrEmpty(second))
            {
                return first;
            }
            else if (StringBuilderTool.IsNullOrEmpty(first) && !StringBuilderTool.IsNullOrEmpty(second))
            {
                return second;
            }
            else
            {
                return first.Append(second.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Contains(this StringBuilder builder, char value)
        {
            return builder.Contains(value, EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="value"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool Contains(this StringBuilder builder, char value, IEqualityComparer<char> comparer)
        {
            return !StringBuilderTool.IsNullOrEmpty(builder) && builder.ToCharArray().Contains(value, comparer ?? EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static int Count(this StringBuilder builder) => StringBuilderTool.IsNullOrEmpty(builder) ? 0 : builder.Length;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static int Count(this StringBuilder builder, Func<char, bool> predicate)
        {
            int accumulator = 0;

            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return accumulator;
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    accumulator++;
                }
                else
                {
                    continue;
                }
            }

            return accumulator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static StringBuilder DefaultIfEmpty(this StringBuilder builder) => GenericTool.IsNull(builder) ? builder : builder.Clear().Append(char.MinValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static char ElementAt(this StringBuilder builder, int index)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (index < 0 || index >= builder.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range [0, {builder.Length}).");
            }

            return builder[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static char ElementAtOrDefault(this StringBuilder builder, int index)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return char.MinValue;
            }

            if (index < 0 || index >= builder.Length)
            {
                return char.MinValue;
            }

            return builder[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static IEnumerable<char> Except(this StringBuilder first, StringBuilder second)
        {
            return first.Except(second, EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static IEnumerable<char> Except(this StringBuilder first, StringBuilder second, IEqualityComparer<char> comparer)
        {
            if (StringBuilderTool.IsNullOrEmpty(first))
            {
                throw new ArgumentNullException(nameof(first));
            }

            if (StringBuilderTool.IsNullOrEmpty(second))
            {
                throw new ArgumentNullException(nameof(second));
            }

            return first.ToCharArray().Except(second.ToCharArray(), comparer ?? EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char First(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char First(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    return builder[i];
                }
                else
                {
                    continue;
                }
            }

            return char.MinValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char FirstOrDefault(this StringBuilder builder)
        {
            return StringBuilderTool.IsNullOrEmpty(builder) ? char.MinValue : builder[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char FirstOrDefault(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return char.MinValue;
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    return builder[i];
                }
                else
                {
                    continue;
                }
            }

            return char.MinValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="traversal"></param>
        /// <returns></returns>
        public static int IndexOf(this StringBuilder builder, Func<char, int> traversal)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return -1;
            }

            for (int i = 0; i < builder.Length; i++)
            {
                if (traversal?.Invoke(builder[i]) > -1)
                {
                    return i;
                }
                else
                {
                    continue;
                }
            }

            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="traversal"></param>
        /// <returns></returns>
        public static int IndexLastOf(this StringBuilder builder, Func<char, int> traversal)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return -1;
            }

            for (int i = builder.Length - 1; i >= 0; i--)
            {
                if (traversal?.Invoke(builder[i]) > -1)
                {
                    return i;
                }
                else
                {
                    continue;
                }
            }

            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static bool IsNull(StringBuilder builder)
        {
            return GenericTool.IsNull(builder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(StringBuilder builder)
        {
            return GenericTool.IsNull(builder) || builder.Length < 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(StringBuilder builder)
        {
            return StringBuilderTool.IsNullOrEmpty(builder) || Enumerable.Range(0, builder.Length).All(i => char.IsWhiteSpace(builder[i]));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="outer"></param>
        /// <param name="inner"></param>
        /// <param name="outerKeySelector"></param>
        /// <param name="innerKeySelector"></param>
        /// <param name="resultSelector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Join<TInner, TKey, TResult>(
            this StringBuilder outer,
            IEnumerable<TInner> inner,
            Func<char, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector,
            Func<char, TInner, TResult> resultSelector)
        {
            return outer.Join(inner, outerKeySelector, innerKeySelector, resultSelector, EqualityComparer<TKey>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="outer"></param>
        /// <param name="inner"></param>
        /// <param name="outerKeySelector"></param>
        /// <param name="innerKeySelector"></param>
        /// <param name="resultSelector"></param>
        /// <param name="keyComparer"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Join<TInner, TKey, TResult>(
            this StringBuilder outer,
            IEnumerable<TInner> inner,
            Func<char, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector,
            Func<char, TInner, TResult> resultSelector,
            IEqualityComparer<TKey> keyComparer)
        {
            if (StringBuilderTool.IsNullOrEmpty(outer))
            {
                throw new ArgumentNullException(nameof(outer));
            }

            if (GenericTool.IsNullOrEmpty(inner))
            {
                throw new ArgumentNullException(nameof(inner));
            }

            return outer.ToCharArray().Join(inner, outerKeySelector, innerKeySelector, resultSelector, keyComparer ?? EqualityComparer<TKey>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char Last(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder[builder.Length - 1];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char LastOrDefault(this StringBuilder builder)
        {
            return StringBuilderTool.IsNullOrEmpty(builder) ? char.MinValue : builder[builder.Length - 1];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char LastOrDefault(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return char.MinValue;
            }

            for (int i = builder.Length - 1; i >= 0; i--)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    return builder[i];
                }
                else
                {
                    continue;
                }
            }

            return char.MinValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="builder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder Prepend<TValue>(this StringBuilder builder, TValue value)
            where TValue : struct, IConvertible
        {
            if (GenericTool.IsNull(builder))
            {
                return builder;
            }

            return builder.Insert(0, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static StringBuilder Prepend(this StringBuilder builder, string value)
        {
            if (GenericTool.IsNull(builder))
            {
                return builder;
            }

            return builder.Insert(0, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="format"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static StringBuilder PrependFormat(this StringBuilder builder, string format, params object[] arguments)
        {
            return builder.PrependFormat(CultureInfo.CurrentCulture, format, arguments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="provider"></param>
        /// <param name="format"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static StringBuilder PrependFormat(this StringBuilder builder, IFormatProvider provider, string format, params object[] arguments)
        {
            if (GenericTool.IsNull(builder))
            {
                return builder;
            }

            return builder.Insert(0, string.Format(provider ?? CultureInfo.CurrentCulture, format, arguments));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="builder"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static StringBuilder PrependRange<TValue>(this StringBuilder builder, TValue[] vector)
            where TValue : struct, IConvertible
        {
            if (GenericTool.IsNull(builder))
            {
                return builder;
            }

            Array.Reverse(vector);
            Array.ForEach(vector, v => builder.Prepend(v));
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static StringBuilder PrependRange(this StringBuilder builder, string[] vector)
        {
            if (GenericTool.IsNull(builder))
            {
                return builder;
            }

            Array.Reverse(vector);
            Array.ForEach(vector, v => builder.Prepend(v));
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="builder"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Select<TResult>(this StringBuilder builder, Func<char, TResult> selector)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Enumerable.Empty<TResult>();
            }

            List<TResult> accumulator = new List<TResult>();

            for (int i = 0; i < builder.Length; i++)
            {
                accumulator.Add(selector.Invoke(builder[i]));
            }

            return accumulator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="builder"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Select<TResult>(this StringBuilder builder, Func<char, int, TResult> selector)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Enumerable.Empty<TResult>();
            }

            List<TResult> accumulator = new List<TResult>();

            for (int i = 0; i < builder.Length; i++)
            {
                accumulator.Add(selector.Invoke(builder[i], i));
            }

            return accumulator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static bool SetEqual(this StringBuilder first, StringBuilder second)
        {
            return first.SetEqual(second, EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool SetEqual(this StringBuilder first, StringBuilder second, IEqualityComparer<char> comparer)
        {
            if (first.SequenceEqual(second, comparer ?? EqualityComparer<char>.Default))
            {
                return true;
            }
            else
            {
                var firstSetDifference = first.Except(second, comparer ?? EqualityComparer<char>.Default);
                var secondSetDifference = second.Except(first, comparer ?? EqualityComparer<char>.Default);

                return CharArrayTool.IsNullOrEmpty(firstSetDifference) && CharArrayTool.IsNullOrEmpty(secondSetDifference);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static bool SequenceEqual(this StringBuilder first, StringBuilder second)
        {
            return first.SequenceEqual(second, EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool SequenceEqual(this StringBuilder first, StringBuilder second, IEqualityComparer<char> comparer)
        {
            if (GenericTool.IsNull(first) && GenericTool.IsNull(second))
            {
                return true;
            }
            else if (GenericTool.IsNull(first) ^ GenericTool.IsNull(second))
            {
                return false;
            }
            else if (first.Length != second.Length)
            {
                return false;
            }
            else
            {
                return first.ToCharArray().SequenceEqual(second.ToCharArray(), comparer ?? EqualityComparer<char>.Default);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static StringBuilder SetAt(this StringBuilder builder, char value, int index)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (index < 0 || index >= builder.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range [0, {builder.Length}).");
            }

            builder[index] = value;
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char Single(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }
            else if (builder.Length > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(builder), builder.Length, $"StringBuilder of length {builder.Length} is not a singleton.");
            }
            
            return builder.First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char SingleOrDefault(this StringBuilder builder)
        {
            if (GenericTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }
            else if (builder.Length < 1)
            {
                return char.MinValue;
            }
            else if (builder.Length > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(builder), builder.Length, $"StringBuilder of length {builder.Length} is not a singleton.");
            }
            else
            {
                return builder.FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static StringBuilder Skip(this StringBuilder builder, int count)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            return new StringBuilder(builder.ToString(count, builder.Length - count));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static StringBuilder SkipWhile(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            int skipCount = 0;

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    skipCount = i;
                    continue;
                }
                else
                {
                    break;
                }
            }

            return builder.Skip(skipCount + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static StringBuilder Take(this StringBuilder builder, int count)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            StringBuilder accumulator = new StringBuilder();

            for (int i = 0; i < Math.Min(builder.Length, count); i++)
            {
                accumulator.Append(builder[i]);
            }

            return accumulator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static StringBuilder TakeWhile(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            StringBuilder accumulator = new StringBuilder();

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    accumulator.Append(builder[i]);
                    continue;
                }
                else
                {
                    break;
                }
            }

            return accumulator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static byte[] ToArray(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Array.Empty<byte>();
            }

            return Convert.FromBase64String(builder.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="byteEncoding"></param>
        /// <returns></returns>
        public static byte[] ToArray(this StringBuilder builder, Encoding byteEncoding)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Array.Empty<byte>();
            }

            return byteEncoding.GetBytes(builder.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static char[] ToCharArray(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Array.Empty<char>();
            }

            return builder.ToString().ToCharArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static ISet<char> ToHashSet(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return new HashSet<char>(Enumerable.Empty<char>());
            }

            return new HashSet<char>(builder.ToCharArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IEnumerable<char> ToList(this StringBuilder builder)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return Enumerable.Empty<char>();
            }

            return new List<char>(builder.ToCharArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static string ToString(this StringBuilder builder, int startIndex)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return string.Empty;
            }

            return builder.ToString().Substring(startIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static IEnumerable<char> Union(this StringBuilder first, StringBuilder second)
        {
            return first.Union(second, EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static IEnumerable<char> Union(this StringBuilder first, StringBuilder second, IEqualityComparer<char> comparer)
        {
            if (StringBuilderTool.IsNullOrEmpty(first))
            {
                throw new ArgumentNullException(nameof(first));
            }

            if (StringBuilderTool.IsNullOrEmpty(second))
            {
                throw new ArgumentNullException(nameof(second));
            }

            return first.ToCharArray().Union(second.ToCharArray(), comparer ?? EqualityComparer<char>.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static StringBuilder Where(this StringBuilder builder, Func<char, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            string accumulator = string.Empty;

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i]) == true)
                {
                    accumulator.Append(builder[i]);
                }
                else
                {
                    continue;
                }
            }

            return new StringBuilder(accumulator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static StringBuilder Where(this StringBuilder builder, Func<char, int, bool> predicate)
        {
            if (StringBuilderTool.IsNullOrEmpty(builder))
            {
                return builder;
            }

            string accumulator = string.Empty;

            for (int i = 0; i < builder.Length; i++)
            {
                if (predicate?.Invoke(builder[i], i) == true)
                {
                    accumulator.Append(builder[i]);
                }
                else
                {
                    continue;
                }
            }

            return new StringBuilder(accumulator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSecond"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="resultSelector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Zip<TSecond, TResult>(
            this StringBuilder first,
            IEnumerable<TSecond> second,
            Func<char, TSecond, TResult> resultSelector)
        {
            if (StringBuilderTool.IsNullOrEmpty(first))
            {
                throw new ArgumentNullException(nameof(first));
            }

            if (GenericTool.IsNullOrEmpty(second))
            {
                throw new ArgumentNullException(nameof(second));
            }

            return first.ToCharArray().Zip(second, resultSelector);
        }
    }
}
