﻿// ------------------------------------------------------------------------------
// <copyright file="VectorTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-14</date>
// <summary>
//     Description of File Name "VectorTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    ///
    /// </summary>
    public static class VectorTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool All<TValue>(this TValue[] vector, Func<TValue, bool> predicate)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                return false;
            }

            foreach (var item in vector)
            {
                if (predicate?.Invoke(item) != true)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool Any<TValue>(this TValue[] vector)
        {
            return !(vector is null) && vector.Length > 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool Any<TValue>(this TValue[] vector, Func<TValue, bool> predicate)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                return false;
            }

            foreach (var item in vector)
            {
                if (predicate?.Invoke(item) != true)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static int Count<TValue>(this TValue[] vector, Func<TValue, bool> predicate)
        {
            int counter = 0;

            if (GenericTool.IsNullOrEmpty(vector))
            {
                return counter;
            }

            foreach (var item in vector)
            {
                if (predicate?.Invoke(item) == true)
                {
                    counter++;
                }
            }

            return counter;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool CountIsInRange<TValue>(
            TValue[] vector,
            int startIndex,
            int length)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (length < 1 || length > vector.Length)
            {
                return false;
            }
            else if (length + startIndex > vector.Length)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool CountIsInRange<TValue>(
            TValue[] vector,
            long startIndex,
            long length)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (length < 1L || length > vector.LongLength)
            {
                return false;
            }
            else if (length + startIndex > vector.LongLength)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Contains<TValue>(
            this TValue[] vector,
            TValue value)
        {
            return vector.Contains(value, EqualityComparer<TValue>.Default);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="value"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool Contains<TValue>(
            this TValue[] vector,
            TValue value,
            IEqualityComparer<TValue> comparer)
        {
            return Array.Exists(vector, e => comparer.Equals(e, value));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="match"></param>
        /// <returns></returns>
        public static bool Contains<TValue>(this TValue[] vector, Func<TValue, bool> match)
        {
            return Array.Exists(vector, GenericTool.ToPredicate(match));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            int index) =>
            GenericTool.IndexIsInRange(vector, index, 0);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="collection"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            IEnumerable<TValue> collection,
            int index) =>
            GenericTool.IndexIsInRange(collection, index, 0, GenericTool.IsNullOrEmpty(collection) ? 0 : collection.Count());

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            int index,
            int startIndex) =>
            GenericTool.IndexIsInRange(vector, index, startIndex, GenericTool.IsNullOrEmpty(vector) ? 0 : vector.Length);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            int index,
            int startIndex,
            int length)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (!GenericTool.StartIndexIsInRange(vector, startIndex))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(startIndex),
                    startIndex,
                    $"Start Index {startIndex} is out of range [0, {vector.Count()}).");
            }

            if (!GenericTool.CountIsInRange(vector.ToArray(), startIndex, length))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(length),
                    length,
                    $"Length {length} is out of range [1, {vector.Length}).");
            }

            return index >= startIndex && index < length + startIndex;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            long index) =>
            GenericTool.IndexIsInRange(vector, index, 0L);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            long index,
            long startIndex) =>
            GenericTool.IndexIsInRange(vector, index, startIndex, GenericTool.IsNullOrEmpty(vector) ? 0L : vector.LongLength);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool IndexIsInRange<TValue>(
            TValue[] vector,
            long index,
            long startIndex,
            long length)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (!GenericTool.StartIndexIsInRange(vector, startIndex))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(startIndex),
                    startIndex,
                    $"Start Index {startIndex} is out of range [0, {vector.LongCount()}).");
            }

            if (!GenericTool.CountIsInRange(vector.ToArray(), startIndex, length))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(length),
                    length,
                    $"Length {length} is out of range [0, {vector.LongLength}).");
            }

            return index >= startIndex && index < length + startIndex;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="index"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public static bool IndexIsInRangeByDimension<TValue>(
            TValue[] vector,
            int index,
            int dimension)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (dimension < 0 || dimension >= vector.Rank)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(dimension),
                    dimension,
                    $"Dimension {dimension} is out of range [0, {vector.Rank}).");
            }

            return !GenericTool.IsNullOrEmpty(vector) &&
                index >= vector.GetLowerBound(dimension) &&
                index <= vector.GetUpperBound(dimension);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="indexes"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public static bool IndexIsInRangeByDimension<TValue>(
            TValue[] vector,
            int[] indexes,
            int dimension)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            if (GenericTool.IsNullOrEmpty(indexes))
            {
                throw new ArgumentNullException(nameof(indexes));
            }

            if (indexes.Length - 1 != dimension)
            {
                throw new ArgumentException(
                    $"Length of {nameof(indexes)} with value minus one {indexes.Length - 1} must equal Dimension value {dimension}.",
                    nameof(indexes));
            }
            else
            {
                return Enumerable.Range(0, dimension).All(d => GenericTool.IndexIsInRange(vector, indexes[d], d));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool IsNotNull<TValue>(TValue[] vector) => !GenericTool.IsNull(vector);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool IsNull<TValue>(TValue[] vector) => (vector is null);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(TValue[] vector)
        {
            return GenericTool.IsNullOrDefault(vector, EqualityComparer<TValue>.Default);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static bool IsNullOrDefault<TValue>(TValue[] vector, IEqualityComparer<TValue> comparer)
        {
            return GenericTool.IsNullOrEmpty(vector) ||
                vector.All(s => (comparer ?? EqualityComparer<TValue>.Default).Equals(s, default));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<TValue>(TValue[] vector) =>
            GenericTool.IsNull(vector) || !vector.Any();

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool StartIndexIsInRange<TValue>(TValue[] vector, int startIndex)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            return startIndex >= 0 && startIndex < vector.Length;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static bool StartIndexIsInRange<TValue>(TValue[] vector, long startIndex)
        {
            if (GenericTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            return startIndex >= 0 && startIndex < vector.LongLength;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static string ToString<TValue>(this TValue[] vector)
        {
            StringBuilder builder = new StringBuilder();

            // null case

            // empty case

            // small case

            // general case

            return builder.ToString();
        }
    }
}
