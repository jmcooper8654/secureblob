﻿// ------------------------------------------------------------------------------
// <copyright file="EnumTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-7</date>
// <summary>
//     Description of File Name "EnumTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Authentication;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    ///
    /// </summary>
    public static class EnumTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CipherAlgorithmType ToCipherAlgorithmType(int value)
        {
            return (CipherAlgorithmType)Enum.Parse(typeof(CipherAlgorithmType), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CipherAlgorithmType ToCipherAlgorithmType(long value)
        {
            return (CipherAlgorithmType)Enum.Parse(typeof(CipherAlgorithmType), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryCipherAlgorithmType(string value, out CipherAlgorithmType result)
        {
            return Enum.TryParse(value, ignoreCase: true, out result);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CipherMode ToCipherMode(int value)
        {
            return (CipherMode)Enum.Parse(typeof(CipherMode), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CipherMode ToCipherMode(long value)
        {
            return (CipherMode)Enum.Parse(typeof(CipherMode), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryCipherMode(string value, out CipherMode result)
        {
            return Enum.TryParse(value, ignoreCase: true, out result);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static CipherAlgorithmType ToDefaultCipherAlgorithmType()
        {
            return (CipherAlgorithmType)Enum.Parse(typeof(CipherAlgorithmType), Enum.GetNames(typeof(CipherAlgorithmType)).First(), ignoreCase: true);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static CipherMode ToDefaultCipherMode()
        {
            return (CipherMode)Enum.Parse(typeof(CipherMode), Enum.GetNames(typeof(CipherMode)).First(), ignoreCase: true);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static PaddingMode ToDefaultPaddingMode()
        {
            return (PaddingMode)Enum.Parse(typeof(PaddingMode), Enum.GetNames(typeof(PaddingMode)).First(), ignoreCase: true);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static PaddingMode ToPaddingMode(int value)
        {
            return (PaddingMode)Enum.Parse(typeof(PaddingMode), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static PaddingMode ToPaddingMode(long value)
        {
            return (PaddingMode)Enum.Parse(typeof(PaddingMode), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryPaddingMode(string value, out PaddingMode result)
        {
            return Enum.TryParse(value, ignoreCase: true, out result);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToScalar(CipherAlgorithmType value)
        {
            return (int)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToScalarLong(CipherAlgorithmType value)
        {
            return (long)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToScalar(CipherMode value)
        {
            return (int)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToScalarLong(CipherMode value)
        {
            return (long)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToScalar(PaddingMode value)
        {
            return (int)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToScalarLong(PaddingMode value)
        {
            return (long)Convert.ChangeType(value, value.GetTypeCode());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToString(this CipherAlgorithmType value, string format)
        {
            return value.ToString(format ?? string.Empty);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToString(this CipherMode value, string format)
        {
            return value.ToString(format ?? string.Empty);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToString(this PaddingMode value, string format)
        {
            return value.ToString(format ?? string.Empty);
        }
    }
}
