﻿// ------------------------------------------------------------------------------
// <copyright file="SecureStringTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-3</date>
// <summary>
//     Description of File Name "SecureStringTool.cs" in Project "Tool":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Tool
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    ///
    /// </summary>
    public static class SecureStringTool
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool All(this SecureString value, Func<char, bool> predicate)
        {
            return SecureStringTool.TestPredicateForAll(value, predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Any(this SecureString value)
        {
            return !SecureStringTool.IsNullOrEmpty(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool Any(this SecureString value, Func<char, bool> predicate)
        {
            return SecureStringTool.TestPredicateForAny(value, predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="THash"></typeparam>
        /// <param name="value"></param>
        /// <param name="byteEncoding"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] ComputeHash<THash>(SecureString value, Encoding byteEncoding, IEnumerable<byte> salt)
            where THash : HashAlgorithm, new()
        {
            byte[] hashString(string plainText)
            {
                return StringTool.ComputeHash<THash>(plainText, byteEncoding, salt);
            }

            return SecureStringTool.Transform(value, hashString);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Count(this SecureString value)
        {
            return SecureStringTool.IsNullOrEmpty(value) ? 0 : value.Length;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static int Count(this SecureString value, Func<char, bool> predicate)
        {
            int counter = 0;

            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return counter;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                foreach (var item in Marshal.PtrToStringBSTR(unmanagedBinaryString))
                {
                    if (predicate?.Invoke(item) == true)
                    {
                        counter++;
                    }
                    else
                    {
                        continue;
                    }
                }

                return counter;
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static char ElementAt(this SecureString value, int index)
        {
            return SecureStringTool.Index(value, index);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static char ElementAtOrDefault(this SecureString value, int index)
        {
            return SecureStringTool.IndexOrDefault(value, index);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static char First(this SecureString value)
        {
            return value.ElementAt(0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char First(this SecureString value, Func<char, bool> predicate)
        {
            return value.SkipWhile(predicate).First();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static char FirstOrDefault(this SecureString value)
        {
            return value.ElementAtOrDefault(0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char FirstOrDefault(this SecureString value, Func<char, bool> predicate)
        {
            return value.SkipWhile(predicate).FirstOrDefault();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IndexIsInRange(SecureString value, int index)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return false;
            }
            else
            {
                return index >= 0 && index < value.Length;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(SecureString value)
        {
            return (value is null) || value.Length < 1;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNullOrReadOnly(SecureString value)
        {
            return SecureStringTool.IsNullOrEmpty(value) || value.IsReadOnly();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static char Last(this SecureString value)
        {
            return value.ElementAt(value.Length - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char Last(this SecureString value, Func<char, bool> predicate)
        {
            return value.Reverse().SkipWhile(predicate).First();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static char LastOrDefault(this SecureString value)
        {
            return value.ElementAtOrDefault(value.Length - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char LastOrDefault(this SecureString value, Func<char, bool> predicate)
        {
            return value.Reverse().SkipWhile(predicate).FirstOrDefault();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static SecureString Reverse(this SecureString value)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            SecureString accumulator = new SecureString();
            Array.ForEach(SecureStringTool.ToCharArray(value).Reverse().ToArray(), c => accumulator.AppendChar(c));
            return accumulator;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Select<TResult>(this SecureString value, Func<char, TResult> selector)
            where TResult : struct, IConvertible
        {
            return SecureStringTool.Transform(value, selector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> Select<TResult>(this SecureString value, Func<char, int, TResult> selector)
            where TResult : struct, IConvertible
        {
            return SecureStringTool.Transform(value, selector);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static SecureString SetAt(this SecureString value, int index, char c)
        {
            if (SecureStringTool.IsNullOrReadOnly(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            int lowerBound = 0;
            int upperBound = value.Length - 1;

            if (index < lowerBound || index > upperBound)
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range [{lowerBound}, {upperBound}].");
            }

            value.SetAt(index, c);
            return value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IEnumerable<char> Skip(this SecureString value, int count)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).Skip(count);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> SkipWhile(this SecureString value, Func<char, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).SkipWhile(predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> SkipWhile(this SecureString value, Func<char, int, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).SkipWhile(predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IEnumerable<char> Take(this SecureString value, int count)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).Take(count);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> TakeWhile(this SecureString value, Func<char, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).TakeWhile(predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> TakeWhile(this SecureString value, Func<char, int, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return SecureStringTool.ToCharArray(value).TakeWhile(predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="byteEncoding"></param>
        /// <returns></returns>
        public static byte[] ToArray(this SecureString value, Encoding byteEncoding)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Array.Empty<byte>();
            }
            else
            {
                return byteEncoding.GetBytes(SecureStringTool.ToString(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static char[] ToCharArray(this SecureString value)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Array.Empty<char>();
            }
            else
            {
                return SecureStringTool.ToString(value).ToCharArray();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToString(this SecureString value)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                return Marshal.PtrToStringBSTR(unmanagedBinaryString);
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static SecureString UpdateAt(this SecureString value, int index, char c)
        {
            if (SecureStringTool.IsNullOrReadOnly(value))
            {
                return value;
            }

            int lowerBound = 0;
            int upperBound = value.Length - 1;

            if (index == lowerBound && c == char.MinValue)
            {
                value.RemoveAt(index);
            }
            else if (index == lowerBound)
            {
                value.InsertAt(index, c);
            }
            else if (index > lowerBound && index <= upperBound && c == char.MinValue)
            {
                value.RemoveAt(index);
            }
            else if (index > lowerBound && index <= upperBound)
            {
                value.SetAt(index, c);
            }
            else if ((index == -1 || index == value.Length) && c != char.MinValue)
            {
                value.AppendChar(c);
            }

            return value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> Where(this SecureString value, Func<char, bool> predicate)
        {
            return SecureStringTool.Filter(value, predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IEnumerable<char> Where(this SecureString value, Func<char, int, bool> predicate)
        {
            return SecureStringTool.Filter(value, predicate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        internal static IEnumerable<char> Filter(SecureString value, Func<char, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Enumerable.Empty<char>();
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                List<char> accumulator = new List<char>();

                foreach (var item in Marshal.PtrToStringBSTR(unmanagedBinaryString).ToCharArray())
                {
                    if (predicate?.Invoke(item) == true)
                    {
                        accumulator.Add(item);
                    }
                    else
                    {
                        continue;
                    }
                }

                return accumulator;
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        internal static IEnumerable<char> Filter(SecureString value, Func<char, int, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Enumerable.Empty<char>();
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;
            string temp = string.Empty;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                List<char> accumulator = new List<char>();
                temp = Marshal.PtrToStringBSTR(unmanagedBinaryString);

                for (int i = 0; i < temp.Length; i++)
                {
                    if (predicate?.Invoke(temp[i], i) == true)
                    {
                        accumulator.Add(temp[i]);
                    }
                }

                return accumulator;
            }
            finally
            {
                SecureStringTool.Clear(ref temp);
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        internal static char Index(SecureString value, int index)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;
            char[] temp = Array.Empty<char>();

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                temp = Marshal.PtrToStringBSTR(unmanagedBinaryString).ToCharArray();

                return GenericTool.IndexIsInRange(temp, index) ? temp[index] : throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range."); ;
            }
            finally
            {
                Array.Clear(temp, 0, temp.Length);
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        internal static char IndexOrDefault(SecureString value, int index)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                throw new ArgumentOutOfRangeException(nameof(index), index, $"Index {index} is out of range.");
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;
            char[] temp = Array.Empty<char>();

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                temp = Marshal.PtrToStringBSTR(unmanagedBinaryString).ToCharArray();

                return GenericTool.IndexIsInRange(temp, index) ? temp[index] : char.MinValue;
            }
            finally
            {
                Array.Clear(temp, 0, temp.Length);
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="processor"></param>
        internal static void Process(SecureString value, Action<char> processor)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                Array.ForEach(Marshal.PtrToStringBSTR(unmanagedBinaryString).ToCharArray(), c => processor?.Invoke(c));
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="processor"></param>
        internal static void Process(SecureString value, Action<char, int> processor)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;
            string temp = string.Empty;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                temp = Marshal.PtrToStringBSTR(unmanagedBinaryString);

                for (int i = 0; i < temp.Length; i++)
                {
                    processor?.Invoke(temp[i], i);
                }
            }
            finally
            {
                SecureStringTool.Clear(ref temp);
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="processor"></param>
        internal static void Process(SecureString value, Action<string> processor)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                processor?.Invoke(Marshal.PtrToStringBSTR(unmanagedBinaryString));
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        internal static bool TestPredicateForAll(SecureString value, Func<char, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return false;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                foreach (var item in Marshal.PtrToStringBSTR(unmanagedBinaryString))
                {
                    if (predicate?.Invoke(item) != true)
                    {
                        return false;
                    }
                }

                return true;
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        internal static bool TestPredicateForAny(SecureString value, Func<char, bool> predicate)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return false;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                foreach (var item in Marshal.PtrToStringBSTR(unmanagedBinaryString))
                {
                    if (predicate?.Invoke(item) == true)
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        internal static IEnumerable<TResult> Transform<TResult>(SecureString value, Func<char, TResult> selector)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Enumerable.Empty<TResult>();
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                List<TResult> accumulator = new List<TResult>();

                foreach (var item in Marshal.PtrToStringBSTR(unmanagedBinaryString).ToCharArray())
                {
                    accumulator.Add(selector.Invoke(item));
                }

                return accumulator;
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        internal static IEnumerable<TResult> Transform<TResult>(SecureString value, Func<char, int, TResult> selector)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Enumerable.Empty<TResult>();
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;
            string temp = string.Empty;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);
                temp = Marshal.PtrToStringBSTR(unmanagedBinaryString);
                List<TResult> accumulator = new List<TResult>();

                for (int i = 0; i < temp.Length; i++)
                {
                    accumulator.Add(selector.Invoke(temp[i], i));
                }

                return accumulator;
            }
            finally
            {
                SecureStringTool.Clear(ref temp);
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        internal static TResult Transform<TResult>(SecureString value, Func<string, TResult> selector)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return default;
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                return selector.Invoke(Marshal.PtrToStringBSTR(unmanagedBinaryString));
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="value"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        internal static TResult[] Transform<TResult>(SecureString value, Func<string, TResult[]> selector)
        {
            if (SecureStringTool.IsNullOrEmpty(value))
            {
                return Array.Empty<TResult>();
            }

            IntPtr unmanagedBinaryString = IntPtr.Zero;

            try
            {
                unmanagedBinaryString = Marshal.SecureStringToBSTR(value);

                return selector.Invoke(Marshal.PtrToStringBSTR(unmanagedBinaryString));
            }
            finally
            {
                SecureStringTool.Zero(unmanagedBinaryString);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        private static void Clear(ref string value)
        {
            value = SecureStringTool.Zero(value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="pointer"></param>
        private static void Zero(IntPtr pointer)
        {
            if (pointer != IntPtr.Zero)
            {
                Marshal.ZeroFreeBSTR(pointer);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string Zero(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = string.Empty;
            }
            else
            {
                Array.Clear(value.ToCharArray(), 0, value.Length);
            }

            return value;
        }
    }
}
