﻿// ------------------------------------------------------------------------------
// <copyright file="EnumerableData.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-10</date>
// <summary>
//     Description of File Name "EnumerableData.cs" in Project "UnitTestData":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class EnumerableData<TValue> : VectorData<TValue> where TValue : struct, IConvertible
    {
        /// <summary>
        /// Private, read-only field containing the collection.
        /// </summary>
        private readonly IEnumerable<TValue> collection;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        public EnumerableData()
            : base()
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="collection"></param>
        public EnumerableData(IEnumerable<TValue> collection)
            : base(collection.ToArray())
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="value"></param>
        public EnumerableData(int capacity, TValue value)
            : base(capacity, value)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, Func<TValue> generator)
            : base(capacity, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, double maxValue, Func<double, TValue> generator)
            : base(capacity, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, double minValue, double maxValue, Func<double, double, TValue> generator)
            : base(capacity, minValue, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, int maxValue, Func<int, TValue> generator)
            : base(capacity, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, int minValue, int maxValue, Func<int, int, TValue> generator)
            : base(capacity, minValue, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, long maxValue, Func<long, TValue> generator)
            : base(capacity, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerableData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public EnumerableData(int capacity, long minValue, long maxValue, Func<long, long, TValue> generator)
            : base(capacity, minValue, maxValue, generator)
        {
            this.collection = base.ToList();
        }

        /// <summary>
        /// Get a value indicating the count of items in the collection.
        /// </summary>
        public int Count
        {
            get => this.collection.Count();
        }

        /// <summary>
        /// Get a value indicating the count of items in the collection.
        /// </summary>
        public long LongCount
        {
            get => this.collection.LongCount();
        }

        /// <summary>
        /// Returns the collection as an array.
        /// </summary>
        /// <returns></returns>
        public new TValue[] ToArray()
        {
            return this.collection.ToArray();
        }

        /// <summary>
        /// Returns the collection as a list.
        /// </summary>
        /// <returns></returns>
        public new IEnumerable<TValue> ToList()
        {
            return this.collection;
        }
    }
}
