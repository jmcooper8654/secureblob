﻿// ------------------------------------------------------------------------------
// <copyright file="RandomTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-10</date>
// <summary>
//     Description of File Name "RandomTool.cs" in Project "UnitTestData":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing.Data
{
    using System;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public static class RandomTool
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetBool()
        {
            return BitConverter.ToBoolean(RandomTool.GetBytes(sizeof(bool)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static byte GetByte()
        {
            Random rnd = new Random();

            return (byte)rnd.Next(byte.MinValue, byte.MaxValue + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="capacity"></param>
        /// <returns></returns>
        public static byte[] GetBytes(int capacity)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            Random rnd = new Random();
            byte[] buffer = new byte[capacity];

            rnd.NextBytes(buffer);
            return buffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static char GetChar()
        {
            Random rnd = new Random();

            return (char)rnd.Next(char.MinValue, char.MaxValue + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static char GetChar(Func<char, bool> predicate)
        {
            char c = char.MinValue;

            do
            {
                c = RandomTool.GetChar();
            } while (predicate?.Invoke(c) != true);

            return c;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GetDouble()
        {
            return BitConverter.ToDouble(RandomTool.GetBytes(sizeof(double)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxExclusiveValue"></param>
        /// <returns></returns>
        public static double GetDouble(double maxExclusiveValue)
        {
            Random rnd = new Random();
            return maxExclusiveValue * rnd.NextDouble();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxExclusiveValue"></param>
        /// <returns></returns>
        public static double GetDouble(double minValue, double maxExclusiveValue)
        {
            return RandomTool.GetDouble(maxExclusiveValue - minValue) + minValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static float GetFloat()
        {
            return BitConverter.ToSingle(RandomTool.GetBytes(sizeof(float)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetInteger()
        {
            return RandomTool.GetInteger(int.MinValue, int.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static int GetInteger(Func<int, bool> predicate)
        {
            int i = int.MinValue;

            do
            {
                i = RandomTool.GetInteger();
            } while (predicate?.Invoke(i) != true);

            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static int GetInteger(int maxValue)
        {
            return RandomTool.GetInteger(0, maxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static int GetInteger(int minValue, int maxValue)
        {
            Random rnd = new Random();
            return rnd.Next(minValue, maxValue + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static short GetShort()
        {
            Random rnd = new Random();

            return (short)rnd.Next(short.MinValue, short.MaxValue + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static short GetShort(Func<short, bool> predicate)
        {
            short i = short.MinValue;

            do
            {
                i = RandomTool.GetShort();
            } while (predicate?.Invoke(i) != true);

            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static string GetBase64String(int length, Base64FormattingOptions options = Base64FormattingOptions.None)
        {
            return Convert.ToBase64String(RandomTool.GetBytes(length * sizeof(char)), options);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetHexString(int length)
        {
            return BitConverter.ToString(RandomTool.GetBytes(length * sizeof(char)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <param name="byteEncoding"></param>
        /// <returns></returns>
        public static string GetString(int length, Encoding byteEncoding)
        {
            return byteEncoding.GetString(RandomTool.GetBytes(length * sizeof(char)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static long GetLong()
        {
            return BitConverter.ToInt64(RandomTool.GetBytes(sizeof(long)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static long GetLong(Func<long, bool> predicate)
        {
            long i = long.MinValue;

            do
            {
                i = RandomTool.GetLong();
            } while (predicate?.Invoke(i) != true);

            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static long GetLong(long maxValue)
        {
            Random rnd = new Random();
            return (long)Math.Round((maxValue + 1L) * rnd.NextDouble(), 0, MidpointRounding.ToEven);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static long GetLong(long minValue, long maxValue)
        {
            return RandomTool.GetLong(maxValue + 1L - minValue) + minValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static uint GetUInteger()
        {
            return (uint)RandomTool.GetLong(uint.MinValue, uint.MaxValue + 1L);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static uint GetUInteger(Func<uint, bool> predicate)
        {
            uint i = uint.MinValue;

            do
            {
                i = RandomTool.GetUInteger();
            } while (predicate?.Invoke(i) != true);

            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ulong GetULong()
        {
            return BitConverter.ToUInt64(RandomTool.GetBytes(sizeof(ulong)), 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static ulong GetULong(Func<ulong, bool> predicate)
        {
            ulong i = ulong.MinValue;

            do
            {
                i = RandomTool.GetULong();
            } while (predicate?.Invoke(i) != true);

            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ushort GetUShort()
        {
            return (ushort)RandomTool.GetInteger(ushort.MinValue, ushort.MaxValue + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static ushort GetUShort(Func<ushort, bool> predicate)
        {
            ushort i = ushort.MinValue;

            do
            {
                i = RandomTool.GetUShort();
            } while (predicate?.Invoke(i) != true);

            return i;
        }
    }
}
