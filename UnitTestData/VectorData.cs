﻿// ------------------------------------------------------------------------------
// <copyright file="VectorData.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-9</date>
// <summary>
//     Description of File Name "VectorData.cs" in Project "UnitTestData":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class VectorData<TValue> where TValue : struct, IConvertible
    {
        /// <summary>
        /// Private, read-only field containing the vector.
        /// </summary>
        private readonly TValue[] storage;

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        public VectorData()
        {
            this.storage = Array.Empty<TValue>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="vector"></param>
        public VectorData(TValue[] vector)
        {
            if (VectorTool.IsNullOrEmpty(vector))
            {
                throw new ArgumentNullException(nameof(vector));
            }

            this.storage = new TValue[vector.Length];
            Array.Copy(vector, this.storage, vector.Length);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="value"></param>
        public VectorData(int capacity, TValue value)
            : this(capacity, () => value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, Func<TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int,TValue>(i => generator.Invoke()).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, double maxValue, Func<double, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(maxValue)).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, double minValue, double maxValue, Func<double, double, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(minValue, maxValue)).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, int maxValue, Func<int, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(maxValue)).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, int minValue, int maxValue, Func<int, int, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(minValue, maxValue)).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, long maxValue, Func<long, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(maxValue)).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VectorData{T}"/> class.
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="generator"></param>
        public VectorData(int capacity, long minValue, long maxValue, Func<long, long, TValue> generator)
        {
            if (capacity < 1 || capacity >= int.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), capacity, $"Capacity {capacity} is out of range [1, {int.MaxValue}).");
            }

            this.storage = Enumerable.Range(0, capacity).Select<int, TValue>(i => generator.Invoke(minValue, maxValue)).ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        public int FirstIndex
        {
            get => this.storage.GetLowerBound(0);
        }

        /// <summary>
        /// 
        /// </summary>
        public int LastIndex
        {
            get => this.storage.GetUpperBound(0);
        }

        /// <summary>
        /// 
        /// </summary>
        public int Length
        {
            get => this.storage.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        public long LongLength
        {
            get => this.storage.LongLength;
        }

        /// <summary>
        /// 
        /// </summary>
        public int RandomIndex
        {
            get => RandomTool.GetInteger(this.FirstIndex, this.LastIndex + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TValue[] ToArray()
        {
            return this.storage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TValue> ToList()
        {
            return this.storage.ToList();
        }
    }
}
