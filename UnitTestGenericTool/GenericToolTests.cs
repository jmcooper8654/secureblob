// ------------------------------------------------------------------------------
// <copyright file="GenericToolTests.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-7</date>
// <summary>
//     Description of File Name "GenericToolTests.cs" in Project "UnitTestGenericTool":
// </summary>
// ------------------------------------------------------------------------------

namespace UnitTestGenericTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    using Gunfighter.Gplv3.Testing;
    using Gunfighter.Gplv3.Testing.Data;
    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the static container <see cref="GenericTool"/> class.
    /// </summary>
    [TestClass]
    public class GenericToolTests : UnitTestBase
    {
        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestInitialize()"/></remarks>
        [TestInitialize]
        public override void EachTestInitialize()
        {
            base.EachTestInitialize();
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestCleanup()"/></remarks>
        [TestCleanup]
        public override void EachTestCleanup()
        {
            base.EachTestCleanup();
        }

        [TestMethod]
        public void CountIsInRange_MaxCountFirstStartIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int startIndex = 0;
            int count = collection.Count();

            // Act
            var result = GenericTool.CountIsInRange(
                collection,
                startIndex,
                count);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CountIsInRange_NoCountFirstStartIndexEmptyValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            int startIndex = 0;
            int count = 0;

            // Act
            var result = GenericTool.CountIsInRange(
                collection,
                startIndex,
                count);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CountIsInRange_NoCountFirstStartIndexNullValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = null;
            int startIndex = 0;
            int count = 0;

            // Act
            var result = GenericTool.CountIsInRange(
                collection,
                startIndex,
                count);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_LastIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = collection.Count() - 1;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstIndexEmptyValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            int index = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstIndexNullValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = null;
            int index = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexFirstIndexPoplulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = 0;
            int startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexLastIndexPoplulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = collection.Count() - 1;
            int startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstStartIndexFirstIndexEmptyValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            int index = 0;
            int startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstStartIndexFirstIndexNullValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = null;
            int index = 0;
            int startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_MaxCountFirstStartIndexFirstIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = 0;
            int startIndex = 0;
            int count = collection.Count();

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex,
                count);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_MaxCountFirstStartIndexLastIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int index = collection.Count() - 1;
            int startIndex = 0;
            int count = collection.Count();

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex,
                count);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_NoCountFirstStartIndexFirstIndexEmptyValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            int index = 0;
            int startIndex = 0;
            int count = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex,
                count);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_NoCountFirstStartIndexFirstIndexNullValueCollection_ArgumentNullException()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            int index = 0;
            int startIndex = 0;
            int count = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex,
                count);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexFirstIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            long index = 0;
            long startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexLastIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            long index = collection.Count() - 1;
            long startIndex = 0;

            // Act
            var result = GenericTool.IndexIsInRange(
                collection,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsDefault_DefaultKeyValuePair_IsTrue()
        {
            // Arrange
            KeyValuePair<int, long> pair = new KeyValuePair<int, long>(0, 0L);

            // Act
            var result = GenericTool.IsDefault(
                pair);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsDefault_KeyValuePair_IsFalse()
        {
            // Arrange
            KeyValuePair<int, long> pair = new KeyValuePair<int, long>(0, 1L);

            // Act
            var result = GenericTool.IsDefault(
                pair);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsDefault_DefaultComparerDefaultKeyValuePair_IsTrue()
        {
            // Arrange
            KeyValuePair<int, long> pair = new KeyValuePair<int, long>(0, 0L);
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsDefault(
                pair,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsDefault_DefaultComparerKeyValuePair_IsFalse()
        {
            // Arrange
            KeyValuePair<int, long> pair = new KeyValuePair<int, long>(0, 1L);
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsDefault(
                pair,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNotNull_DefaultReference_IsFalse()
        {
            // Arrange
            string reference = default;

            // Act
            var result = GenericTool.IsNotNull(
                reference);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNotNull_StringReference_IsTrue()
        {
            // Arrange
            string reference = "test";

            // Act
            var result = GenericTool.IsNotNull(
                reference);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_PopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();

            // Act
            var result = GenericTool.IsNotNull(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_EmptyValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();

            // Act
            var result = GenericTool.IsNotNull(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_NullValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = null;

            // Act
            var result = GenericTool.IsNotNull(
                collection);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNotNull_DefaultNullable_IsFalse()
        {
            // Arrange
            int? value = default;

            // Act
            var result = GenericTool.IsNotNull(
                value);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNotNull_ZeroNullable_IsTrue()
        {
            // Arrange
            int? value = 0;

            // Act
            var result = GenericTool.IsNotNull(
                value);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_OneNullable_IsTrue()
        {
            // Arrange
            int? value = 1;

            // Act
            var result = GenericTool.IsNotNull(
                value);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNull_DefaultReference_IsTrue()
        {
            // Arrange
            string reference = default;

            // Act
            var result = GenericTool.IsNull(
                reference);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNull_StringReference_IsFalse()
        {
            // Arrange
            string reference = "test";

            // Act
            var result = GenericTool.IsNull(
                reference);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_PopulatedValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();

            // Act
            var result = GenericTool.IsNull(
                collection);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_EmptyValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();

            // Act
            var result = GenericTool.IsNull(
                collection);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_NullValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = null;

            // Act
            var result = GenericTool.IsNull(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNull_DefaultNullable_IsTrue()
        {
            // Arrange
            int? value = default;

            // Act
            var result = GenericTool.IsNull(
                value);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNull_ZeroNullable_IsFalse()
        {
            // Arrange
            int? value = 0;

            // Act
            var result = GenericTool.IsNull(
                value);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_OneNullable_IsFalse()
        {
            // Arrange
            int? value = 1;

            // Act
            var result = GenericTool.IsNull(
                value);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_PointerZero_IsTrue()
        {
            // Arrange
            IntPtr pointer = IntPtr.Zero;

            // Act
            var result = GenericTool.IsNull(
                pointer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNull_PointerAddress_IsFalse()
        {
            // Arrange
            IntPtr pointer = IntPtr.Zero;

            try
            {
                pointer = Marshal.AllocHGlobal(sizeof(long));

                // Act
                var result = GenericTool.IsNull(pointer);

                // Assert
                Assert.IsFalse(result);
            }
            finally
            {
                if (pointer != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(pointer);
                }
            }
        }

        [TestMethod]
        public unsafe void IsNull_PointerNull_IsTrue()
        {
            // Arrange
            void* pointer = null;

            // Act
            var result = GenericTool.IsNull(
                pointer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public unsafe void IsNull_PointerString_IsFalse()
        {
            // Arrange
            fixed(char* pointer = "test")
            {
                // Act
                var result = GenericTool.IsNull(pointer);

                // Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedDefaultCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, 0)).ToList();

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_EmptyValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_NullValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = null;

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultNullable_IsTrue()
        {
            // Arrange
            int? value = default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_ZeroNullable_IsFalse()
        {
            // Arrange
            int? value = 0;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_OneNullable_IsFalse()
        {
            // Arrange
            int? value = 1;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparableDefaultNullable_IsTrue()
        {
            // Arrange
            int? value = default;
            IEqualityComparer<int?> comparer = EqualityComparer<int?>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparableZeroNullable_IsFalse()
        {
            // Arrange
            int? value = 0;
            IEqualityComparer<int?> comparer = EqualityComparer<int?>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparableOneNullable_IsFalse()
        {
            // Arrange
            int? value = 1;
            IEqualityComparer<int?> comparer = EqualityComparer<int?>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                value,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedDefaultCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, 0)).ToList();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerEmptyValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerNullValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = null;
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                collection,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedValueValueDictionary_IsFalse()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, RandomTool.GetLong() },
                { 1, RandomTool.GetLong() },
                { 2, RandomTool.GetLong() },
                { 3, RandomTool.GetLong() },
                { 4, RandomTool.GetLong() },
                { 5, RandomTool.GetLong() },
                { 6, RandomTool.GetLong() },
                { 7, RandomTool.GetLong() },
                { 8, RandomTool.GetLong() },
                { 9, RandomTool.GetLong() }
            };

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedValueDefaultDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, 0L },
                { 1, 0L },
                { 2, 0L },
                { 3, 0L },
                { 4, 0L },
                { 5, 0L },
                { 6, 0L },
                { 7, 0L },
                { 8, 0L },
                { 9, 0L }
            };

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_EmptyValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_NullValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedValueValueDictionary_IsFalse()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, RandomTool.GetLong() },
                { 1, RandomTool.GetLong() },
                { 2, RandomTool.GetLong() },
                { 3, RandomTool.GetLong() },
                { 4, RandomTool.GetLong() },
                { 5, RandomTool.GetLong() },
                { 6, RandomTool.GetLong() },
                { 7, RandomTool.GetLong() },
                { 8, RandomTool.GetLong() },
                { 9, RandomTool.GetLong() }
            };
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedValueDefaultDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, 0L },
                { 1, 0L },
                { 2, 0L },
                { 3, 0L },
                { 4, 0L },
                { 5, 0L },
                { 6, 0L },
                { 7, 0L },
                { 8, 0L },
                { 9, 0L }
            };
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerEmptyValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerNullValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = null;
            IEqualityComparer<long> comparer = EqualityComparer<long>.Default;

            // Act
            var result = GenericTool.IsNullOrDefault(
                dictionary,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_PopulatedValueCollection_IsFalse()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();

            // Act
            var result = GenericTool.IsNullOrEmpty(
                collection);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_EmptyValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = Enumerable.Empty<int>();

            // Act
            var result = GenericTool.IsNullOrEmpty(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_NullValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = null;

            // Act
            var result = GenericTool.IsNullOrEmpty(
                collection);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_PopulatedValueValueDictionary_IsFalse()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, RandomTool.GetLong() },
                { 1, RandomTool.GetLong() },
                { 2, RandomTool.GetLong() },
                { 3, RandomTool.GetLong() },
                { 4, RandomTool.GetLong() },
                { 5, RandomTool.GetLong() },
                { 6, RandomTool.GetLong() },
                { 7, RandomTool.GetLong() },
                { 8, RandomTool.GetLong() },
                { 9, RandomTool.GetLong() }
            };

            // Act
            var result = GenericTool.IsNullOrEmpty(
                dictionary);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_EmptyValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();

            // Act
            var result = GenericTool.IsNullOrEmpty(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_NullValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();

            // Act
            var result = GenericTool.IsNullOrEmpty(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmptyValues_PopulatedValueValueDictionary_IsFalse()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>()
            {
                { 0, RandomTool.GetLong() },
                { 1, RandomTool.GetLong() },
                { 2, RandomTool.GetLong() },
                { 3, RandomTool.GetLong() },
                { 4, RandomTool.GetLong() },
                { 5, RandomTool.GetLong() },
                { 6, RandomTool.GetLong() },
                { 7, RandomTool.GetLong() },
                { 8, RandomTool.GetLong() },
                { 9, RandomTool.GetLong() }
            };

            // Act
            var result = GenericTool.IsNullOrEmptyValues(
                dictionary);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrEmptyValues_EmptyValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = new Dictionary<int, long>();

            // Act
            var result = GenericTool.IsNullOrEmptyValues(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmptyValues_NullValueValueDictionary_IsTrue()
        {
            // Arrange
            Dictionary<int, long> dictionary = null;

            // Act
            var result = GenericTool.IsNullOrEmptyValues(
                dictionary);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void StartIndexIsInRange_FirstStartIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int startIndex = 0;

            // Act
            var result = GenericTool.StartIndexIsInRange(
                vector,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void StartIndexIsInRange_FirstStartIndexPopulatedValueCollection_IsTrue()
        {
            // Arrange
            IEnumerable<int> collection = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToList();
            int startIndex = 0;

            // Act
            var result = GenericTool.StartIndexIsInRange(
                collection,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StartIndexIsInRange_FirstStartIndexDataRowValueCollection_IsFalse(string value)
        {
            // Arrange
            IEnumerable<int> collection = (value is null) ? null : Enumerable.Empty<int>();
            int startIndex = 0;

            // Act
            var result = GenericTool.StartIndexIsInRange(collection, startIndex);

            // Assert
            Assert.Fail($"{nameof(GenericTool.StartIndexIsInRange)} should have thrown {typeof(ArgumentNullException).Name}.");
        }
    }
}
