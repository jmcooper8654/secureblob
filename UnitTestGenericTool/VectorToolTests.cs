﻿// ------------------------------------------------------------------------------
// <copyright file="VectorToolTests.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-14</date>
// <summary>
//     Description of File Name "VectorToolTests.cs" in Project "UnitTestGenericTool":
// </summary>
// ------------------------------------------------------------------------------

namespace UnitTestGenericTool
{
    using System;
    using System.Collections.Generic;

    using Gunfighter.Gplv3.Testing;
    using Gunfighter.Gplv3.Testing.Data;
    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the static container <see cref="GenericTool"/> class.
    /// </summary>
    [TestClass]
    public class VectorToolTests : UnitTestBase
    {
        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestInitialize()"/></remarks>
        [TestInitialize]
        public override void EachTestInitialize()
        {
            base.EachTestInitialize();
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestCleanup()"/></remarks>
        [TestCleanup]
        public override void EachTestCleanup()
        {
            base.EachTestCleanup();
        }

        [TestMethod]
        public void CountIsInRange_MaxLengthFirstStartIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int startIndex = 0;
            int length = vector.Length;

            // Act
            var result = VectorTool.CountIsInRange(
                vector,
                startIndex,
                length);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CountIsInRange_NoLengthFirstStartIndexEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int startIndex = 0;
            int length = 0;

            // Act
            var result = VectorTool.CountIsInRange(
                vector,
                startIndex,
                length);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CountIsInRange_NoLengthFirstStartIndexNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int startIndex = 0;
            int length = 0;

            // Act
            var result = VectorTool.CountIsInRange(
                vector,
                startIndex,
                length);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_LastIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = vector.Length - 1;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstIndexEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int index = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstIndexNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int index = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexFirstIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = 0;
            int startIndex = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_FirstStartIndexLastIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = vector.Length - 1;
            int startIndex = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstStartIndexFirstIndexEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int index = 0;
            int startIndex = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_FirstStartIndexFirstIndexNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int index = 0;
            int startIndex = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRange_MaxLengthFirstStartIndexFirstIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = 0;
            int startIndex = 0;
            int length = vector.Length;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex,
                length);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRange_MaxLengthFirstStartIndexLastIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = vector.Length - 1;
            int startIndex = 0;
            int length = vector.Length;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex,
                length);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_NoLengthFirstStartIndexFirstIndexEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int index = 0;
            int startIndex = 0;
            int length = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex,
                length);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRange_NoLengthFirstStartIndexFirstIndexNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int index = 0;
            int startIndex = 0;
            int length = 0;

            // Act
            var result = VectorTool.IndexIsInRange(
                vector,
                index,
                startIndex,
                length);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRangeByDimension_DimensionZeroFirstIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = 0;
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                index,
                dimension);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRangeByDimension_DimensionZeroLastIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int index = vector.Length - 1;
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                index,
                dimension);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRangeByDimension_DimensionZeroFirstIndexEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int index = 0;
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                index,
                dimension);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRangeByDimension_DimensionZeroFirstIndexNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int index = 0;
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                index,
                dimension);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IndexIsInRangeByDimension_DimensionZeroPopulatedIndexesPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int[] indexes = new int[] { 0 };
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                indexes,
                dimension);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IndexIsInRangeByDimension_DimensionZeroLastIndexesPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int[] indexes = new int[] { vector.Length - 1 };
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                indexes,
                dimension);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRangeByDimension_DimensionZeroPopulatedIndexesEmptyValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            int[] indexes = new int[] { 0 };
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                indexes,
                dimension);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndexIsInRangeByDimension_DimensionZeroPopulatedIndexesNullValueVector_ArgumentNullException()
        {
            // Arrange
            int[] vector = null;
            int[] indexes = new int[] { 0 };
            int dimension = 0;

            // Act
            var result = VectorTool.IndexIsInRangeByDimension(
                vector,
                indexes,
                dimension);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNotNull_PopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();

            // Act
            var result = VectorTool.IsNotNull(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_EmptyValueVector_IsTrue()
        {
            // Arrange
            int[] vector = Array.Empty<int>();

            // Act
            var result = VectorTool.IsNotNull(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotNull_NullValueVector_IsFalse()
        {
            // Arrange
            int[] vector = null;

            // Act
            var result = VectorTool.IsNotNull(
                vector);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_PopulatedValueVector_IsFalse()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();

            // Act
            var result = VectorTool.IsNull(
                vector);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_EmptyValueVector_IsFalse()
        {
            // Arrange
            int[] vector = Array.Empty<int>();

            // Act
            var result = VectorTool.IsNull(
                vector);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNull_NullValueVector_IsTrue()
        {
            // Arrange
            int[] vector = null;

            // Act
            var result = VectorTool.IsNull(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedValueVector_IsFalse()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_PopulatedDefaultVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, 0)).ToArray();

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_EmptyValueVector_IsTrue()
        {
            // Arrange
            int[] vector = Array.Empty<int>();

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_NullValueVector_IsTrue()
        {
            // Arrange
            int[] vector = null;

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedValueVector_IsFalse()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector,
                comparer);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerPopulatedDefaultVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, 0)).ToArray();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerEmptyValueVector_IsTrue()
        {
            // Arrange
            int[] vector = Array.Empty<int>();
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrDefault_DefaultComparerNullValueVector_IsTrue()
        {
            // Arrange
            int[] vector = null;
            IEqualityComparer<int> comparer = EqualityComparer<int>.Default;

            // Act
            var result = VectorTool.IsNullOrDefault(
                vector,
                comparer);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_PopulatedValueVector_IsFalse()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();

            // Act
            var result = VectorTool.IsNullOrEmpty(
                vector);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_EmptyValueVector_IsTrue()
        {
            // Arrange
            int[] vector = Array.Empty<int>();

            // Act
            var result = VectorTool.IsNullOrEmpty(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNullOrEmpty_NullValueVector_IsTrue()
        {
            // Arrange
            int[] vector = null;

            // Act
            var result = VectorTool.IsNullOrEmpty(
                vector);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void StartIndexIsInRange_FirstStartIndexPopulatedValueVector_IsTrue()
        {
            // Arrange
            int[] vector = (new VectorData<int>(100, () => RandomTool.GetInteger())).ToArray();
            int startIndex = 0;

            // Act
            var result = VectorTool.StartIndexIsInRange(
                vector,
                startIndex);

            // Assert
            Assert.IsTrue(result);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void StartIndexIsInRange_FirstStartIndexNullValueVector_IsFalse(string value)
        {
            // Arrange
            int[] vector = (value is null) ? null : Array.Empty<int>();
            int startIndex = 0;

            // Act
            var result = VectorTool.StartIndexIsInRange(
                vector,
                startIndex);

            // Assert
            Assert.Fail($"{nameof(GenericTool.StartIndexIsInRange)} should have thrown {typeof(ArgumentNullException).Name}.");
        }
    }
}
