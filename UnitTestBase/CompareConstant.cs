﻿// ------------------------------------------------------------------------------
// <copyright file="CompareConstant.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-16</date>
// <summary>
//     Description of File Name "CompareConstant.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    /// <summary>
    /// Enumeration of comparison constants for <see cref="StringAssertExtension.CompareTo(string, string, CompareConstant)"/>.
    /// </summary>
    public enum CompareConstant
    {
        /// <summary>
        /// Constant for less than or equal to comparison.
        /// </summary>
        CompareLessThanOrEqual = -2,

        /// <summary>
        /// Constant for less than comparison.
        /// </summary>
        CommpareLessThan = -1,

        /// <summary>
        /// Constant for equality comparison.
        /// </summary>
        CompareEqual = 0,

        /// <summary>
        /// Constant for greater than comparison.
        /// </summary>
        CompareGreaterThan = 1,

        /// <summary>
        /// Constant for greater than or equal to comparison.
        /// </summary>
        CompareGreaterThanOrEqual = 2,

        /// <summary>
        /// Constant for inequality comparison.
        /// </summary>
        CompareNotEqual = 3,
    }
}