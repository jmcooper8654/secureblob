﻿// ------------------------------------------------------------------------------
// <copyright file="ExceptionTool.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-17</date>
// <summary>
//     Description of File Name "ExceptionTool.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// Static container class holding methods for logging from <see cref="Exception"/>.
    /// </summary>
    public static class ExceptionTool
    {
        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message and appends to <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public static StringBuilder ExceptionBasic(
            this StringBuilder builder,
            Exception exception)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            builder.Clear();

            string exceptionName = exception.GetType().Name;

            builder.AppendFormat(CultureInfo.InvariantCulture, exceptionName);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.InvariantCulture, "0x{0:X8} ERROR", exception.HResult);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.CurrentCulture, exception.Message).AppendLine();

            builder.Append('\t').Append("Exception").Append(" = ").Append(exception.ToString()).AppendLine();

            return builder;
        }

        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message and appends to <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <param name="name">The member name of the caller that threw <paramref name="exception"/>.</param>
        /// <param name="line">The line number of the caller that threw <paramref name="exception"/>.</param>
        /// <param name="path">The source file of the caller that threw <paramref name="exception"/>.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="exception"/> is <see langref="null"/>.</exception>
        public static StringBuilder ExceptionBasic(
            this StringBuilder builder,
            Exception exception,
            string name,
            int line,
            FileInfo path)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            builder.Clear();

            string exceptionName = exception.GetType().Name;

            builder.AppendFormat(CultureInfo.InvariantCulture, "{0}({1})", name, line);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.InvariantCulture, "0x{0:X8} {1} ERROR", exception.HResult, exceptionName);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.CurrentCulture, exception.Message).AppendLine();

            builder.Append('\t').Append("Exception").Append("       = ").Append(exception.ToString()).AppendLine();
            builder.Append('\t').Append("Source File").Append("     = ").Append(path.FullName).AppendLine();
            builder.Append('\t').Append("Line Number").Append("     = ").Append(line).AppendLine();

            return builder;
        }

        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message and appends to <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <param name="name">The member name of the caller that threw <paramref name="exception"/>.</param>
        /// <param name="line">The line number of the caller that threw <paramref name="exception"/>.</param>
        /// <param name="path">The source file of the caller that threw <paramref name="exception"/>.</param>
        /// <param name="message">An additional message to display.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="exception"/> is <see langref="null"/>.</exception>
        public static StringBuilder ExceptionBasic(
            this StringBuilder builder,
            Exception exception,
            string name,
            int line,
            FileInfo path,
            string message)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            builder.Clear();

            string exceptionName = exception.GetType().Name;

            builder.AppendFormat(CultureInfo.InvariantCulture, "{0}({1})", name, line);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.InvariantCulture, "0x{0:X8} {1} ERROR", exception.HResult, exceptionName);
            builder.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.CurrentCulture, exception.Message).AppendLine();

            builder.Append('\t').Append("Exception").Append("       = ").Append(exception.ToString()).AppendLine();

            if (!string.IsNullOrWhiteSpace(message))
            {
                builder.Append('\t').Append("Fail Message").Append("    = ").AppendFormat(CultureInfo.CurrentCulture, message).AppendLine();
            }

            builder.Append('\t').Append("Source File").Append("     = ").Append(path.FullName).AppendLine();
            builder.Append('\t').Append("Line Number").Append("     = ").Append(line).AppendLine();

            return builder;
        }

        /// <summary>
        /// Extract <see cref="Exception"/> data into <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <returns>Returns a <see cref="StringBuilder"/> with <see cref="Exception"/> data formatted and appended to it.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="builder"/> or <paramref name="exception"/> are <see langref="null"/>.</exception>
        public static StringBuilder ExceptionData(this StringBuilder builder, Exception exception)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            string exceptionName = exception.GetType().Name;

            builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "Exception Data for {0}:", exceptionName).AppendLine();
            builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "{0,-15}   {1,-53}", "Key", "Value").AppendLine();
            builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "{0,-15}   {1,-53}", new string('-', 15), new string('-', 53)).AppendLine();

            foreach (DictionaryEntry item in exception.Data)
            {
                builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "{0,-15}   {1,-53:X}", item.Key, item.Value).AppendLine();
            }

            return builder;
        }

        /// <summary>
        /// Extracts the <see cref="Exception"/> detail and formats it to <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <returns>Returns a<see cref= "StringBuilder" /> with <see cref= "Exception" /> detail formatted and appended to it.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="builder"/> or <paramref name="exception"/> are <see langref="null"/>.</exception>
        public static StringBuilder ExceptionDetail(this StringBuilder builder, Exception exception)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            builder.ExceptionBasic(exception);

            builder.Append('\t').Append("Source Name").Append("     = ").Append(exception.Source).AppendLine();
            builder.Append('\t').Append("Target Site").Append("     = ").Append(exception.TargetSite.Name).AppendLine();
            builder.Append('\t').Append("Target Type").Append("     = ").Append(exception.TargetSite.MemberType).AppendLine();

            if (!(exception.InnerException is null))
            {
                builder.Append('\t').Append("Inner Exception").Append(" = ").Append(exception.InnerException.GetType().Name).AppendLine();
            }

            builder.Append('\t').Append("Help Link").Append("       = ").Append(exception.HelpLink).AppendLine();

            return builder;
        }

        /// <summary>
        /// Extracts the <see cref="Exception"/> detail and formats it to <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <param name="related"><see cref="FileInfo"/> related to the <paramref name="exception"/>.</param>
        /// <returns>Returns a<see cref= "StringBuilder" /> with <see cref= "Exception" /> detail formatted and appended to it.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="builder"/> or <paramref name="exception"/> are <see langref="null"/>.</exception>
        public static StringBuilder ExceptionDetail(this StringBuilder builder, Exception exception, FileInfo related)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            builder.ExceptionDetail(exception, related);
            builder.Append('\t').Append("Related File").Append("    = ").Append(related.FullName).AppendLine();

            return builder;
        }

        /// <summary>
        /// Extracts the stack traces and inner exceptions starting with <paramref name="exception"/> as root.
        /// </summary>
        /// <param name="builder">The <see cref="StringBuilder"/> instance.</param>
        /// <param name="exception">The <see cref="Exception"/> to process.</param>
        /// <param name="displayDetail">If <see langref="true"/>, <see cref="ExceptionDetail(StringBuilder, Exception)"/> will be used for each <see cref="Exception"/>.</param>
        /// <returns>Returns a<see cref= "StringBuilder" /> with <see cref= "Exception" /> stack trace and inner exceptions formatted and appended to it.</returns>
        /// <exception cref="ArgumentNullException">Throws if either <paramref name="builder"/> or <paramref name="exception"/> are <see langref="null"/>.</exception>
        public static StringBuilder ExceptionStackTrace(this StringBuilder builder, Exception exception, bool displayDetail)
        {
            if (StringBuilderTool.IsNull(builder))
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            Exception nextException = exception;

            do
            {
                if (displayDetail)
                {
                    builder.ExceptionDetail(nextException);
                }
                else
                {
                    builder.ExceptionBasic(nextException);
                }

                builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "Stack Trace for Exception {0}:", nextException.GetType().Name).AppendLine();
                builder.Append('\t').AppendFormat(CultureInfo.InvariantCulture, "{0}", new string('-', 72)).AppendLine();

                foreach (var item in nextException.StackTrace.Split('\n'))
                {
                    builder.Append('\t').AppendLine(item);
                }

                nextException = nextException.InnerException;
            }
            while (!(nextException is null));

            return builder;
        }
    }
}
