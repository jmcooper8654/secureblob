﻿// ------------------------------------------------------------------------------
// <copyright file="PropertyAssert.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-16</date>
// <summary>
//     Description of File Name "PropertyAssert.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Text;

    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///
    /// </summary>
    public static class PropertyAssert
    {
        /// <summary>
        ///
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        ///
        /// </summary>
        private static StringBuilder buffer = new StringBuilder(InitialCapacity);

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static string AssertFailedMessage(object value, string property)
        {
            buffer.Clear();

            string objectValueName = value.GetType().Name;

            if (value is null)
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null.\n", nameof(value));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is an instance of Type {1}", nameof(value), objectValueName);
            }

            if (string.IsNullOrWhiteSpace(property))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null, empty, or all whitespace.\n", nameof(property));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is not a member of this instance of {1}.\n", nameof(property), objectValueName);
            }

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        /// <returns></returns>
        public static string AssertFailedMessage(object value, string property, Type expected)
        {
            StringBuilder builder = new StringBuilder();
            string objectValueName = value.GetType().Name;

            if (value is null)
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null.\n", nameof(value));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is an instance of Type {1}", nameof(value), objectValueName);
            }

            if (string.IsNullOrWhiteSpace(property))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null, empty, or all whitespace.\n", nameof(property));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} does not have expected Type {1}.\n", nameof(property), expected.Name);
            }

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns></returns>
        public static string AssertFailedMessage<TValue>(object value, string property, TValue expected, TValue actual)
            where TValue : IConvertible
        {
            StringBuilder builder = new StringBuilder();

            if (GenericTool.IsNull(value))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null.\n", nameof(value));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is an instance of Type {1}", nameof(value), value.GetType().Name);
            }

            if (string.IsNullOrWhiteSpace(property))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null, empty, or all whitespace.\n", nameof(property));
            }

            if (GenericTool.IsNull(expected))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null or empty.\n", nameof(expected));
            }

            if (GenericTool.IsNull(actual))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null or empty.\n", nameof(actual));
            }

            buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} = <<{1}>>\n", nameof(expected), expected);
            buffer.AppendFormat(CultureInfo.InvariantCulture, "{0}   = <<{1}>>\n", nameof(actual), actual);
            buffer.AppendFormat(CultureInfo.InvariantCulture, "Expected did not equal Actual\n");

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static string AssertFailedMessage(object value, string property, string expected, string actual, StringComparison comparison)
        {
            StringBuilder builder = new StringBuilder();

            if (value is null)
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null.\n", nameof(value));
            }
            else
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is an instance of Type {1}", nameof(value), value.GetType().Name);
            }

            if (string.IsNullOrWhiteSpace(property))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null, empty, or all whitespace.\n", nameof(property));
            }

            if (string.IsNullOrEmpty(expected))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null or empty.\n", nameof(expected));
            }

            if (string.IsNullOrEmpty(actual))
            {
                buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} is null or empty.\n", nameof(actual));
            }

            buffer.AppendFormat(CultureInfo.InvariantCulture, "{0} = <<{1}>>\n", nameof(expected), expected);
            buffer.AppendFormat(CultureInfo.InvariantCulture, "{0}   = <<{1}>>\n", nameof(actual), actual);
            buffer.AppendFormat(CultureInfo.InvariantCulture, "Expected did not equal Actual for StringComparison '{0}'\n", comparison);

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        public static void InstanceOfHasProperty(this Assert assert, object value, string property)
        {
            Assert.IsNotNull(value);
            Assert.IsFalse(string.IsNullOrWhiteSpace(property));
            Assert.IsNotNull(value.GetType().GetProperty(property), PropertyAssert.AssertFailedMessage(value, property));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithType(this Assert assert, object value, string property, Type expected)
        {
            Assert.IsNotNull(value);
            PropertyAssert.InstanceOfHasProperty(assert, value, property);
            Assert.IsNotNull(expected);

            Type actual = value.GetType().GetProperty(property).PropertyType;

            Assert.IsTrue(expected == actual || actual.IsAssignableFrom(expected), PropertyAssert.AssertFailedMessage(value, property, expected));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, bool expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(bool));
            bool actual = false;

            try
            {
                actual = Convert.ToBoolean(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, byte expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(byte));
            byte actual = byte.MinValue;

            try
            {
                actual = Convert.ToByte(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, char expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(char));
            char actual = char.MinValue;

            try
            {
                actual = Convert.ToChar(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, DateTime expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(DateTime));
            DateTime actual = DateTime.MinValue;

            try
            {
                actual = Convert.ToDateTime(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, Decimal expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(Decimal));
            Decimal actual = Decimal.Zero;

            try
            {
                actual = Convert.ToDecimal(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, double expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(double));
            double actual = 0.0;

            try
            {
                actual = Convert.ToDouble(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, short expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(short));
            short actual = 0;

            try
            {
                actual = Convert.ToInt16(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, int expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(int));
            int actual = 0;

            try
            {
                actual = Convert.ToInt32(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, long expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(long));
            long actual = 0L;

            try
            {
                actual = Convert.ToInt64(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, sbyte expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(sbyte));
            sbyte actual = 0;

            try
            {
                actual = Convert.ToSByte(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, float expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(float));
            float actual = 0.0F;

            try
            {
                actual = Convert.ToSingle(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        /// <param name="comparison"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, string expected, StringComparison comparison = StringComparison.InvariantCulture)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(string));
            string actual = string.Empty;

            try
            {
                actual = Convert.ToString(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.IsTrue(string.Equals(expected, actual, comparison), PropertyAssert.AssertFailedMessage(value, property, expected, actual, comparison));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, ushort expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(ushort));
            ushort actual = 0;

            try
            {
                actual = Convert.ToUInt16(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, uint expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(uint));
            uint actual = 0;

            try
            {
                actual = Convert.ToUInt32(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        /// <param name="property"></param>
        /// <param name="expected"></param>
        public static void InstanceOfHasPropertyWithValue(this Assert assert, object value, string property, ulong expected)
        {
            PropertyAssert.InstanceOfHasPropertyWithType(assert, value, property, typeof(ulong));
            ulong actual = 0L;

            try
            {
                actual = Convert.ToUInt64(value.GetType().GetProperty(property).GetValue(value));
            }
            catch (AmbiguousMatchException amex)
            {
                throw new AssertFailedException((new Message(amex)).ToString(amex), amex);
            }
            catch (ArgumentNullException anex)
            {
                throw new AssertFailedException((new Message(anex)).ToString(anex), anex);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException((new Message(ex)).ToString(ex), ex);
            }

            Assert.AreEqual(expected, actual, PropertyAssert.AssertFailedMessage(value, property, expected, actual));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="property"></param>
        public static void AreEquivalent(this Assert assert, object expected, object actual, string property)
        {
            Assert.IsFalse(string.IsNullOrWhiteSpace(property));

            if ((expected is null) && (actual is null))
            {
                return;
            }
            else if ((expected is null) ^ (actual is null))
            {
                throw new AssertFailedException();
            }
            else
            {
                PropertyAssert.InstanceOfHasProperty(assert, expected, property);
                PropertyAssert.InstanceOfHasProperty(assert, actual, property);
            }
        }
    }
}
