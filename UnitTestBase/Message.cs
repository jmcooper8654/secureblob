﻿// ------------------------------------------------------------------------------
// <copyright file="Message.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-17</date>
// <summary>
//     Description of File Name "Message.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    using Gunfighter.Gplv3.Tool;

    /// <summary>
    /// Class implementing uniform support for <c>MSTest</c> <see cref="Assert"/> messages.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Initial capacity for the <see cref="StringBuilder"/> instance.
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        /// The <see cref="StringBuilder"/> instance.
        /// </summary>
        private StringBuilder buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="path">Caller file path that triggered assertion failure.</param>
        /// <param name="line">Caller line number that triggered assertion failure.</param>
        /// <param name="name">Caller member name that triggered assertion failure.</param>
        public Message([CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : this(CultureInfo.CurrentCulture, "Assertion failed in source file {0} at source line number '{1}' in member {2}.", path, line, name, path, line, name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> being processed for a formatted message.</param>
        /// <param name="path">Caller file path that triggered the <see cref="Exception"/>.</param>
        /// <param name="line">Caller line number that triggered the <see cref="Exception"/>.</param>
        /// <param name="name">Caller member name that triggered the <see cref="Exception"/>.</param>
        public Message(Exception exception, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : this(CultureInfo.CurrentCulture, exception?.Message, path, line, name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="message">The message string.</param>
        /// <param name="path">Caller file path that triggered assertion failure.</param>
        /// <param name="line">Caller line number that triggered assertion failure.</param>
        /// <param name="name">Caller member name that triggered assertion failure.</param>
        public Message(string message, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null)
            : this(CultureInfo.CurrentCulture, message, path, line, name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="format">The message format string to instantiate with <paramref name="arguments"/>.</param>
        /// <param name="path">Caller file path that triggered assertion failure.</param>
        /// <param name="line">Caller line number that triggered assertion failure.</param>
        /// <param name="name">Caller member name that triggered assertion failure.</param>
        /// <param name="arguments">Arguments to provide to the <paramref name="format"/>.</param>
        public Message(string format, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null, params object[] arguments)
            : this(CultureInfo.CurrentCulture, format, path, line, name, arguments)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="provider">Culture information provider to support culture-specific formatting.</param>
        /// <param name="format">The message format string to instantiate with <paramref name="arguments"/>.</param>
        /// <param name="path">Caller file path that triggered assertion failure.</param>
        /// <param name="line">Caller line number that triggered assertion failure.</param>
        /// <param name="name">Caller member name that triggered assertion failure.</param>
        /// <param name="arguments">Arguments to provide to the <paramref name="format"/>.</param>
        public Message(IFormatProvider provider, string format, [CallerFilePath] string path = null, [CallerLineNumber] int line = 0, [CallerMemberName] string name = null, params object[] arguments)
        {
            this.buffer = new StringBuilder(InitialCapacity);

            this.FilePath = new FileInfo(path);
            this.LineNumber = line;
            this.Member = name;
            this.Text = string.Format(provider ?? CultureInfo.CurrentCulture, format, arguments);
        }

        /// <summary>
        /// Gets a value indicating the file path related to the message.
        /// </summary>
        public FileInfo FilePath { get; private set; }

        /// <summary>
        /// Gets a value indicating the line number related to the message.
        /// </summary>
        public int LineNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating the member name related to the message.
        /// </summary>
        public string Member { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating the message text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Formats a message to <see cref="string"/>.
        /// </summary>
        /// <returns>Returns the formatted message as a string.</returns>
        public string FormatMessage()
        {
            this.buffer.Clear();

            this.buffer.AppendFormat(CultureInfo.InvariantCulture, "{0}({1})", this.Member, this.LineNumber);
            this.buffer.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.InvariantCulture, "AssertFailedException ERROR");
            this.buffer.Append(' ').Append(':').Append(' ').AppendFormat(CultureInfo.CurrentCulture, this.Text).AppendLine();

            this.buffer.Append('\t').Append("Source").Append("      = ").Append(this.FilePath.FullName).AppendLine();
            this.buffer.Append('\t').Append("Line Number").Append(" = ").Append(this.LineNumber).AppendLine();

            return this.buffer.ToString();
        }

        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message to <see cref="string"/>.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string FormatMessage(Exception exception)
        {
            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            return this.FormatMessage(exception, displayDetail: false, displayStackTrace: false);
        }

        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message to <see cref="string"/>.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <param name="displayDetail">If <see langref="true"/>, display the <paramref name="exception"/> with more detail.</param>
        /// <param name="displayStackTrace">If <see langref="true"/>, display the <paramref name="exception"/> with a stack trace.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string FormatMessage(Exception exception, bool displayDetail, bool displayStackTrace)
        {
            this.buffer.Clear();

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            if (!displayDetail && !displayStackTrace)
            {
                return this.buffer.ExceptionBasic(exception, this.Member, this.LineNumber, this.FilePath).ToString();
            }

            if (displayDetail)
            {
                this.buffer.ExceptionDetail(exception);
                this.buffer.ExceptionData(exception);
            }

            if (displayStackTrace)
            {
                this.buffer.ExceptionStackTrace(exception, displayDetail);
            }

            return this.buffer.ToString();
        }

        /// <summary>
        /// Extracts and formats an <see cref="Exception"/> message to <see cref="string"/>.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <param name="displayDetail">If <see langref="true"/>, display the <paramref name="exception"/> with more detail.</param>
        /// <param name="displayStackTrace">If <see langref="true"/>, display the <paramref name="exception"/> with a stack trace.</param>
        /// <param name="related">File associated with the <paramref name="exception"/>.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string FormatMessage(Exception exception, bool displayDetail, bool displayStackTrace, FileInfo related)
        {
            this.buffer.Clear();

            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            if (!displayDetail && !displayStackTrace)
            {
                return this.buffer.ExceptionBasic(exception).ToString();
            }

            if (displayDetail)
            {
                this.buffer.ExceptionDetail(exception, related);
                this.buffer.ExceptionData(exception);
            }

            if (displayStackTrace)
            {
                this.buffer.ExceptionStackTrace(exception, displayDetail);
            }

            return this.buffer.ToString();
        }

        /// <summary>
        /// Formats a message to string.
        /// </summary>
        /// <returns>Returns the formatted message as a string.</returns>
        public override string ToString()
        {
            return this.FormatMessage();
        }

        /// <summary>
        /// Formats an <see cref="Exception"/> message to string.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string ToString(Exception exception)
        {
            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            return this.FormatMessage(exception, false, false);
        }

        /// <summary>
        /// Formats an <see cref="Exception"/> message to string.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <param name="displayDetail">If <see langref="true"/>, display the <paramref name="exception"/> with more detail.</param>
        /// <param name="displayStackTrace">If <see langref="true"/>, display the <paramref name="exception"/> with a stack trace.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string ToString(Exception exception, bool displayDetail, bool displayStackTrace)
        {
            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            return this.FormatMessage(exception, displayDetail, displayStackTrace);
        }

        /// <summary>
        /// Formats an <see cref="Exception"/> message to string.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> to extract and format.</param>
        /// <param name="displayDetail">If <see langref="true"/>, display the <paramref name="exception"/> with more detail.</param>
        /// <param name="displayStackTrace">If <see langref="true"/>, display the <paramref name="exception"/> with a stack trace.</param>
        /// <param name="related">File associated with the <paramref name="exception"/>.</param>
        /// <returns>Returns the extracted and formatted exception message as a string.</returns>
        /// <exception cref="ArgumentNullException">Throws if <paramref name="exception"/> is <see langref="null"/>.</exception>
        public string ToString(Exception exception, bool displayDetail, bool displayStackTrace, FileInfo related)
        {
            if (GenericTool.IsNull(exception))
            {
                throw new ArgumentNullException(nameof(exception));
            }

            return this.FormatMessage(exception, displayDetail, displayStackTrace, related);
        }
    }
}
