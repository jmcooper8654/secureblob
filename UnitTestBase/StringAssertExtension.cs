﻿// ------------------------------------------------------------------------------
// <copyright file="StringAssertExtension.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-16</date>
// <summary>
//     Description of File Name "StringAssertExtension.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    using System;
    using System.CodeDom;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///
    /// </summary>
    public static class StringAssertExtension
    {
        /// <summary>
        ///
        /// </summary>
        private const int InitialCapacity = 4096;

        /// <summary>
        ///
        /// </summary>
        private readonly static StringBuilder buffer = new StringBuilder(InitialCapacity);

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AssertEmptyFailedMessage(string value)
        {
            string display = value is null ? "<<null>>" : "<<empty>>";

            buffer.Clear();

            buffer.AppendLine($"Value is null or empty.");
            buffer.AppendFormat(CultureInfo.InvariantCulture, "Value:  {0}", display);

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <returns></returns>
        public static string AssertFailedMessage(string expected, string actual)
        {
            string EscapeControl(string value)
            {
                using (var sw = new StringWriter())
                {
                    using (var cSharpProvider = CodeDomProvider.CreateProvider("CSharp"))
                    {
                        cSharpProvider.GenerateCodeFromExpression(new CodePrimitiveExpression(value), sw, null);
                    }

                    return sw.ToString();
                }
            }

            string ReplaceWhiteSpace(string value, char c)
            {
                foreach (var item in value)
                {
                    if (char.IsWhiteSpace(item))
                    {
                        value.Replace(item, c);
                    }
                }

                return value;
            }

            if (expected.Any(c => char.IsWhiteSpace(c)))
            {
                expected = ReplaceWhiteSpace(expected, '.');
            }

            if (actual.Any(c => char.IsWhiteSpace(c)))
            {
                actual = ReplaceWhiteSpace(actual, '.');
            }

            if (expected.Any(c => char.IsControl(c)))
            {
                expected = EscapeControl(expected);
            }

            if (actual.Any(c => char.IsControl(c)))
            {
                actual = EscapeControl(actual);
            }

            int index = expected.Zip(actual, (x, a) => x.Equals(a)).TakeWhile(c => c).Count();

            buffer.Clear();

            buffer.AppendLine($"Expected and Actual strings differ.");
            buffer.AppendFormat(CultureInfo.CurrentCulture, "Expected:  {0}", expected);
            buffer.AppendFormat(CultureInfo.CurrentCulture, "Actual:    {0}", actual);
            buffer.Append(' ', index).Append('^').AppendLine();
            buffer.Append(' ', index).Append('|').AppendLine();

            string column = index.ToString();

            for (int i = 0; i < column.Length; i++)
            {
                buffer.Append(' ', index).Append(column[i]).AppendLine();
            }

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="compareType"></param>
        /// <returns></returns>
        public static string AssertFailedMessage(string expected, string actual, CompareConstant compareType)
        {
            string EscapeControl(string value)
            {
                using (var sw = new StringWriter())
                {
                    using (var cSharpProvider = CodeDomProvider.CreateProvider("CSharp"))
                    {
                        cSharpProvider.GenerateCodeFromExpression(new CodePrimitiveExpression(value), sw, null);
                    }

                    return sw.ToString();
                }
            }

            string ReplaceWhiteSpace(string value, char c)
            {
                foreach (var item in value)
                {
                    if (char.IsWhiteSpace(item))
                    {
                        value.Replace(item, c);
                    }
                }

                return value;
            }

            buffer.Clear();

            if (expected.Any(c => char.IsWhiteSpace(c)))
            {
                expected = ReplaceWhiteSpace(expected, '.');
            }

            if (actual.Any(c => char.IsWhiteSpace(c)))
            {
                actual = ReplaceWhiteSpace(actual, '.');
            }

            if (expected.Any(c => char.IsControl(c)))
            {
                expected = EscapeControl(expected);
            }

            if (actual.Any(c => char.IsControl(c)))
            {
                actual = EscapeControl(actual);
            }

            int index = 0;

            switch (compareType)
            {
                case CompareConstant.CommpareLessThan:
                    if (string.Equals(expected, actual))
                    {
                        buffer.AppendLine($"Expected and Actual strings are lexically identical.");
                        return buffer.ToString();
                    }
                    else
                    {
                        index = expected.Zip(actual, (x, a) => x >= a).TakeWhile(c => c).Count();
                        buffer.AppendLine($"Expected string is lexically greater than Actual string at position '{index}'.");
                    }

                    break;

                case CompareConstant.CompareEqual:
                    index = expected.Zip(actual, (x, a) => x.Equals(a)).TakeWhile(c => c).Count();

                    buffer.AppendLine($"Expected and Actual strings are lexically different at position '{index}'.");
                    break;

                case CompareConstant.CompareGreaterThan:
                    if (string.Equals(expected, actual))
                    {
                        buffer.AppendLine($"Expected and Actual strings are lexically identical.");
                        return buffer.ToString();
                    }
                    else
                    {
                        index = expected.Zip(actual, (x, a) => x <= a).TakeWhile(c => c).Count();
                        buffer.AppendLine($"Expected string is lexically less than Actual string at position '{index}'.");
                    }

                    break;

                case CompareConstant.CompareGreaterThanOrEqual:
                    index = expected.Zip(actual, (x, a) => x < a).TakeWhile(c => c).Count();
                    buffer.AppendLine($"Expected string is lexically less than Actual string at position '{index}'.");
                    break;

                case CompareConstant.CompareLessThanOrEqual:
                    index = expected.Zip(actual, (x, a) => x > a).TakeWhile(c => c).Count();
                    buffer.AppendLine($"Expected string is lexically greater than Actual string at position '{index}'.");
                    break;

                case CompareConstant.CompareNotEqual:
                    buffer.AppendLine($"Expected and Actual strings are lexically identical.");
                    return buffer.ToString();

                default:
                    break;
            }

            buffer.AppendFormat(CultureInfo.CurrentCulture, "Expected:  {0}", expected);
            buffer.AppendFormat(CultureInfo.CurrentCulture, "Actual:    {0}", actual);
            buffer.Append(' ', index + 11).Append('^').AppendLine();
            buffer.Append(' ', index + 11).Append('|').AppendLine();

            string column = index.ToString();

            for (int i = 0; i < column.Length; i++)
            {
                buffer.Append(' ', index + 11).Append(column[i]).AppendLine();
            }

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AssertNotEmptyFailedMessage(string value)
        {
            string ReplaceWhiteSpace(char c)
            {
                foreach (var item in value)
                {
                    if (char.IsWhiteSpace(item))
                    {
                        value.Replace(item, c);
                    }
                }

                return value;
            }

            string display = value.Any(c => char.IsWhiteSpace(c)) ? ReplaceWhiteSpace('.') : value;

            buffer.Clear();

            buffer.AppendLine($"Value is not empty.");
            buffer.AppendFormat(CultureInfo.InvariantCulture, "Value:  {0}", display);

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AssertNotEmptyNotWhiteSpaceFailedMessage(string value)
        {
            string ReplaceWhiteSpace(char c)
            {
                foreach (var item in value)
                {
                    if (char.IsWhiteSpace(item))
                    {
                        value.Replace(item, c);
                    }
                }

                return value;
            }

            string display = value.Any(c => char.IsWhiteSpace(c)) ? ReplaceWhiteSpace('.') : value;

            buffer.Clear();

            if (!string.IsNullOrWhiteSpace(value))
            {
                buffer.AppendLine($"Value is not null, not empty, and not all whitespace.");
            }
            else
            {
                buffer.AppendLine($"Value is both not null and not empty.");
            }

            buffer.AppendFormat(CultureInfo.InvariantCulture, "Value:  {0}", display);

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AssertWhiteSpaceFailedMessage(string value)
        {
            string ReplaceWhiteSpace(char c)
            {
                foreach (var item in value)
                {
                    if (char.IsWhiteSpace(item))
                    {
                        value.Replace(item, c);
                    }
                }

                return value;
            }

            string display = value is null ? "<<null>>" : string.IsNullOrEmpty(value) ? "<<empty>>" : ReplaceWhiteSpace('.');

            buffer.Clear();

            buffer.AppendLine($"Value is null, empty, or all whitespace.");
            buffer.AppendFormat(CultureInfo.CurrentCulture, "Value:  {0}", display);

            return buffer.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="value"></param>
        public static void CompareTo(string left, string right, CompareConstant value)
        {
            StringAssertExtension.CompareTo(left, right, value, StringComparison.Ordinal);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="value"></param>
        /// <param name="comparisonType"></param>
        public static void CompareTo(string left, string right, CompareConstant value, StringComparison comparisonType)
        {
            switch (value)
            {
                case CompareConstant.CompareLessThanOrEqual:
                    if (left.CompareTo(right) > 0)
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                case CompareConstant.CommpareLessThan:
                    if (left.CompareTo(right) > 0 || string.Equals(left, right, comparisonType))
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                case CompareConstant.CompareEqual:
                    if (!string.Equals(left, right, comparisonType))
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                case CompareConstant.CompareNotEqual:
                    if (string.Equals(left, right, comparisonType))
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                case CompareConstant.CompareGreaterThan:
                    if (left.CompareTo(right) < 0 || string.Equals(left, right, comparisonType))
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                case CompareConstant.CompareGreaterThanOrEqual:
                    if (left.CompareTo(right) < 0)
                    {
                        throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(left, right, value));
                    }

                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"CompareConstant {value} is not supported.");
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void Contains(this StringAssert assert, string source, char value)
        {
            if (!source.Contains(new string(value, 1)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void Contains(this StringAssert assert, string source, string value)
        {
            if (!source.Contains(value))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="match"></param>
        public static void Contains(this StringAssert assert, string source, Func<char, bool> match)
        {
            if (!source.Contains(match))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="anyOf"></param>
        public static void ContainsAny(this StringAssert assert, string source, IEnumerable<char> anyOf)
        {
            if (source.All(c => !anyOf.Contains(c)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="allOf"></param>
        public static void ContainsAll(this StringAssert assert, string source, IEnumerable<char> allOf)
        {
            if (source.Any(c => !allOf.Contains(c)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void EndsWith(this StringAssert assert, string source, char value)
        {
            if (!source.EndsWith(new string(value, 1)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void EndsWith(this StringAssert assert, string source, string value)
        {
            if (!source.EndsWith(value))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        public static void Equals(this StringAssert assert, string expected, string actual)
        {
            assert.Equals(expected, actual, StringComparison.Ordinal);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="comparisonType"></param>
        public static void Equals(this StringAssert assert, string expected, string actual, StringComparison comparisonType)
        {
            if (!string.Equals(expected, actual, comparisonType))
            {
                throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(expected, actual));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsDoubleQuoted(this StringAssert assert, string value)
        {
            if (!value.StartsWith("\"") || !value.EndsWith("\""))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsNotDoubleQuoted(this StringAssert assert, string value)
        {
            if (value.StartsWith("\"") && value.EndsWith("\""))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsNotEmpty(this StringAssert assert, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new AssertFailedException(StringAssertExtension.AssertNotEmptyFailedMessage(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsNotEmptyAndNotWhiteSpace(this StringAssert assert, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new AssertFailedException(StringAssertExtension.AssertNotEmptyNotWhiteSpaceFailedMessage(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsNullOrEmpty(this StringAssert assert, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                throw new AssertFailedException(StringAssertExtension.AssertEmptyFailedMessage(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="value"></param>
        public static void IsNullOrWhiteSpace(this StringAssert assert, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                throw new AssertFailedException(StringAssertExtension.AssertWhiteSpaceFailedMessage(value));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void NotContains(this StringAssert assert, string source, char value)
        {
            if (!source.Contains(new string(value, 1)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void NotContains(this StringAssert assert, string source, string value)
        {
            if (source.Contains(value))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="match"></param>
        public static void NotContains(this StringAssert assert, string source, Func<char, bool> match)
        {
            if (source.Contains(match))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void NotContainsEmbedded(this StringAssert assert, string source, char value)
        {
            if (source.Skip(1).Take(source.Length - 2).Contains(value))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        public static void SetNoDifference(string expected, string actual)
        {
            StringAssertExtension.SetNoDifference(expected, actual, EqualityComparer<char>.Default);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="comparer"></param>
        public static void SetNoDifference(string expected, string actual, IEqualityComparer<char> comparer)
        {
            var left = expected.Except(actual, comparer ?? EqualityComparer<char>.Default);
            var right = actual.Except(expected, comparer ?? EqualityComparer<char>.Default);

            if (GenericTool.IsNullOrEmpty(left) && GenericTool.IsNullOrEmpty(right))
            {
                return;
            }
            else
            {
                throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(expected, actual));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        public static void SequenceEqual(string expected, string actual)
        {
            expected.SequenceEqual(actual, EqualityComparer<char>.Default);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="comparer"></param>
        public static void SequenceEqual(string expected, string actual, IEqualityComparer<char> comparer)
        {
            if (!expected.SequenceEqual(actual, comparer ?? EqualityComparer<char>.Default))
            {
                throw new AssertFailedException(StringAssertExtension.AssertFailedMessage(expected, actual));
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void StartsWith(this StringAssert assert, string source, char value)
        {
            if (!source.StartsWith(new string(value, 1)))
            {
                throw new AssertFailedException();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="assert"></param>
        /// <param name="source"></param>
        /// <param name="value"></param>
        public static void StartsWith(this StringAssert assert, string source, string value)
        {
            if (!source.StartsWith(value))
            {
                throw new AssertFailedException();
            }
        }
    }
}
