﻿// ------------------------------------------------------------------------------
// <copyright file="UnitTestBase.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-14</date>
// <summary>
//     Description of File Name "UnitTestBase.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

namespace Gunfighter.Gplv3.Testing
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// UnitTest abstract base class.
    /// </summary>
    // [TestClass]
    public abstract class UnitTestBase
    {
        /// <summary>
        /// Gets a value indicating the caller file path initialized by calling <see cref="InitCallerFilePath(string)"/> in the test method.
        /// </summary>
        public FileInfo CallerFilePath { get; private set; }

        /// <summary>
        /// Gets a value indicating the caller member name initialized by calling <see cref="InitCallerMemberName(string)"/> in the test method.
        /// </summary>
        public string CallerMemberName { get; private set; }

        /// <summary>
        /// Gets a value indicating the caller line number initialized by calling <see cref="InitCallerLineNumber(int)"/> in the test method.
        /// </summary>
        public int CallerLineNumber { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating the <see cref="TestContext"/> which provides
        /// information about and functionality for the
        /// <see cref="UnitTestBase"/> test run.
        /// </summary>
        /// <remarks>Set directly by the test runner.</remarks>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// Gets the test name from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the current test name.</returns>
        public string TestName { get => this.TestContext.TestName; }

        /// <summary>
        /// Clears the per test public properties.
        /// </summary>
        public void Clear()
        {
            this.CallerFilePath = new FileInfo("UnitTestBase.cs");
            this.CallerLineNumber = 0;
            this.CallerMemberName = this.TestName;
        }

        /// <summary>
        /// Tests whether <paramref name="propertyName"/> is contained in <paramref name="properties"/>.
        /// </summary>
        /// <param name="properties"><see cref="Dictionary{TKey, TValue}"/> of properties.</param>
        /// <param name="propertyName">Property name to test for existance in <paramref name="properties"/>.</param>
        /// <returns>Returns <see langref="true"/> if <paramref name="propertyName"/> is contained in <paramref name="properties"/>; otherwise <see langref="false"/>.</returns>
        public bool ContainsProperty(Dictionary<string, object> properties, string propertyName)
        {
            return properties.ContainsKey(propertyName);
        }

        /// <summary>
        /// Creates a <see cref="System.Timers.Timer"/> with output variable name "testMethodTimer".
        /// </summary>
        /// <param name="testMethodTimer">Outputs the timer.</param>
        /// <returns>Always returns the string "testMethodTimer".</returns>
        public string CreateNamedTestMethodTimer(out System.Timers.Timer testMethodTimer)
        {
            testMethodTimer = new System.Timers.Timer();
            return nameof(testMethodTimer);
        }

        /// <summary>
        /// Gets the deployment directory from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the directory for files deployed for the test run.</returns>
        public DirectoryInfo DeploymentDirectory() => new DirectoryInfo(this.TestContext.DeploymentDirectory);

        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="TestInitializeAttribute"/></remarks>
        [TestInitialize]
        public virtual void EachTestInitialize()
        {
            this.TestContext.WriteLine($"{nameof(this.EachTestInitialize)} is running for {this.TestName}.");
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="TestCleanupAttribute"/></remarks>
        [TestCleanup]
        public virtual void EachTestCleanup()
        {
            this.TestContext.WriteLine($"{nameof(this.EachTestCleanup)} is running for {this.TestName}.");
            this.Clear();
        }

        /// <summary>
        /// Gets the fully qualified test class name from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the fully qualified test class name as a <see cref="string"/>.</returns>
        public string FullTestClassName()
        {
            return this.TestContext.FullyQualifiedTestClassName;
        }

        /// <summary>
        /// Concatenates <see cref="FullTestClassName"/> and <see cref="TestName"/>.
        /// </summary>
        /// <returns>Returns the fully qualified test name.</returns>
        public string FullTestName()
        {
            List<string> parts = new List<string>()
            {
                this.FullTestClassName(),
                this.TestName.Concat("()").ToString(),
            };

            return string.Join(".", parts);
        }

        /// <summary>
        /// Get the <see cref="Dictionary{TKey, TValue}"/> of properties for the test method.
        /// </summary>
        /// <returns>Returns the test method properties.</returns>
        public Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> accumulator = new Dictionary<string, object>();

            if ((this.TestContext?.Properties?.Keys is null) || this.TestContext.Properties.Keys.Count < 1)
            {
                return accumulator;
            }

            foreach (DictionaryEntry item in this.TestContext.Properties)
            {
                accumulator.Add(item.Key.ToString(), item.Value);
            }

            return accumulator;
        }

        /// <summary>
        /// Tests if a test method has timed out.
        /// </summary>
        /// <returns>Returns <see langref="true"/> if the <see cref="TestTimeoutCancellationTokenSource.IsCancellationRequested"/> is <see langref="true"/>.</returns>
        public bool HasCancellationRequested()
        {
            return this.TestTimeoutCancellationTokenSource().IsCancellationRequested;
        }

        /// <summary>
        /// Sets the <see cref="CallerFilePath"/> from within the <see cref="TestMethod"/> or <see cref="DataTestMethod"/>.
        /// </summary>
        /// <param name="filePath">Parameter used by the compiler to inject the caller file path.</param>
        /// <remarks>Must be called from each <see cref="TestMethod"/> or <see cref="DataTestMethod"/> to work.</remarks>
        public void InitCallerFilePath([CallerFilePath] string filePath = null)
        {
            this.CallerFilePath = !string.IsNullOrEmpty(filePath) ? new FileInfo(filePath) : new FileInfo("UnitTestBase.cs");
        }

        /// <summary>
        /// Sets the <see cref="CallerLineNumber"/> from within the <see cref="TestMethod"/> or <see cref="DataTestMethod"/>.
        /// </summary>
        /// <param name="line">Parameter used by the compiler to inject the caller line number.</param>
        /// <remarks>Must be called from each <see cref="TestMethod"/> or <see cref="DataTestMethod"/> to work.</remarks>
        public void InitCallerLineNumber([CallerLineNumber] int line = 0)
        {
            this.CallerLineNumber = line > 0 ? line : 0;
        }

        /// <summary>
        /// Sets the <see cref="CallerMemberName"/> from within the <see cref="TestMethod"/> or <see cref="DataTestMethod"/>.
        /// </summary>
        /// <param name="name">Parameter used by the compiler to inject the caller member name.</param>
        /// <remarks>Must be called from each <see cref="TestMethod"/> or <see cref="DataTestMethod"/> to work.</remarks>
        public void InitCallerMemberName([CallerMemberName] string name = null)
        {
            this.CallerMemberName = !string.IsNullOrWhiteSpace(name) ? name : this.TestName;
        }

        /// <summary>
        /// Start a named timer.
        /// </summary>
        /// <param name="timerName">The name of the timer.</param>
        public void StartTestMethodNamedTimer(string timerName = "testMethodTimer")
        {
            this.TestContext.BeginTimer(timerName);
        }

        /// <summary>
        /// Stop a named timer.
        /// </summary>
        /// <param name="timerName">The name of the timer.</param>
        public void StopTestMethodNamedTimer(string timerName = "testMethodTimer")
        {
            this.TestContext.EndTimer(timerName);
        }

        /// <summary>
        /// Gets the <see cref="UnitTestOutcome"/> for the current test from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the <see cref="UnitTestOutcome"/> for the current test.</returns>
        public UnitTestOutcome TestOutcome()
        {
            return this.TestContext.CurrentTestOutcome;
        }

        /// <summary>
        /// Gets the test results directory from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the directory for the test results file.</returns>
        public DirectoryInfo TestResultsDirectory()
        {
            return new DirectoryInfo(this.TestContext.TestResultsDirectory);
        }

        /// <summary>
        /// Gets the test run directory from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the base directory for the test run.</returns>
        public DirectoryInfo TestRunDirectory()
        {
            return new DirectoryInfo(this.TestContext.TestRunDirectory);
        }

        /// <summary>
        /// Gets the test run results directory from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the directory for the test run results file.</returns>
        public DirectoryInfo TestRunResultsDirectory()
        {
            return new DirectoryInfo(this.TestContext.TestRunResultsDirectory);
        }

        /// <summary>
        /// Gets the cancellation token source for the current test from the <see cref="TestContext"/>.
        /// </summary>
        /// <returns>Returns the <see cref="CancellationTokenSource"/> for the current test.</returns>
        public CancellationTokenSource TestTimeoutCancellationTokenSource()
        {
            return this.TestContext.CancellationTokenSource;
        }

        /// <summary>
        /// Conditionally attempt to retrieve the <paramref name="value"/> for the <paramref name="propertyName"/>.
        /// </summary>
        /// <param name="properties">The test method properties.</param>
        /// <param name="propertyName">The property name to attempt to retrieve a value for.</param>
        /// <param name="value">The value associated with <paramref name="propertyName"/>.</param>
        /// <returns>Returns <see langref="true"/> if the <paramref name="value"/> is successfully retrieved; otherwise <see langref="false"/>.</returns>
        public static bool TryGetValue(Dictionary<string, object> properties, string propertyName, out object value)
        {
            return properties.TryGetValue(propertyName, out value);
        }
    }
}
