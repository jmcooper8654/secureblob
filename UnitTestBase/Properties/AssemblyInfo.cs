// ------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-14</date>
// <summary>
//     Description of File Name "AssemblyInfo.cs" in Project "UnitTestBase":
// </summary>
// ------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("UnitTestBase")]
[assembly: AssemblyDescription("MSTest base class for unit testing with extensions.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("John Merryweather Cooper")]
[assembly: AssemblyProduct("UnitTestBase")]
[assembly: AssemblyCopyright("Copyright © 2020 John Merryweather Cooper")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("18ffab94-c7d2-4b38-b362-5a4f1e1bf93c")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
