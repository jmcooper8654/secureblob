// ------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-10</date>
// <summary>
//     Description of File Name "AssemblyInfo.cs" in Project "UnitTestSymmetricHeader":
// </summary>
// ------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("UnitTestSymmetricHeader")]
[assembly: AssemblyDescription("Unit tests for the SymmetricHeader class.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("John Merryweather Cooper")]
[assembly: AssemblyProduct("SecureBlob")]
[assembly: AssemblyCopyright("Copyright © 2020 John Merryweather Cooper")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("4c52c24e-b012-4ec6-ad8e-5f91bf202891")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
