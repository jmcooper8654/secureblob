// ------------------------------------------------------------------------------
// <copyright file="SymmetricHeaderTests.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-15</date>
// <summary>
//     Description of File Name "SymmetricHeaderTests.cs" in Project "UnitTestSymmetricHeader":
// </summary>
// ------------------------------------------------------------------------------

namespace UnitTestSymmetricHeader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    using Gunfighter.Gplv3.Security.SecureBlob;
    using Gunfighter.Gplv3.Testing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the <see cref="SymmetricHeader"/> class.
    /// </summary>
    [TestClass]
    public class SymmetricHeaderTests : UnitTestBase
    {
        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestInitialize()"/></remarks>
        [TestInitialize]
        public override void EachTestInitialize()
        {
            base.EachTestInitialize();
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestCleanup()"/></remarks>
        [TestCleanup]
        public override void EachTestCleanup()
        {
            base.EachTestCleanup();
        }

        [TestMethod]
        public void Deserialize_DefaultSymmeticHeaderBytes_AreEqual()
        {
            // Arrange
            SymmetricHeader expected = new SymmetricHeader();
            IEnumerable<byte> bytes = expected.Serialize<SymmetricHeader>();

            // Act
            var actual = expected.Deserialize<SymmetricHeader>(bytes);

            // Assert
            Assert.AreEqual(expected.Algorithm, actual.Algorithm, ignoreCase: false);
            CollectionAssert.AreEqual(expected.IV.ToArray(), actual.IV.ToArray());
            Assert.AreEqual(expected.Mode, actual.Mode);
            Assert.AreEqual(expected.Padding, actual.Padding);
        }

        [TestMethod]
        public void GetLength_DefaultSymmeticHeader_LengthOf141Bytes()
        {
            // Arrange
            const int ExpectedLength = 141;
            SymmetricHeader header = new SymmetricHeader();
            IEnumerable<byte> bytes = header.Serialize<SymmetricHeader>();

            // Act
            var result = SymmetricHeader.GetLength(bytes);

            // Assert
            Assert.IsTrue(result == ExpectedLength, $"Serialized bytes {result} are not equal to {ExpectedLength}.");
        }

        [DataTestMethod]
        [DataRow(long.MinValue)]
        [DataRow(0L)]
        [DataRow(91L)]
        [DataRow(107L)]
        [DataRow(140L)]
        [DataRow(141L)]
        [DataRow(156L)]
        [DataRow(163L)]
        [DataRow(360L)]
        [DataRow(381L)]
        [DataRow(382L)]
        [DataRow(long.MaxValue)]
        public void ToLengthBytes_DataRowLength_Returns8Bytes(long length)
        {
            // Arrange

            // Act
            var result = SymmetricHeader.ToLengthBytes(length);

            // Assert
            Assert.IsTrue(result.Count() == sizeof(long));
        }

        [TestMethod]
        public void Serialize_DefaultSymmetricHeader_Returns141Bytes()
        {
            // Arrange
            const int ExpectedLength = 141;
            SymmetricHeader header = new SymmetricHeader();

            // Act
            var result = header.Serialize<SymmetricHeader>();

            // Assert
            Assert.IsTrue(result.Count() == ExpectedLength, $"Serialized bytes {result.Count()} are not equal to {ExpectedLength}.");
        }

        [TestMethod]
        public void Serialize_SymmetricHeaderIV_Returns163Bytes()
        {
            // Arrange
            const int ExpectedLength = 163;

            using (AesCng cipher = new AesCng())
            {
                cipher.GenerateIV();

                SymmetricHeader header = new SymmetricHeader(cipher.GetType().Name, cipher.IV, Array.Empty<byte>());

                // Act
                var result = header.Serialize<SymmetricHeader>();

                // Assert
                Assert.IsTrue(result.Count() == ExpectedLength, $"Serialized bytes {result.Count()} are not equal to {ExpectedLength}.");
            }
        }

        [TestMethod]
        public void Serialize_SymmetricHeaderIVCBCISO10126_Returns163Bytes()
        {
            // Arrange
            const int ExpectedLength = 163;

            using (AesCng cipher = new AesCng())
            {
                cipher.GenerateIV();

                SymmetricHeader header = new SymmetricHeader(cipher.GetType().Name, cipher.IV, Array.Empty<byte>(), 1000, HashAlgorithmName.SHA256, CipherMode.CBC, PaddingMode.ISO10126);

                // Act
                var result = header.Serialize<SymmetricHeader>();

                // Assert
                Assert.IsTrue(result.Count() == ExpectedLength, $"Serialized bytes {result.Count()} are not equal to {ExpectedLength}.");
            }
        }
    }
}
