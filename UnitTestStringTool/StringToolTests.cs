// ------------------------------------------------------------------------------
// <copyright file="StringToolTests.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-10</date>
// <summary>
//     Description of File Name "StringToolTests.cs" in Project "UnitTestStringTool":
// </summary>
// ------------------------------------------------------------------------------


namespace UnitTestStringTool
{
    using System;
    using System.Text;

    using Gunfighter.Gplv3.Testing;
    using Gunfighter.Gplv3.Tool;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///
    /// </summary>
    [TestClass]
    public class StringToolTests : UnitTestBase
    {
        /// <summary>
        /// Test preamble code run immediately before each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestInitialize()"/></remarks>
        [TestInitialize]
        public override void EachTestInitialize()
        {
            base.EachTestInitialize();
        }

        /// <summary>
        /// Test epilogue code to run immediately after each test.
        /// </summary>
        /// <remarks><see cref="UnitTestBase.EachTestCleanup()"/></remarks>
        [TestCleanup]
        public override void EachTestCleanup()
        {
            base.EachTestCleanup();
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Append_DefaultCharValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            char value = char.MinValue;

            // Act
            var result = source.Append(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow('\0')]
        [DataRow('a')]
        [DataRow('B')]
        [DataRow(' ')]
        [DataRow('.')]
        [DataRow('#')]
        [DataRow('\uFFFF')]
        public void Append_DataRowCharValuePropogatedSource_AreEqual(char value)
        {
            // Arrange
            string source = "Test";
            string expected = source + value;

            // Act
            var actual = source.Append(value);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Append_MinCountDefaultCharValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            char value = char.MinValue;
            int count = 1;

            // Act
            var result = source.Append(value, count);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow("AB")]
        [DataRow("cde")]
        [DataRow("FGHI")]
        [DataRow("jklmnOPQRS")]
        public void Append_DataRowCharArrayValueDataRowSource_AreEqual(string value)
        {
            // Arrange
            string source = "Test";
            string expected = source + value;

            // Act
            var actual = source.Append(value.ToCharArray());

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Append_NullCharArrayValueDataRowSource_AreEqual(string source)
        {
            // Arrange
            char[] value = null;

            // Act
            var actual = source.Append(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow("AB")]
        [DataRow("cde")]
        [DataRow("FGHI")]
        [DataRow("jklmnOPQRS")]
        public void Append_MinLengthFirstStartIndexDataRowCharArrayValuePopulatedSource_ArgumentNullException(string value)
        {
            // Arrange
            string source = "Test";
            string expected = source + value;
            int startIndex = 0;
            int length = value.ToCharArray().Length;

            // Act
            var actual = source.Append(value.ToCharArray(), startIndex, length);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Append_MinLengthFirstStartIndexNullCharArrayValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            char[] value = null;
            int startIndex = 0;
            int length = 1;

            // Act
            var result = source.Append(value, startIndex, length);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow("AB")]
        [DataRow("cde")]
        [DataRow("FGHI")]
        [DataRow("jklmnOPQRS")]
        public void Append_DataRowStringValuePopulatedSource_ArgumentNullException(string value)
        {
            // Arrange
            string source = "Test";
            string expected = source + value;

            // Act
            var actual = source.Append(value);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Append_NullStringValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            string value = null;

            // Act
            var result = source.Append(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AppendFormat_NullArgumentsFormatDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            string format = null;
            object[] arguments = null;

            // Act
            var result = source.AppendFormat(format, arguments);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AppendFormat_NullProviderNullArgumentsFormatDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            IFormatProvider provider = null;
            string format = null;
            object[] arguments = null;

            // Act
            var result = source.AppendFormat(provider, format, arguments);

            // Assert
            Assert.Fail($"{nameof(StringTool.Append)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow("Test")]
        public void EndsWith_MinCharValueDataRowSource_IsFalse(string source)
        {
            // Arrange
            char value = char.MinValue;

            // Act
            var result = source.EndsWith(value);

            // Assert
            Assert.IsFalse(result);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Prepend_DefaultCharValueNullSource_ArgumentNullException(string source)
        {
            // Arrange
            char value = char.MinValue;

            // Act
            var result = source.Prepend(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Prepend)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Prepend_MinCountDefaultCharValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            char value = char.MinValue;
            int count = 1;

            // Act
            var result = source.Prepend(value, count);

            // Assert
            Assert.Fail($"{nameof(StringTool.Prepend)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Prepend_NullCharArrayValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            char[] value = null;

            // Act
            var result = source.Prepend(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Prepend)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Prepend_MinLengthFirstStartIndexNullCharArrayValueNullSource_ArgumentNullException(string source)
        {
            // Arrange
            char[] value = null;
            int startIndex = 0;
            int length = 1;

            // Act
            var result = source.Prepend(value, startIndex, length);

            // Assert
            Assert.Fail($"{nameof(StringTool.Prepend)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Prepend_NullStringValueDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            string value = null;

            // Act
            var result = source.Prepend(value);

            // Assert
            Assert.Fail($"{nameof(StringTool.Prepend)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [DataRow("Test")]
        public void StartsWith_MinCharValueDataRowSource_IsFalse(string source)
        {
            // Arrange
            char value = char.MinValue;

            // Act
            var result = source.StartsWith(value);

            // Assert
            Assert.IsFalse(result);
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToArrayEncoded_DefaultEncodingDataRowValue_ArgumentNullException(string value)
        {
            // Arrange
            Encoding byteEncoding = Encoding.Default;

            // Act
            var result = value.ToArrayEncoded(byteEncoding);

            // Assert
            Assert.Fail($"{nameof(StringTool.ToArrayEncoded)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToArrayFromBase64_DataRowValue_ArgumentNullException(string value)
        {
            // Arrange

            // Act
            var result = value.ToArrayFromBase64();

            // Assert
            Assert.Fail($"{nameof(StringTool.ToArrayFromBase64)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [TestMethod]
        [DataRow("Test String")]
        [DataRow("String")]
        [DataRow("Test")]
        [DataRow(" ")]
        [DataRow("T")]
        [DataRow("Te")]
        [DataRow("ng")]
        public void ToStringBuilder_DataRowValue_AreEqual(string expected)
        {
            // Arrange
            string source = expected;

            // Act
            var actual = source.ToStringBuilder();

            // Assert
            Assert.AreEqual(expected, actual.ToString());
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToStringBuilder_DataRowValue_ArgumentNullException(string source)
        {
            // Arrange

            // Act
            var result = source.ToStringBuilder();

            // Assert
            Assert.Fail($"{nameof(StringTool.ToStringBuilder)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(3)]
        [DataRow(5)]
        public void ToStringBuilder_DataRowStartIndexEmptyValue_AreEqual(int startIndex)
        {
            // Arrange
            string source = "Test String";
            string expected = source.Substring(startIndex);

            // Act
            var actual = source.ToStringBuilder(startIndex);

            // Assert
            Assert.AreEqual(expected, actual.ToString());
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToStringBuilder_FirstStartIndexDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            int startIndex = 0;

            // Act
            var result = source.ToStringBuilder(startIndex);

            // Assert
            Assert.Fail($"{nameof(StringTool.ToStringBuilder)} should have thrown {typeof(ArgumentNullException).Name}.");
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(2)]
        [DataRow(3)]
        [DataRow(5)]
        public void ToStringBuilder_MaxLengthDataRowStartIndexProgatedValue_AreEqual(int startIndex)
        {
            // Arrange
            string source = "Test String";
            int length = source.Length - startIndex;
            string expected = source.Substring(startIndex, length);

            // Act
            var actual = source.ToStringBuilder(startIndex, length);

            // Assert
            Assert.AreEqual(expected, actual.ToString());
        }

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ToStringBuilder_MinLengthFirstStartIndexDataRowSource_ArgumentNullException(string source)
        {
            // Arrange
            int startIndex = 0;
            int length = 1;

            // Act
            var result = source.ToStringBuilder(startIndex, length);

            // Assert
            Assert.Fail($"{nameof(StringTool.ToStringBuilder)} should have thrown {typeof(ArgumentNullException).Name}.");
        }
    }
}
