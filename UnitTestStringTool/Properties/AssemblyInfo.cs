// ------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="John Merryweather Cooper">
//     Copyright (c) 2020 {companyName}.  All Rights Reserved.
//     Licensed under the GPL v3.0 license. See the LICENSE.txt
//     file in the project root for full license information.
// </copyright>
// <author>John Merryweather Cooper</author>
// <date>2020-11-10</date>
// <summary>
//     Description of File Name "AssemblyInfo.cs" in Project "UnitTestStringTool":
// </summary>
// ------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("UnitTestStringTool")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("UnitTestStringTool")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("18f44a91-9a86-4481-8621-5c2cc14b672c")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
